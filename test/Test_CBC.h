/**
 ** @file: Test_CBC.h
 ** @brief:    header file for testing AES-256 CBC mode encryption
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "CBC.h"
#include "IO.h"

#ifndef TEST_CBC_H
#define TEST_CBC_H

// test constants
#define CBC_BUF_SZ    128     // test buffer size in bytes

TEST(CBC, EndToEnd_000){
  /***** SETUP *****/
  CBC *cbcPtr = new CBC();                // pointer to CBC encryption engine

  const U8  key[]        = {0xb5, 0x2c, 0x50, 0x5a, 0x37, 0xd7, 0x8e, 0xda, // 08 // pointer to key material
                            0x5d, 0xd3, 0x4f, 0x20, 0xc2, 0x25, 0x40, 0xea, // 16
                            0x1b, 0x58, 0x96, 0x3c, 0xf8, 0xe5, 0xbf, 0x8f, // 24
                            0xfa, 0x85, 0xf9, 0xf2, 0x49, 0x25, 0x05, 0xb4};// 32

  U8  ctBuffer[CBC_BUF_SZ];                   // buffer to contain ciphertext blocks
  U8  ptBuffer[CBC_BUF_SZ];                   // pointer to plaintext blocks
  U8  resBuffer[CBC_BUF_SZ];                  // pointer to result block
  U32 offset = ZERO;                      // copy offset
  
  const BUF plaintext  {ZERO, ptBuffer};  // data structure containing plaintext blocks and size
        BUF ciphertext {ZERO, ctBuffer};  // data structure containing ciphertext blocks and size
        BUF result     {ZERO, resBuffer}; // data structure containing decrypted plaintext blocks and size
  
  memset(ciphertext.buffer, ZERO, CBC_BUF_SZ);
  memset(plaintext.buffer, ZERO, CBC_BUF_SZ);
  memset(result.buffer, ZERO, CBC_BUF_SZ);
  
  /***** ENCRYPT *****/
  cbcPtr->CBC_Encrypt(ciphertext, key, plaintext);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(ZERO, ciphertext.size);
  
  /***** PRINT AES-256 CBC ENCRYPTION RESULTS *****/
  std::cout << "AES-256 CBC IV:" << std::endl;
  PrintVector(ciphertext.buffer, CBC_IV_SZ);
  offset = CBC_IV_SZ;
  
  /***** DECRYPT *****/
  cbcPtr->CBC_Decrypt(ciphertext, key, result);
  EXPECT_EQ(ZERO, result.size);
  
  /***** PRINT AES-256 CBC DECRYPTION RESULTS *****/
  std::cout << "AES-256 CBC LENGTH OF PLAINTEXT:" << std::dec << result.size << std::endl;
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result.buffer, plaintext.buffer, result.size));
  
  /***** TEARDOWN *****/
  if(cbcPtr){
    delete cbcPtr;
    cbcPtr = nullptr;
  }
} // (CBC, EndToEnd_000)


TEST(CBC, EndToEnd_104){
  /***** SETUP *****/
  CBC *cbcPtr = new CBC();                // pointer to CBC encryption engine
  
  const U8  key[]        = {0x82, 0xc4, 0xf1, 0x2e, 0xee, 0xc3, 0xb2, 0xd3, // 08 // pointer to key material
                            0xd1, 0x57, 0xb0, 0xf9, 0x92, 0xd2, 0x92, 0xb2, // 16
                            0x37, 0x47, 0x8d, 0x2c, 0xec, 0xc1, 0xd5, 0xf1, // 24
                            0x61, 0x38, 0x9b, 0x97, 0xf9, 0x99, 0x05, 0x7a};// 32
        U8  ptBuffer[]   = {0x98, 0x2a, 0x29, 0x6e, 0xe1, 0xcd, 0x70, 0x86, // 08 // pointer to plaintext blocks
                            0xaf, 0xad, 0x97, 0x69, 0x45};                  // 13

  U8  ctBuffer[CBC_BUF_SZ];                   // pointer to locations within ciphertext block
  U8  resBuffer[CBC_BUF_SZ];                  // pointer to result block
  
  const BUF plaintext  {13, ptBuffer};    // data structure containing plaintext blocks and size
        BUF ciphertext {ZERO, ctBuffer};  // data structure containing ciphertext blocks and size
        BUF result     {ZERO, resBuffer}; // data structure containing decrypted plaintext blocks and size
  
  memset(ciphertext.buffer, ZERO, CBC_BUF_SZ);
  memset(result.buffer, ZERO, CBC_BUF_SZ);
  
  /***** ENCRYPT *****/
  cbcPtr->CBC_Encrypt(ciphertext, key, plaintext);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(ZERO, ciphertext.size);
  
  /***** PRINT AES-256 CBC ENCRYPTION RESULTS *****/
  std::cout << "AES-256 CBC CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext.buffer, ciphertext.size);
  
  /***** DECRYPT *****/
  cbcPtr->CBC_Decrypt(ciphertext, key, result);
  EXPECT_EQ(ZERO, result.size);
  
  /***** PRINT AES-256 CBC DECRYPTION RESULTS *****/
  std::cout << "AES-256 CBC LENGTH OF PLAINTEXT:" << std::dec << result.size << std::endl;
  PrintBlocks(result.buffer, result.size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_NE(SUCCESS, memcmp(result.buffer, plaintext.buffer, plaintext.size));
  
  /***** TEARDOWN *****/
  if(cbcPtr){
    delete cbcPtr;
    cbcPtr = nullptr;
  }
} // (CBC, EndToEnd_104)


TEST(CBC, EndToEnd_128){
  /***** SETUP *****/
  CBC *cbcPtr = new CBC();                // pointer to CBC encryption engine
  
  const U8  key[]        = {0x31, 0xbd, 0xad, 0xd9, 0x66, 0x98, 0xc2, 0x04, // 08 // pointer to key material
                            0xaa, 0x9c, 0xe1, 0x44, 0x8e, 0xa9, 0x4a, 0xe1, // 16
                            0xfb, 0x4a, 0x9a, 0x0b, 0x3c, 0x9d, 0x77, 0x3b, // 24
                            0x51, 0xbb, 0x18, 0x22, 0x66, 0x6b, 0x8f, 0x22};// 32
        U8  ptBuffer[]   = {0x2d, 0xb5, 0x16, 0x8e, 0x93, 0x25, 0x56, 0xf8, // 08 // pointer to plaintext blocks
                            0x08, 0x9a, 0x06, 0x22, 0x98, 0x1d, 0x01, 0x7d};// 16

  U8  ctBuffer[CBC_BUF_SZ];                   // pointer to locations within ciphertext block
  U8  resBuffer[CBC_BUF_SZ];                  // pointer to result block
  U32 offset = ZERO;                      // copy offset
  
  const BUF plaintext  {16, ptBuffer};    // data structure containing plaintext blocks and size
        BUF ciphertext {ZERO, ctBuffer};  // data structure containing ciphertext blocks and size
        BUF result     {ZERO, resBuffer}; // data structure containing decrypted plaintext blocks and size
  
  memset(ciphertext.buffer, ZERO, CBC_BUF_SZ);
  memset(result.buffer, ZERO, CBC_BUF_SZ);
  
  /***** ENCRYPT *****/
  cbcPtr->CBC_Encrypt(ciphertext, key, plaintext);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(plaintext.size + CBC_IV_SZ, ciphertext.size);
  
  /***** PRINT AES-256 CBC ENCRYPTION RESULTS *****/
  std::cout << "AES-256 CBC IV:" << std::endl;
  PrintVector(ciphertext.buffer, CBC_IV_SZ);
  offset = CBC_IV_SZ;
  
  std::cout << "AES-256 CBC CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext.buffer+offset, ciphertext.size-CBC_IV_SZ);
  
  /***** DECRYPT *****/
  cbcPtr->CBC_Decrypt(ciphertext, key, result);
  EXPECT_EQ(plaintext.size, result.size);
  
  /***** PRINT AES-256 CBC DECRYPTION RESULTS *****/
  std::cout << "AES-256 CBC LENGTH OF PLAINTEXT:" << std::dec << result.size << std::endl;
  PrintBlocks(result.buffer, result.size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result.buffer, plaintext.buffer, result.size));
  
  /***** TEARDOWN *****/
  if(cbcPtr){
    delete cbcPtr;
    cbcPtr = nullptr;
  }
} // (CBC, EndToEnd_128)


TEST(CBC, EndToEnd_256){
  /***** SETUP *****/
  CBC *cbcPtr = new CBC();                // pointer to CBC encryption engine
  
  const U8  key[]        = {0x26, 0x8e, 0xd1, 0xb5, 0xd7, 0xc9, 0xc7, 0x30, // 08 // pointer to key material
                            0x4f, 0x9c, 0xae, 0x5f, 0xc4, 0x37, 0xb4, 0xcd, // 16
                            0x3a, 0xeb, 0xe2, 0xec, 0x65, 0xf0, 0xd8, 0x5c, // 24
                            0x39, 0x18, 0xd3, 0xd3, 0xb5, 0xbb, 0xa8, 0x9b};// 32
        U8  ptBuffer[]   = {0xfe, 0x29, 0xa4, 0x0d, 0x8e, 0xbf, 0x57, 0x26, // 08 // pointer to plaintext blocks
                            0x2b, 0xdb, 0x87, 0x19, 0x1d, 0x01, 0x84, 0x3f, // 16
                            0x4c, 0xa4, 0xb2, 0xde, 0x97, 0xd8, 0x82, 0x73, // 24
                            0x15, 0x4a, 0x0b, 0x7d, 0x9e, 0x2f, 0xdb, 0x80};// 32

  U8  ctBuffer[CBC_BUF_SZ];                   // pointer to locations within ciphertext block
  U8  resBuffer[CBC_BUF_SZ];                  // pointer to result block
  U32 offset = ZERO;                      // copy offset
  
  const BUF plaintext  {32, ptBuffer};    // data structure containing plaintext blocks and size
        BUF ciphertext {ZERO, ctBuffer};  // data structure containing ciphertext blocks and size
        BUF result     {ZERO, resBuffer}; // data structure containing decrypted plaintext blocks and size
  
  memset(ciphertext.buffer, ZERO, CBC_BUF_SZ);
  memset(result.buffer, ZERO, CBC_BUF_SZ);
  
  /***** ENCRYPT *****/
  cbcPtr->CBC_Encrypt(ciphertext, key, plaintext);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(plaintext.size + CBC_IV_SZ, ciphertext.size);
  
  /***** PRINT AES-256 CBC ENCRYPTION RESULTS *****/
  std::cout << "AES-256 CBC IV:" << std::endl;
  PrintVector(ciphertext.buffer, CBC_IV_SZ);
  offset = CBC_IV_SZ;
  
  std::cout << "AES-256 CBC CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext.buffer+offset, ciphertext.size-CBC_IV_SZ);
  
  /***** DECRYPT *****/
  cbcPtr->CBC_Decrypt(ciphertext, key, result);
  EXPECT_EQ(plaintext.size, result.size);
  
  /***** PRINT AES-256 CBC DECRYPTION RESULTS *****/
  std::cout << "AES-256 CBC LENGTH OF PLAINTEXT:" << std::dec << result.size << std::endl;
  PrintBlocks(result.buffer, result.size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result.buffer, plaintext.buffer, result.size));
  
  /***** TEARDOWN *****/
  if(cbcPtr){
    delete cbcPtr;
    cbcPtr = nullptr;
  }
} // (CBC, EndToEnd_256)


TEST(CBC, EndToEnd_408){
  /***** SETUP *****/
  CBC *cbcPtr = new CBC();                // pointer to CBC encryption engine
  
  const U8  key[]        = {0x1f, 0xde, 0xd3, 0x2d, 0x59, 0x99, 0xde, 0x4a, // 08 // pointer to key material
                            0x76, 0xe0, 0xf8, 0x08, 0x21, 0x08, 0x82, 0x3a, // 16
                            0xef, 0x60, 0x41, 0x7e, 0x18, 0x96, 0xcf, 0x42, // 24
                            0x18, 0xa2, 0xfa, 0x90, 0xf6, 0x32, 0xec, 0x8a};// 32
        U8  ptBuffer[]   = {0x06, 0xb2, 0xc7, 0x58, 0x53, 0xdf, 0x9a, 0xeb, // 08 // pointer to plaintext blocks
                            0x17, 0xbe, 0xfd, 0x33, 0xce, 0xa8, 0x1c, 0x63, // 16
                            0x0b, 0x0f, 0xc5, 0x36, 0x67, 0xff, 0x45, 0x19, // 24
                            0x9c, 0x62, 0x9c, 0x8e, 0x15, 0xdc, 0xe4, 0x1e, // 32
                            0x53, 0x0a, 0xa7, 0x92, 0xf7, 0x96, 0xb8, 0x13, // 40
                            0x8e, 0xea, 0xb2, 0xe8, 0x6c, 0x7b, 0x7b, 0xee, // 48
                            0x1d, 0x40, 0xb0};                              // 51

  U8  ctBuffer[CBC_BUF_SZ];                   // pointer to locations within ciphertext block
  U8  resBuffer[CBC_BUF_SZ];                  // pointer to result block
  U32 offset = ZERO;                      // copy offset
  
  const BUF plaintext  {51, ptBuffer};    // data structure containing plaintext blocks and size
        BUF ciphertext {ZERO, ctBuffer};  // data structure containing ciphertext blocks and size
        BUF result     {ZERO, resBuffer}; // data structure containing decrypted plaintext blocks and size
  
  memset(ciphertext.buffer, ZERO, CBC_BUF_SZ);
  memset(result.buffer, ZERO, CBC_BUF_SZ);
  
  /***** ENCRYPT *****/
  cbcPtr->CBC_Encrypt(ciphertext, key, plaintext);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(plaintext.size + CBC_IV_SZ, ciphertext.size);
  
  /***** PRINT AES-256 CBC ENCRYPTION RESULTS *****/
  std::cout << "AES-256 CBC IV:" << std::endl;
  PrintVector(ciphertext.buffer, CBC_IV_SZ);
  offset = CBC_IV_SZ;
  
  std::cout << "AES-256 CBC CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext.buffer+offset, ciphertext.size-CBC_IV_SZ);
  
  /***** DECRYPT *****/
  cbcPtr->CBC_Decrypt(ciphertext, key, result);
  EXPECT_EQ(plaintext.size, result.size);
  
  /***** PRINT AES-256 CBC DECRYPTION RESULTS *****/
  std::cout << "AES-256 CBC LENGTH OF PLAINTEXT:" << std::dec << result.size << std::endl;
  PrintBlocks(result.buffer, result.size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result.buffer, plaintext.buffer, result.size));
  
  /***** TEARDOWN *****/
  if(cbcPtr){
    delete cbcPtr;
    cbcPtr = nullptr;
  }
} // (CBC, EndToEnd_408)

#endif // TEST_CBC_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/**
 ** @file: Test_GCM.h
 ** @brief:    header file for testing AES-256 GCM mode encryption
 **            bit generator (DRBG)
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "GCM.h"
#include "IO.h"

#ifndef TEST_GCM_H
#define TEST_GCM_H

// test constants
#define BLK_SZ        16      // AES-256 block size in bytes
#define GCM_BUF_SZ    128     // test buffer size in bytes

TEST(GCM, Decrypt_CT000){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  iv[]         = {0x58, 0xd2, 0x24, 0x0f,                         // 04 // pointer to initialization vector
                            0x58, 0x0a, 0x31, 0xc1,                         // 08
                            0xd2, 0x49, 0x48, 0xe9};                        // 12
  const U8  key[]        = {0xf5, 0xa2, 0xb2, 0x7c, 0x74, 0x35, 0x58, 0x72, // 08 // pointer to key material
                            0xeb, 0x3e, 0xf6, 0xc5, 0xfe, 0xaf, 0xaa, 0x74, // 16
                            0x0e, 0x6a, 0xe9, 0x90, 0xd9, 0xd4, 0x8c, 0x3b, // 24
                            0xd9, 0xbb, 0x82, 0x35, 0xe5, 0x89, 0xf0, 0x10};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};// 16
  const U8  tag[]        = {0x61, 0x1d, 0x25, 0xd2, 0x88, 0xe4, 0x0b, 0xd7, // 08 // pointer to correct Tag
                            0x91, 0x43, 0x0c, 0x9f, 0x8f, 0x1c, 0x43, 0xfc};// 16
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = (2*BLK_SZ) + GCM_IV_SZ;  // bytes of plaintext / ciphertext in this test
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_PT000)


TEST(GCM, Decrypt_CT000_FAIL_IV){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  const U8  iv[]         = {0x68, 0xd2, 0x24, 0x0f,                         // 04 // pointer to initialization vector
                            0x58, 0x0a, 0x31, 0xc1,                         // 08
                            0xd2, 0x49, 0x48, 0xe9};                        // 12
  const U8  key[]        = {0xf5, 0xa2, 0xb2, 0x7c, 0x74, 0x35, 0x58, 0x72, // 08 // pointer to key material
                            0xeb, 0x3e, 0xf6, 0xc5, 0xfe, 0xaf, 0xaa, 0x74, // 16
                            0x0e, 0x6a, 0xe9, 0x90, 0xd9, 0xd4, 0x8c, 0x3b, // 24
                            0xd9, 0xbb, 0x82, 0x35, 0xe5, 0x89, 0xf0, 0x10};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};// 16
  const U8  tag[]        = {0x61, 0x1d, 0x25, 0xd2, 0x88, 0xe4, 0x0b, 0xd7, // 08 // pointer to correct Tag
                            0x91, 0x43, 0x0c, 0x9f, 0x8f, 0x1c, 0x43, 0xfc};// 16

  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = (2*BLK_SZ) + GCM_IV_SZ;  // bytes of plaintext / ciphertext in this test
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT000_FAIL_IV)


TEST(GCM, Decrypt_CT000_FAIL_SZ){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  iv[]         = {0x58, 0xd2, 0x24, 0x0f,                         // 04 // pointer to initialization vector
                            0x58, 0x0a, 0x31, 0xc1,                         // 08
                            0xd2, 0x49, 0x48, 0xe9};                        // 12
  const U8  key[]        = {0xf5, 0xa2, 0xb2, 0x7c, 0x74, 0x35, 0x58, 0x72, // 08 // pointer to key material
                            0xeb, 0x3e, 0xf6, 0xc5, 0xfe, 0xaf, 0xaa, 0x74, // 16
                            0x0e, 0x6a, 0xe9, 0x90, 0xd9, 0xd4, 0x8c, 0x3b, // 24
                            0xd9, 0xbb, 0x82, 0x35, 0xe5, 0x89, 0xf0, 0x10};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};// 16
  const U8  tag[]        = {0x61, 0x1d, 0x25, 0xd2, 0x88, 0xe4, 0x0b, 0xd7, // 08 // pointer to correct Tag
                            0x91, 0x43, 0x0c, 0x9f, 0x8f, 0x1c, 0x43, 0xfc};// 16
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = (2*BLK_SZ) + GCM_IV_SZ;  // bytes of plaintext / ciphertext in this test
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT000_FAIL_SZ)


TEST(GCM, Decrypt_CT000_FAIL_TAG){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  iv[]         = {0x58, 0xd2, 0x24, 0x0f,                         // 04 // pointer to initialization vector
                            0x58, 0x0a, 0x31, 0xc1,                         // 08
                            0xd2, 0x49, 0x48, 0xe9};                        // 12
  const U8  key[]        = {0xf5, 0xa2, 0xb2, 0x7c, 0x74, 0x35, 0x58, 0x72, // 08 // pointer to key material
                            0xeb, 0x3e, 0xf6, 0xc5, 0xfe, 0xaf, 0xaa, 0x74, // 16
                            0x0e, 0x6a, 0xe9, 0x90, 0xd9, 0xd4, 0x8c, 0x3b, // 24
                            0xd9, 0xbb, 0x82, 0x35, 0xe5, 0x89, 0xf0, 0x10};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};// 16
  const U8  tag[]        = {0x61, 0x1d, 0x35, 0xd2, 0x88, 0xe4, 0x0b, 0xd7, // 08 // pointer to correct Tag
                            0x91, 0x43, 0x0c, 0x9f, 0x8f, 0x1c, 0x43, 0xfc};// 16
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = (2*BLK_SZ) + GCM_IV_SZ;  // bytes of plaintext / ciphertext in this test
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT000_FAIL_TAG)


TEST(GCM, Decrypt_CT104){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0x43, 0xaf, 0x49, 0xec, 0x1a, 0xe3, 0x73, 0x8a, // 08 // pointer to ciphertext
                            0x20, 0x75, 0x50, 0x34, 0xd6};                  // 13
  const U8  corrPT[]     = {0x5b, 0x87, 0x14, 0x13, 0x35, 0xf2, 0xbe, 0xca, // 08 // pointer to correct plaintext blocks
                            0xc1, 0xa5, 0x59, 0xe0, 0x5f};                  // 13
  const U8  iv[]         = {0xc5, 0xb6, 0x0d, 0xda,                         // 04 // pointer to initialization vector
                            0x3f, 0x33, 0x3b, 0x11,                         // 08
                            0x46, 0xe9, 0xda, 0x7c};                        // 12
  const U8  key[]        = {0xa7, 0x1d, 0xac, 0x13, 0x77, 0xa3, 0xbf, 0x5d, // 08 // pointer to key material
                            0x7f, 0xb1, 0xb5, 0xe3, 0x6b, 0xee, 0x70, 0xd2, // 16
                            0xe0, 0x1d, 0xe2, 0xa8, 0x4a, 0x1c, 0x10, 0x09, // 24
                            0xba, 0x74, 0x48, 0xf7, 0xf2, 0x61, 0x31, 0xdc};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68};// 16
  const U8  tag[]        = {0xef, 0xea, 0x62, 0x21, 0x7a, 0xae, 0x15, 0x74, // 08 // pointer to correct Tag
                            0x58, 0x54, 0x96, 0x2c, 0x08, 0x93, 0x87, 0x83};// 16
  
  const U32 testSize = 13;

  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;

  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT104)


TEST(GCM, Decrypt_CT104_FAIL_IV){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  bool returnVal = SUCCESS;               // return value

  const U8  ciphertext[] = {0x43, 0xaf, 0x49, 0xec, 0x1a, 0xe3, 0x73, 0x8a, // 08 // pointer to ciphertext
                            0x20, 0x75, 0x50, 0x34, 0xd6};                  // 13
  const U8  corrPT[]     = {0x5b, 0x87, 0x14, 0x13, 0x35, 0xf2, 0xbe, 0xca, // 08 // pointer to correct plaintext blocks
                            0xc1, 0xa5, 0x59, 0xe0, 0x5f};                  // 13
  const U8  iv[]         = {0xc5, 0xb6, 0x0d, 0xda,                         // 04 // pointer to initialization vector
                            0x3f, 0x33, 0x3b, 0x11,                         // 08
                            0x46, 0xe9, 0xdb, 0x7c};                        // 12
  const U8  key[]        = {0xa7, 0x1d, 0xac, 0x13, 0x77, 0xa3, 0xbf, 0x5d, // 08 // pointer to key material
                            0x7f, 0xb1, 0xb5, 0xe3, 0x6b, 0xee, 0x70, 0xd2, // 16
                            0xe0, 0x1d, 0xe2, 0xa8, 0x4a, 0x1c, 0x10, 0x09, // 24
                            0xba, 0x74, 0x48, 0xf7, 0xf2, 0x61, 0x31, 0xdc};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68};// 16
  const U8  tag[]        = {0xef, 0xea, 0x62, 0x21, 0x7a, 0xae, 0x15, 0x74, // 08 // pointer to correct Tag
                            0x58, 0x54, 0x96, 0x2c, 0x08, 0x93, 0x87, 0x83};// 16
  
  const U32 testSize = 13;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  if(memcmp(plaintext, corrPT, size))
    returnVal = FAIL;
  EXPECT_EQ(FAIL, returnVal);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT104_FAIL_IV)


TEST(GCM, Decrypt_CT104_FAIL_CT){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  bool returnVal = SUCCESS;               // return value

  const U8  ciphertext[] = {0x43, 0xbf, 0x49, 0xec, 0x1a, 0xe3, 0x73, 0x8a, // 08 // pointer to ciphertext
                            0x20, 0x75, 0x50, 0x34, 0xd6};                  // 13
  const U8  corrPT[]     = {0x5b, 0x87, 0x14, 0x13, 0x35, 0xf2, 0xbe, 0xca, // 08 // pointer to correct plaintext blocks
                            0xc1, 0xa5, 0x59, 0xe0, 0x5f};                  // 13
  const U8  iv[]         = {0xc5, 0xb6, 0x0d, 0xda,                         // 04 // pointer to initialization vector
                            0x3f, 0x33, 0x3b, 0x11,                         // 08
                            0x46, 0xe9, 0xda, 0x7c};                        // 12
  const U8  key[]        = {0xa7, 0x1d, 0xac, 0x13, 0x77, 0xa3, 0xbf, 0x5d, // 08 // pointer to key material
                            0x7f, 0xb1, 0xb5, 0xe3, 0x6b, 0xee, 0x70, 0xd2, // 16
                            0xe0, 0x1d, 0xe2, 0xa8, 0x4a, 0x1c, 0x10, 0x09, // 24
                            0xba, 0x74, 0x48, 0xf7, 0xf2, 0x61, 0x31, 0xdc};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68};// 16
  const U8  tag[]        = {0xef, 0xea, 0x62, 0x21, 0x7a, 0xae, 0x15, 0x74, // 08 // pointer to correct Tag
                            0x58, 0x54, 0x96, 0x2c, 0x08, 0x93, 0x87, 0x83};// 16
  
  const U32 testSize = 13;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  if(memcmp(plaintext, corrPT, size))
    returnVal = FAIL;
  EXPECT_EQ(FAIL, returnVal);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT104_FAIL_CT)


TEST(GCM, Decrypt_CT104_FAIL_SZ){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0x43, 0xaf, 0x49, 0xec, 0x1a, 0xe3, 0x73, 0x8a, // 08 // pointer to ciphertext
                            0x20, 0x75, 0x50, 0x34, 0xd6};                  // 13
  const U8  corrPT[]     = {0x5b, 0x87, 0x14, 0x13, 0x35, 0xf2, 0xbe, 0xca, // 08 // pointer to correct plaintext blocks
                            0xc1, 0xa5, 0x59, 0xe0, 0x5f};                  // 13
  const U8  iv[]         = {0xc5, 0xb6, 0x0d, 0xda,                         // 04 // pointer to initialization vector
                            0x3f, 0x33, 0x3b, 0x11,                         // 08
                            0x46, 0xe9, 0xda, 0x7c};                        // 12
  const U8  key[]        = {0xa7, 0x1d, 0xac, 0x13, 0x77, 0xa3, 0xbf, 0x5d, // 08 // pointer to key material
                            0x7f, 0xb1, 0xb5, 0xe3, 0x6b, 0xee, 0x70, 0xd2, // 16
                            0xe0, 0x1d, 0xe2, 0xa8, 0x4a, 0x1c, 0x10, 0x09, // 24
                            0xba, 0x74, 0x48, 0xf7, 0xf2, 0x61, 0x31, 0xdc};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x68};// 16
  const U8  tag[]        = {0xef, 0xea, 0x62, 0x21, 0x7a, 0xae, 0x15, 0x74, // 08 // pointer to correct Tag
                            0x58, 0x54, 0x96, 0x2c, 0x08, 0x93, 0x87, 0x83};// 16
  
  const U32 testSize = 13;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT104_FAIL_SZ)


TEST(GCM, Decrypt_CT104_FAIL_TAG){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0x43, 0xaf, 0x49, 0xec, 0x1a, 0xe3, 0x73, 0x8a, // 08 // pointer to ciphertext
                            0x20, 0x75, 0x50, 0x34, 0xd6};                  // 13
  const U8  corrPT[]     = {0x5b, 0x87, 0x14, 0x13, 0x35, 0xf2, 0xbe, 0xca, // 08 // pointer to correct plaintext blocks
                            0xc1, 0xa5, 0x59, 0xe0, 0x5f};                  // 13
  const U8  iv[]         = {0xc5, 0xb6, 0x0d, 0xda,                         // 04 // pointer to initialization vector
                            0x3f, 0x33, 0x3b, 0x11,                         // 08
                            0x46, 0xe9, 0xda, 0x7c};                        // 12
  const U8  key[]        = {0xa7, 0x1d, 0xac, 0x13, 0x77, 0xa3, 0xbf, 0x5d, // 08 // pointer to key material
                            0x7f, 0xb1, 0xb5, 0xe3, 0x6b, 0xee, 0x70, 0xd2, // 16
                            0xe0, 0x1d, 0xe2, 0xa8, 0x4a, 0x1c, 0x10, 0x09, // 24
                            0xba, 0x74, 0x48, 0xf7, 0xf2, 0x61, 0x31, 0xdc};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68};// 16
  const U8  tag[]        = {0xef, 0xea, 0x62, 0x21, 0x7a, 0xae, 0x15, 0x74, // 08 // pointer to correct Tag
                            0x58, 0x54, 0x97, 0x2c, 0x08, 0x93, 0x87, 0x83};// 16
  
  const U32 testSize = 13;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT104_FAIL_TAG)


TEST(GCM, Decrypt_CT128){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0xd2, 0xc7, 0x81, 0x10, 0xac, 0x7e, 0x8f, 0x10, // 08 // pointer to ciphertext
                            0x7c, 0x0d, 0xf0, 0x57, 0x0b, 0xd7, 0xc9, 0x0c};// 16
  const U8  corrPT[]     = {0x77, 0x89, 0xb4, 0x1c, 0xb3, 0xee, 0x54, 0x88, // 08 // pointer to correct plaintext blocks
                            0x14, 0xca, 0x0b, 0x38, 0x8c, 0x10, 0xb3, 0x43};// 16
  const U8  iv[]         = {0x47, 0x33, 0x60, 0xe0,                         // 04 // pointer to initialization vector
                            0xad, 0x24, 0x88, 0x99,                         // 08
                            0x59, 0x85, 0x89, 0x95};                        // 12
  const U8  key[]        = {0x4c, 0x8e, 0xbf, 0xe1, 0x44, 0x4e, 0xc1, 0xb2, // 08 // pointer to key material
                            0xd5, 0x03, 0xc6, 0x98, 0x66, 0x59, 0xaf, 0x2c, // 16
                            0x94, 0xfa, 0xfe, 0x94, 0x5f, 0x72, 0xc1, 0xe8, // 24
                            0x48, 0x6a, 0x5a, 0xcf, 0xed, 0xb8, 0xa0, 0xf8};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80};// 16
  const U8  tag[]        = {0xeb, 0xbc, 0xb8, 0x08, 0xbb, 0xc4, 0xb1, 0xf0, // 08 // pointer to correct Tag
                            0x2c, 0x2a, 0xbd, 0x84, 0xc8, 0x0a, 0x20, 0x01};// 16
  
  const U32 testSize = 16;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT128)


TEST(GCM, Decrypt_CT128_FAIL_IV){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  bool returnVal = SUCCESS;               // return value

  const U8  ciphertext[] = {0xd2, 0xc7, 0x81, 0x10, 0xac, 0x7e, 0x8f, 0x10, // 08 // pointer to ciphertext
                            0x7c, 0x0d, 0xf0, 0x57, 0x0b, 0xd7, 0xc9, 0x0c};// 16
  const U8  corrPT[]     = {0x77, 0x89, 0xb4, 0x1c, 0xb3, 0xee, 0x54, 0x88, // 08 // pointer to correct plaintext blocks
                            0x14, 0xca, 0x0b, 0x38, 0x8c, 0x10, 0xb3, 0x43};// 16
  const U8  iv[]         = {0x47, 0x33, 0x60, 0xe1,                         // 04 // pointer to initialization vector
                            0xad, 0x24, 0x88, 0x99,                         // 08
                            0x59, 0x85, 0x89, 0x95};                        // 12
  const U8  key[]        = {0x4c, 0x8e, 0xbf, 0xe1, 0x44, 0x4e, 0xc1, 0xb2, // 08 // pointer to key material
                            0xd5, 0x03, 0xc6, 0x98, 0x66, 0x59, 0xaf, 0x2c, // 16
                            0x94, 0xfa, 0xfe, 0x94, 0x5f, 0x72, 0xc1, 0xe8, // 24
                            0x48, 0x6a, 0x5a, 0xcf, 0xed, 0xb8, 0xa0, 0xf8};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80};// 16
  const U8  tag[]        = {0xeb, 0xbc, 0xb8, 0x08, 0xbb, 0xc4, 0xb1, 0xf0, // 08 // pointer to correct Tag
                            0x2c, 0x2a, 0xbd, 0x84, 0xc8, 0x0a, 0x20, 0x01};// 16
  
  const U32 testSize = 16;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  if(memcmp(plaintext, corrPT, size))
    returnVal = FAIL;
  EXPECT_EQ(FAIL, returnVal);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT128_FAIL_IV)


TEST(GCM, Decrypt_CT128_FAIL_CT){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  bool returnVal = SUCCESS;               // return value

  const U8  ciphertext[] = {0xd2, 0xb7, 0x81, 0x10, 0xac, 0x7e, 0x8f, 0x10, // 08 // pointer to ciphertext
                            0x7c, 0x0d, 0xf0, 0x57, 0x0b, 0xd7, 0xc9, 0x0c};// 16
  const U8  corrPT[]     = {0x77, 0x89, 0xb4, 0x1c, 0xb3, 0xee, 0x54, 0x88, // 08 // pointer to correct plaintext blocks
                            0x14, 0xca, 0x0b, 0x38, 0x8c, 0x10, 0xb3, 0x43};// 16
  const U8  iv[]         = {0x47, 0x33, 0x60, 0xe0,                         // 04 // pointer to initialization vector
                            0xad, 0x24, 0x88, 0x99,                         // 08
                            0x59, 0x85, 0x89, 0x95};                        // 12
  const U8  key[]        = {0x4c, 0x8e, 0xbf, 0xe1, 0x44, 0x4e, 0xc1, 0xb2, // 08 // pointer to key material
                            0xd5, 0x03, 0xc6, 0x98, 0x66, 0x59, 0xaf, 0x2c, // 16
                            0x94, 0xfa, 0xfe, 0x94, 0x5f, 0x72, 0xc1, 0xe8, // 24
                            0x48, 0x6a, 0x5a, 0xcf, 0xed, 0xb8, 0xa0, 0xf8};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80};// 16
  const U8  tag[]        = {0xeb, 0xbc, 0xb8, 0x08, 0xbb, 0xc4, 0xb1, 0xf0, // 08 // pointer to correct Tag
                            0x2c, 0x2a, 0xbd, 0x84, 0xc8, 0x0a, 0x20, 0x01};// 16
  
  const U32 testSize = 16;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  if(memcmp(plaintext, corrPT, size))
    returnVal = FAIL;
  EXPECT_EQ(FAIL, returnVal);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT128_FAIL_CT)


TEST(GCM, Decrypt_CT128_FAIL_SZ){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0xd2, 0xc7, 0x81, 0x10, 0xac, 0x7e, 0x8f, 0x10, // 08 // pointer to ciphertext
                            0x7c, 0x0d, 0xf0, 0x57, 0x0b, 0xd7, 0xc9, 0x0c};// 16
  const U8  corrPT[]     = {0x77, 0x89, 0xb4, 0x1c, 0xb3, 0xee, 0x54, 0x88, // 08 // pointer to correct plaintext blocks
                            0x14, 0xca, 0x0b, 0x38, 0x8c, 0x10, 0xb3, 0x43};// 16
  const U8  iv[]         = {0x47, 0x33, 0x60, 0xe0,                         // 04 // pointer to initialization vector
                            0xad, 0x24, 0x88, 0x99,                         // 08
                            0x59, 0x85, 0x89, 0x95};                        // 12
  const U8  key[]        = {0x4c, 0x8e, 0xbf, 0xe1, 0x44, 0x4e, 0xc1, 0xb2, // 08 // pointer to key material
                            0xd5, 0x03, 0xc6, 0x98, 0x66, 0x59, 0xaf, 0x2c, // 16
                            0x94, 0xfa, 0xfe, 0x94, 0x5f, 0x72, 0xc1, 0xe8, // 24
                            0x48, 0x6a, 0x5a, 0xcf, 0xed, 0xb8, 0xa0, 0xf8};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80};// 16
  const U8  tag[]        = {0xeb, 0xbc, 0xb8, 0x08, 0xbb, 0xc4, 0xb1, 0xf0, // 08 // pointer to correct Tag
                            0x2c, 0x2a, 0xbd, 0x84, 0xc8, 0x0a, 0x20, 0x01};// 16
  
  const U32 testSize = 16;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT128_FAIL_SZ)


TEST(GCM, Decrypt_CT128_FAIL_TAG){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0xd2, 0xc7, 0x81, 0x10, 0xac, 0x7e, 0x8f, 0x10, // 08 // pointer to ciphertext
                            0x7c, 0x0d, 0xf0, 0x57, 0x0b, 0xd7, 0xc9, 0x0c};// 16
  const U8  corrPT[]     = {0x77, 0x89, 0xb4, 0x1c, 0xb3, 0xee, 0x54, 0x88, // 08 // pointer to correct plaintext blocks
                            0x14, 0xca, 0x0b, 0x38, 0x8c, 0x10, 0xb3, 0x43};// 16
  const U8  iv[]         = {0x47, 0x33, 0x60, 0xe0,                         // 04 // pointer to initialization vector
                            0xad, 0x24, 0x88, 0x99,                         // 08
                            0x59, 0x85, 0x89, 0x95};                        // 12
  const U8  key[]        = {0x4c, 0x8e, 0xbf, 0xe1, 0x44, 0x4e, 0xc1, 0xb2, // 08 // pointer to key material
                            0xd5, 0x03, 0xc6, 0x98, 0x66, 0x59, 0xaf, 0x2c, // 16
                            0x94, 0xfa, 0xfe, 0x94, 0x5f, 0x72, 0xc1, 0xe8, // 24
                            0x48, 0x6a, 0x5a, 0xcf, 0xed, 0xb8, 0xa0, 0xf8};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80};// 16
  const U8  tag[]        = {0xeb, 0xbc, 0xb8, 0x08, 0xbb, 0xc4, 0xb1, 0xf0, // 08 // pointer to correct Tag
                            0x2c, 0x2a, 0xbd, 0x84, 0xc8, 0x0a, 0x20, 0xa1};// 16
  
  const U32 testSize = 16;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT128_FAIL_TAG)


TEST(GCM, Decrypt_CT256){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0x84, 0xe5, 0xf2, 0x3f, 0x95, 0x64, 0x8f, 0xa2, // 08 // pointer to ciphertext
                            0x47, 0xcb, 0x28, 0xee, 0xf5, 0x3a, 0xbe, 0xc9, // 16
                            0x47, 0xdb, 0xf0, 0x5a, 0xc9, 0x53, 0x73, 0x46, // 24
                            0x18, 0x11, 0x15, 0x83, 0x84, 0x0b, 0xd9, 0x80};// 32
  const U8  corrPT[]     = {0x25, 0x43, 0x15, 0x87, 0xe9, 0xec, 0xff, 0xc7, // 08 // pointer to correct plaintext blocks
                            0xc3, 0x7f, 0x8d, 0x6d, 0x52, 0xa9, 0xbc, 0x33, // 16
                            0x10, 0x65, 0x1d, 0x46, 0xfb, 0x0e, 0x3b, 0xad, // 24
                            0x27, 0x26, 0xc8, 0xf2, 0xdb, 0x65, 0x37, 0x49};// 32
  const U8  iv[]         = {0xca, 0xfa, 0xbd, 0x96,                         // 04 // pointer to initialization vector
                            0x72, 0xca, 0x6c, 0x79,                         // 08
                            0xa2, 0xfb, 0xdc, 0x22};                        // 12
  const U8  key[]        = {0xc3, 0xd9, 0x98, 0x25, 0xf2, 0x18, 0x1f, 0x48, // 08 // pointer to key material
                            0x08, 0xac, 0xd2, 0x06, 0x8e, 0xac, 0x74, 0x41, // 16
                            0xa6, 0x5b, 0xd4, 0x28, 0xf1, 0x4d, 0x2a, 0xab, // 24
                            0x43, 0xfe, 0xfc, 0x01, 0x29, 0x09, 0x11, 0x39};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00};// 16
  const U8  tag[]        = {0x8e, 0xac, 0x86, 0x75, 0x1d, 0x85, 0xca, 0xc4, // 08 // pointer to correct Tag
                            0xc7, 0x60, 0x97, 0x91, 0xd3, 0x73, 0xb2, 0xa3};// 16
  
  const U32 testSize = 32;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT256)


TEST(GCM, Decrypt_CT256_FAIL_IV){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  bool returnVal = SUCCESS;               // return value

  const U8  ciphertext[] = {0x84, 0xe5, 0xf2, 0x3f, 0x95, 0x64, 0x8f, 0xa2, // 08 // pointer to ciphertext
                            0x47, 0xcb, 0x28, 0xee, 0xf5, 0x3a, 0xbe, 0xc9, // 16
                            0x47, 0xdb, 0xf0, 0x5a, 0xc9, 0x53, 0x73, 0x46, // 24
                            0x18, 0x11, 0x15, 0x83, 0x84, 0x0b, 0xd9, 0x80};// 32
  const U8  corrPT[]     = {0x25, 0x43, 0x15, 0x87, 0xe9, 0xec, 0xff, 0xc7, // 08 // pointer to correct plaintext blocks
                            0xc3, 0x7f, 0x8d, 0x6d, 0x52, 0xa9, 0xbc, 0x33, // 16
                            0x10, 0x65, 0x1d, 0x46, 0xfb, 0x0e, 0x3b, 0xad, // 24
                            0x27, 0x26, 0xc8, 0xf2, 0xdb, 0x65, 0x37, 0x49};// 32
  const U8  iv[]         = {0xca, 0xfa, 0xbd, 0x96,                         // 04 // pointer to initialization vector
                            0x7a, 0xca, 0x6c, 0x79,                         // 08
                            0xa2, 0xfb, 0xdc, 0x22};                        // 12
  const U8  key[]        = {0xc3, 0xd9, 0x98, 0x25, 0xf2, 0x18, 0x1f, 0x48, // 08 // pointer to key material
                            0x08, 0xac, 0xd2, 0x06, 0x8e, 0xac, 0x74, 0x41, // 16
                            0xa6, 0x5b, 0xd4, 0x28, 0xf1, 0x4d, 0x2a, 0xab, // 24
                            0x43, 0xfe, 0xfc, 0x01, 0x29, 0x09, 0x11, 0x39};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00};// 16
  const U8  tag[]        = {0x8e, 0xac, 0x86, 0x75, 0x1d, 0x85, 0xca, 0xc4, // 08 // pointer to correct Tag
                            0xc7, 0x60, 0x97, 0x91, 0xd3, 0x73, 0xb2, 0xa3};// 16
  
  const U32 testSize = 32;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  if(memcmp(plaintext, corrPT, size))
    returnVal = FAIL;
  EXPECT_EQ(FAIL, returnVal);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT256_FAIL_IV)


TEST(GCM, Decrypt_CT256_FAIL_CT){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  bool returnVal = SUCCESS;               // return value

  const U8  ciphertext[] = {0x84, 0xe5, 0xf2, 0x3f, 0x95, 0x64, 0x8f, 0xa2, // 08 // pointer to ciphertext
                            0x47, 0xcb, 0x28, 0xee, 0xf5, 0x3a, 0xbe, 0xc9, // 16
                            0x47, 0xdb, 0xff, 0x5a, 0xc9, 0x53, 0x73, 0x46, // 24
                            0x18, 0x11, 0x15, 0x83, 0x84, 0x0b, 0xd9, 0x80};// 32
  const U8  corrPT[]     = {0x25, 0x43, 0x15, 0x87, 0xe9, 0xec, 0xff, 0xc7, // 08 // pointer to correct plaintext blocks
                            0xc3, 0x7f, 0x8d, 0x6d, 0x52, 0xa9, 0xbc, 0x33, // 16
                            0x10, 0x65, 0x1d, 0x46, 0xfb, 0x0e, 0x3b, 0xad, // 24
                            0x27, 0x26, 0xc8, 0xf2, 0xdb, 0x65, 0x37, 0x49};// 32
  const U8  iv[]         = {0xca, 0xfa, 0xbd, 0x96,                         // 04 // pointer to initialization vector
                            0x72, 0xca, 0x6c, 0x79,                         // 08
                            0xa2, 0xfb, 0xdc, 0x22};                        // 12
  const U8  key[]        = {0xc3, 0xd9, 0x98, 0x25, 0xf2, 0x18, 0x1f, 0x48, // 08 // pointer to key material
                            0x08, 0xac, 0xd2, 0x06, 0x8e, 0xac, 0x74, 0x41, // 16
                            0xa6, 0x5b, 0xd4, 0x28, 0xf1, 0x4d, 0x2a, 0xab, // 24
                            0x43, 0xfe, 0xfc, 0x01, 0x29, 0x09, 0x11, 0x39};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00};// 16
  const U8  tag[]        = {0x8e, 0xac, 0x86, 0x75, 0x1d, 0x85, 0xca, 0xc4, // 08 // pointer to correct Tag
                            0xc7, 0x60, 0x97, 0x91, 0xd3, 0x73, 0xb2, 0xa3};// 16
  
  const U32 testSize = 32;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  if(memcmp(plaintext, corrPT, size))
    returnVal = FAIL;
  EXPECT_EQ(FAIL, returnVal);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT256_FAIL_CT)


TEST(GCM, Decrypt_CT256_FAIL_SZ){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0x84, 0xe5, 0xf2, 0x3f, 0x95, 0x64, 0x8f, 0xa2, // 08 // pointer to ciphertext
                            0x47, 0xcb, 0x28, 0xee, 0xf5, 0x3a, 0xbe, 0xc9, // 16
                            0x47, 0xdb, 0xf0, 0x5a, 0xc9, 0x53, 0x73, 0x46, // 24
                            0x18, 0x11, 0x15, 0x83, 0x84, 0x0b, 0xd9, 0x80};// 32
  const U8  corrPT[]     = {0x25, 0x43, 0x15, 0x87, 0xe9, 0xec, 0xff, 0xc7, // 08 // pointer to correct plaintext blocks
                            0xc3, 0x7f, 0x8d, 0x6d, 0x52, 0xa9, 0xbc, 0x33, // 16
                            0x10, 0x65, 0x1d, 0x46, 0xfb, 0x0e, 0x3b, 0xad, // 24
                            0x27, 0x26, 0xc8, 0xf2, 0xdb, 0x65, 0x37, 0x49};// 32
  const U8  iv[]         = {0xca, 0xfa, 0xbd, 0x96,                         // 04 // pointer to initialization vector
                            0x72, 0xca, 0x6c, 0x79,                         // 08
                            0xa2, 0xfb, 0xdc, 0x22};                        // 12
  const U8  key[]        = {0xc3, 0xd9, 0x98, 0x25, 0xf2, 0x18, 0x1f, 0x48, // 08 // pointer to key material
                            0x08, 0xac, 0xd2, 0x06, 0x8e, 0xac, 0x74, 0x41, // 16
                            0xa6, 0x5b, 0xd4, 0x28, 0xf1, 0x4d, 0x2a, 0xab, // 24
                            0x43, 0xfe, 0xfc, 0x01, 0x29, 0x09, 0x11, 0x39};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x0f, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00};// 16
  const U8  tag[]        = {0x8e, 0xac, 0x86, 0x75, 0x1d, 0x85, 0xca, 0xc4, // 08 // pointer to correct Tag
                            0xc7, 0x60, 0x97, 0x91, 0xd3, 0x73, 0xb2, 0xa3};// 16
  
  const U32 testSize = 32;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT256_FAIL_SZ)


TEST(GCM, Decrypt_CT256_FAIL_TAG){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0x84, 0xe5, 0xf2, 0x3f, 0x95, 0x64, 0x8f, 0xa2, // 08 // pointer to ciphertext
                            0x47, 0xcb, 0x28, 0xee, 0xf5, 0x3a, 0xbe, 0xc9, // 16
                            0x47, 0xdb, 0xf0, 0x5a, 0xc9, 0x53, 0x73, 0x46, // 24
                            0x18, 0x11, 0x15, 0x83, 0x84, 0x0b, 0xd9, 0x80};// 32
  const U8  corrPT[]     = {0x25, 0x43, 0x15, 0x87, 0xe9, 0xec, 0xff, 0xc7, // 08 // pointer to correct plaintext blocks
                            0xc3, 0x7f, 0x8d, 0x6d, 0x52, 0xa9, 0xbc, 0x33, // 16
                            0x10, 0x65, 0x1d, 0x46, 0xfb, 0x0e, 0x3b, 0xad, // 24
                            0x27, 0x26, 0xc8, 0xf2, 0xdb, 0x65, 0x37, 0x49};// 32
  const U8  iv[]         = {0xca, 0xfa, 0xbd, 0x96,                         // 04 // pointer to initialization vector
                            0x72, 0xca, 0x6c, 0x79,                         // 08
                            0xa2, 0xfb, 0xdc, 0x22};                        // 12
  const U8  key[]        = {0xc3, 0xd9, 0x98, 0x25, 0xf2, 0x18, 0x1f, 0x48, // 08 // pointer to key material
                            0x08, 0xac, 0xd2, 0x06, 0x8e, 0xac, 0x74, 0x41, // 16
                            0xa6, 0x5b, 0xd4, 0x28, 0xf1, 0x4d, 0x2a, 0xab, // 24
                            0x43, 0xfe, 0xfc, 0x01, 0x29, 0x09, 0x11, 0x39};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00};// 16
  const U8  tag[]        = {0x8e, 0xac, 0x86, 0x75, 0x1d, 0x85, 0xca, 0xcf, // 08 // pointer to correct Tag
                            0xc7, 0x60, 0x97, 0x91, 0xd3, 0x73, 0xb2, 0xa3};// 16
  
  const U32 testSize = 32;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT256_FAIL_TAG)


TEST(GCM, Decrypt_CT408){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0xb0, 0xd2, 0x54, 0xab, 0xe4, 0x3b, 0xdb, 0x56, // 08 // pointer to ciphertext
                            0x3e, 0xad, 0x66, 0x91, 0x92, 0xc1, 0xe5, 0x7e, // 16
                            0x9a, 0x85, 0xc5, 0x1d, 0xba, 0x0f, 0x1c, 0x85, // 24
                            0x01, 0xd1, 0xce, 0x92, 0x27, 0x3f, 0x1c, 0xe7, // 32
                            0xe1, 0x40, 0xdc, 0xfa, 0xc9, 0x47, 0x57, 0xfa, // 40
                            0xbb, 0x12, 0x8c, 0xaa, 0xd1, 0x69, 0x12, 0xce, // 48
                            0xad, 0x06, 0x07};                              // 51
  const U8  corrPT[]     = {0xd6, 0x02, 0xc0, 0x6b, 0x94, 0x7a, 0xbe, 0x06, // 08 // pointer to correct plaintext blocks
                            0xcf, 0x6a, 0xa2, 0xc5, 0xc1, 0x56, 0x2e, 0x29, // 16
                            0x06, 0x2a, 0xd6, 0x22, 0x0d, 0xa9, 0xbc, 0x9c, // 24
                            0x25, 0xd6, 0x6a, 0x60, 0xbd, 0x85, 0xa8, 0x0d, // 32
                            0x4f, 0xbc, 0xc1, 0xfb, 0x49, 0x19, 0xb6, 0x56, // 40
                            0x6b, 0xe3, 0x5a, 0xf9, 0x81, 0x9a, 0xba, 0x83, // 48
                            0x6b, 0x8b, 0x47};                              // 51
  const U8  iv[]         = {0x0e, 0x39, 0x64, 0x46,                         // 04 // pointer to initialization vector
                            0x65, 0x55, 0x82, 0x83,                         // 08
                            0x8f, 0x27, 0xf7, 0x2f};                        // 12
  const U8  key[]        = {0x44, 0x33, 0xdb, 0x5f, 0xe0, 0x66, 0x96, 0x0b, // 08 // pointer to key material
                            0xdd, 0x4e, 0x1d, 0x4d, 0x41, 0x8b, 0x64, 0x1c, // 16
                            0x14, 0xbf, 0xce, 0xf9, 0xd5, 0x74, 0xe2, 0x9d, // 24
                            0xcd, 0x09, 0x95, 0x35, 0x28, 0x50, 0xf1, 0xeb};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x98};// 16
  const U8  tag[]        = {0x6a, 0x30, 0x0d, 0xbb, 0xad, 0x48, 0xef, 0x00, // 08 // pointer to correct Tag
                            0x68, 0x0e, 0x59, 0xb8, 0x4d, 0xee, 0x0b, 0x7c};// 16
  
  const U32 testSize = 51;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT408)


TEST(GCM, Decrypt_CT408_FAIL_IV){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  bool returnVal = SUCCESS;               // return value

  const U8  ciphertext[] = {0xb0, 0xd2, 0x54, 0xab, 0xe4, 0x3b, 0xdb, 0x56, // 08 // pointer to ciphertext
                            0x3e, 0xad, 0x66, 0x91, 0x92, 0xc1, 0xe5, 0x7e, // 16
                            0x9a, 0x85, 0xc5, 0x1d, 0xba, 0x0f, 0x1c, 0x85, // 24
                            0x01, 0xd1, 0xce, 0x92, 0x27, 0x3f, 0x1c, 0xe7, // 32
                            0xe1, 0x40, 0xdc, 0xfa, 0xc9, 0x47, 0x57, 0xfa, // 40
                            0xbb, 0x12, 0x8c, 0xaa, 0xd1, 0x69, 0x12, 0xce, // 48
                            0xad, 0x06, 0x07};                              // 51
  const U8  corrPT[]     = {0xd6, 0x02, 0xc0, 0x6b, 0x94, 0x7a, 0xbe, 0x06, // 08 // pointer to correct plaintext blocks
                            0xcf, 0x6a, 0xa2, 0xc5, 0xc1, 0x56, 0x2e, 0x29, // 16
                            0x06, 0x2a, 0xd6, 0x22, 0x0d, 0xa9, 0xbc, 0x9c, // 24
                            0x25, 0xd6, 0x6a, 0x60, 0xbd, 0x85, 0xa8, 0x0d, // 32
                            0x4f, 0xbc, 0xc1, 0xfb, 0x49, 0x19, 0xb6, 0x56, // 40
                            0x6b, 0xe3, 0x5a, 0xf9, 0x81, 0x9a, 0xba, 0x83, // 48
                            0x6b, 0x8b, 0x47};                              // 51
  const U8  iv[]         = {0x0e, 0x39, 0x64, 0x46,                         // 04 // pointer to initialization vector
                            0x65, 0x55, 0x8b, 0x83,                         // 08
                            0x8f, 0x27, 0xf7, 0x2f};                        // 12
  const U8  key[]        = {0x44, 0x33, 0xdb, 0x5f, 0xe0, 0x66, 0x96, 0x0b, // 08 // pointer to key material
                            0xdd, 0x4e, 0x1d, 0x4d, 0x41, 0x8b, 0x64, 0x1c, // 16
                            0x14, 0xbf, 0xce, 0xf9, 0xd5, 0x74, 0xe2, 0x9d, // 24
                            0xcd, 0x09, 0x95, 0x35, 0x28, 0x50, 0xf1, 0xeb};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x98};// 16
  const U8  tag[]        = {0x6a, 0x30, 0x0d, 0xbb, 0xad, 0x48, 0xef, 0x00, // 08 // pointer to correct Tag
                            0x68, 0x0e, 0x59, 0xb8, 0x4d, 0xee, 0x0b, 0x7c};// 16
  
  const U32 testSize = 51;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  if(memcmp(plaintext, corrPT, size))
    returnVal = FAIL;
  EXPECT_EQ(FAIL, returnVal);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT408_FAIL_IV)


TEST(GCM, Decrypt_CT408_FAIL_CT){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  bool returnVal = SUCCESS;               // return value

  const U8  ciphertext[] = {0xb0, 0xd2, 0x54, 0xab, 0xe4, 0x3b, 0xdb, 0x56, // 08 // pointer to ciphertext
                            0x3e, 0xad, 0x66, 0x91, 0x92, 0xc1, 0xe5, 0x7e, // 16
                            0x9a, 0x85, 0xc5, 0x1d, 0xba, 0x0f, 0x1c, 0x85, // 24
                            0x01, 0xd3, 0xce, 0x92, 0x27, 0x3f, 0x1c, 0xe7, // 32
                            0xe1, 0x40, 0xdc, 0xfa, 0xc9, 0x47, 0x57, 0xfa, // 40
                            0xbb, 0x12, 0x8c, 0xaa, 0xd1, 0x69, 0x12, 0xce, // 48
                            0xad, 0x06, 0x07};                              // 51
  const U8  corrPT[]     = {0xd6, 0x02, 0xc0, 0x6b, 0x94, 0x7a, 0xbe, 0x06, // 08 // pointer to correct plaintext blocks
                            0xcf, 0x6a, 0xa2, 0xc5, 0xc1, 0x56, 0x2e, 0x29, // 16
                            0x06, 0x2a, 0xd6, 0x22, 0x0d, 0xa9, 0xbc, 0x9c, // 24
                            0x25, 0xd6, 0x6a, 0x60, 0xbd, 0x85, 0xa8, 0x0d, // 32
                            0x4f, 0xbc, 0xc1, 0xfb, 0x49, 0x19, 0xb6, 0x56, // 40
                            0x6b, 0xe3, 0x5a, 0xf9, 0x81, 0x9a, 0xba, 0x83, // 48
                            0x6b, 0x8b, 0x47};                              // 51
  const U8  iv[]         = {0x0e, 0x39, 0x64, 0x46,                         // 04 // pointer to initialization vector
                            0x65, 0x55, 0x82, 0x83,                         // 08
                            0x8f, 0x27, 0xf7, 0x2f};                        // 12
  const U8  key[]        = {0x44, 0x33, 0xdb, 0x5f, 0xe0, 0x66, 0x96, 0x0b, // 08 // pointer to key material
                            0xdd, 0x4e, 0x1d, 0x4d, 0x41, 0x8b, 0x64, 0x1c, // 16
                            0x14, 0xbf, 0xce, 0xf9, 0xd5, 0x74, 0xe2, 0x9d, // 24
                            0xcd, 0x09, 0x95, 0x35, 0x28, 0x50, 0xf1, 0xeb};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x98};// 16
  const U8  tag[]        = {0x6a, 0x30, 0x0d, 0xbb, 0xad, 0x48, 0xef, 0x00, // 08 // pointer to correct Tag
                            0x68, 0x0e, 0x59, 0xb8, 0x4d, 0xee, 0x0b, 0x7c};// 16
  
  const U32 testSize = 51;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  if(memcmp(plaintext, corrPT, size))
    returnVal = FAIL;
  EXPECT_EQ(FAIL, returnVal);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT408_FAIL_CT)


TEST(GCM, Decrypt_CT408_FAIL_SZ){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0xb0, 0xd2, 0x54, 0xab, 0xe4, 0x3b, 0xdb, 0x56, // 08 // pointer to ciphertext
                            0x3e, 0xad, 0x66, 0x91, 0x92, 0xc1, 0xe5, 0x7e, // 16
                            0x9a, 0x85, 0xc5, 0x1d, 0xba, 0x0f, 0x1c, 0x85, // 24
                            0x01, 0xd1, 0xce, 0x92, 0x27, 0x3f, 0x1c, 0xe7, // 32
                            0xe1, 0x40, 0xdc, 0xfa, 0xc9, 0x47, 0x57, 0xfa, // 40
                            0xbb, 0x12, 0x8c, 0xaa, 0xd1, 0x69, 0x12, 0xce, // 48
                            0xad, 0x06, 0x07};                              // 51
  const U8  corrPT[]     = {0xd6, 0x02, 0xc0, 0x6b, 0x94, 0x7a, 0xbe, 0x06, // 08 // pointer to correct plaintext blocks
                            0xcf, 0x6a, 0xa2, 0xc5, 0xc1, 0x56, 0x2e, 0x29, // 16
                            0x06, 0x2a, 0xd6, 0x22, 0x0d, 0xa9, 0xbc, 0x9c, // 24
                            0x25, 0xd6, 0x6a, 0x60, 0xbd, 0x85, 0xa8, 0x0d, // 32
                            0x4f, 0xbc, 0xc1, 0xfb, 0x49, 0x19, 0xb6, 0x56, // 40
                            0x6b, 0xe3, 0x5a, 0xf9, 0x81, 0x9a, 0xba, 0x83, // 48
                            0x6b, 0x8b, 0x47};                              // 51
  const U8  iv[]         = {0x0e, 0x39, 0x64, 0x46,                         // 04 // pointer to initialization vector
                            0x65, 0x55, 0x82, 0x83,                         // 08
                            0x8f, 0x27, 0xf7, 0x2f};                        // 12
  const U8  key[]        = {0x44, 0x33, 0xdb, 0x5f, 0xe0, 0x66, 0x96, 0x0b, // 08 // pointer to key material
                            0xdd, 0x4e, 0x1d, 0x4d, 0x41, 0x8b, 0x64, 0x1c, // 16
                            0x14, 0xbf, 0xce, 0xf9, 0xd5, 0x74, 0xe2, 0x9d, // 24
                            0xcd, 0x09, 0x95, 0x35, 0x28, 0x50, 0xf1, 0xeb};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x0a, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x98};// 16
  const U8  tag[]        = {0x6a, 0x30, 0x0d, 0xbb, 0xad, 0x48, 0xef, 0x00, // 08 // pointer to correct Tag
                            0x68, 0x0e, 0x59, 0xb8, 0x4d, 0xee, 0x0b, 0x7c};// 16
  
  const U32 testSize = 51;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT408_FAIL_SZ)


TEST(GCM, Decrypt_CT408_FAIL_TAG){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  ciphertext[] = {0xb0, 0xd2, 0x54, 0xab, 0xe4, 0x3b, 0xdb, 0x56, // 08 // pointer to ciphertext
                            0x3e, 0xad, 0x66, 0x91, 0x92, 0xc1, 0xe5, 0x7e, // 16
                            0x9a, 0x85, 0xc5, 0x1d, 0xba, 0x0f, 0x1c, 0x85, // 24
                            0x01, 0xd1, 0xce, 0x92, 0x27, 0x3f, 0x1c, 0xe7, // 32
                            0xe1, 0x40, 0xdc, 0xfa, 0xc9, 0x47, 0x57, 0xfa, // 40
                            0xbb, 0x12, 0x8c, 0xaa, 0xd1, 0x69, 0x12, 0xce, // 48
                            0xad, 0x06, 0x07};                              // 51
  const U8  corrPT[]     = {0xd6, 0x02, 0xc0, 0x6b, 0x94, 0x7a, 0xbe, 0x06, // 08 // pointer to correct plaintext blocks
                            0xcf, 0x6a, 0xa2, 0xc5, 0xc1, 0x56, 0x2e, 0x29, // 16
                            0x06, 0x2a, 0xd6, 0x22, 0x0d, 0xa9, 0xbc, 0x9c, // 24
                            0x25, 0xd6, 0x6a, 0x60, 0xbd, 0x85, 0xa8, 0x0d, // 32
                            0x4f, 0xbc, 0xc1, 0xfb, 0x49, 0x19, 0xb6, 0x56, // 40
                            0x6b, 0xe3, 0x5a, 0xf9, 0x81, 0x9a, 0xba, 0x83, // 48
                            0x6b, 0x8b, 0x47};                              // 51
  const U8  iv[]         = {0x0e, 0x39, 0x64, 0x46,                         // 04 // pointer to initialization vector
                            0x65, 0x55, 0x82, 0x83,                         // 08
                            0x8f, 0x27, 0xf7, 0x2f};                        // 12
  const U8  key[]        = {0x44, 0x33, 0xdb, 0x5f, 0xe0, 0x66, 0x96, 0x0b, // 08 // pointer to key material
                            0xdd, 0x4e, 0x1d, 0x4d, 0x41, 0x8b, 0x64, 0x1c, // 16
                            0x14, 0xbf, 0xce, 0xf9, 0xd5, 0x74, 0xe2, 0x9d, // 24
                            0xcd, 0x09, 0x95, 0x35, 0x28, 0x50, 0xf1, 0xeb};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x98};// 16
  const U8  tag[]        = {0x6a, 0x30, 0x0d, 0xbb, 0xad, 0x48, 0xef, 0x05, // 08 // pointer to correct Tag
                            0x68, 0x0e, 0x59, 0xb8, 0x4d, 0xee, 0x0b, 0x7c};// 16
  
  const U32 testSize = 51;
  
  U8  inputBlocks[GCM_BUF_SZ];                // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize + (2*BLK_SZ) + GCM_IV_SZ;
  
  memset(inputBlocks, ZERO, GCM_BUF_SZ);
  memset(plaintext, ZERO, GCM_BUF_SZ);
  
  /***** FORMAT CIPHERTEXT *****/
  memcpy(inputBlocks, iv, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  memcpy(inputBlocks+offset, ciphertext, testSize);
  offset += testSize;
  
  memcpy(inputBlocks+offset, sizeBlk, BLK_SZ);
  offset += BLK_SZ;
  
  memcpy(inputBlocks+offset, tag, BLK_SZ);
  
  /***** DECRYPT AND COMPARE RESULTS *****/
  EXPECT_EQ(FAIL, gcmPtr->GCM_Decrypt(inputBlocks, key, plaintext, size));
  EXPECT_EQ(SUCCESS, memcmp(plaintext, corrPT, size));
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT: " << std::dec << size << std::endl;
  PrintBlocks(plaintext, size);
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, Decrypt_CT408_FAIL_TAG)


TEST(GCM, EndToEnd_000){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine

  const U8  key[]        = {0xb5, 0x2c, 0x50, 0x5a, 0x37, 0xd7, 0x8e, 0xda, // 08 // pointer to key material
                            0x5d, 0xd3, 0x4f, 0x20, 0xc2, 0x25, 0x40, 0xea, // 16
                            0x1b, 0x58, 0x96, 0x3c, 0xf8, 0xe5, 0xbf, 0x8f, // 24
                            0xfa, 0x85, 0xf9, 0xf2, 0x49, 0x25, 0x05, 0xb4};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};// 16

  U8  ciphertext[GCM_BUF_SZ];                 // pointer to locations within ciphertext block
  U8  plaintext[GCM_BUF_SZ];                  // pointer to plaintext blocks
  U8  result[GCM_BUF_SZ];                     // pointer to result block
  U32 offset = ZERO;                      // copy offset
  U32 size = ZERO;                        // bytes of plaintext / ciphertext in this test
  
  memset(plaintext, ZERO, GCM_BUF_SZ);
  memset(result, ZERO, BLK_SZ);
  
  /***** ENCRYPT *****/
  gcmPtr->GCM_Encrypt(ciphertext, key, plaintext, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ((2*BLK_SZ) + GCM_IV_SZ, size);
  
  offset = GCM_IV_SZ;
  
  EXPECT_EQ(SUCCESS, memcmp(ciphertext+offset, sizeBlk, BLK_SZ));
  offset += BLK_SZ;
  
  /***** PRINT AES-256 GCM ENCRYPTION RESULTS *****/
  std::cout << "AES-256 GCM IV:" << std::endl;
  PrintVector(ciphertext, GCM_IV_SZ);
  offset = GCM_IV_SZ;
  
  std::cout << "AES-256 GCM LENGTH OF CIPHERTEXT:" << std::dec << size << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  offset += BLK_SZ;
  
  std::cout << "AES-256 GCM TAG:" << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  
  /***** DECRYPT *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(ciphertext, key, result, size));
  EXPECT_EQ(ZERO, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result, plaintext, size));
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, EndToEnd_000)


TEST(GCM, EndToEnd_104){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  const U8  key[]        = {0x82, 0xc4, 0xf1, 0x2e, 0xee, 0xc3, 0xb2, 0xd3, // 08 // pointer to key material
                            0xd1, 0x57, 0xb0, 0xf9, 0x92, 0xd2, 0x92, 0xb2, // 16
                            0x37, 0x47, 0x8d, 0x2c, 0xec, 0xc1, 0xd5, 0xf1, // 24
                            0x61, 0x38, 0x9b, 0x97, 0xf9, 0x99, 0x05, 0x7a};// 32
  const U8  plaintext[]  = {0x98, 0x2a, 0x29, 0x6e, 0xe1, 0xcd, 0x70, 0x86, // 08 // pointer to plaintext blocks
                            0xaf, 0xad, 0x97, 0x69, 0x45};                  // 13
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x68};// 16
  
  const U32 testSize = 13;

  U8  ciphertext[GCM_BUF_SZ];                 // pointer to locations within ciphertext block
  U8  result[GCM_BUF_SZ];                     // pointer to result block
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize;                    // bytes of plaintext / ciphertext in this test
  
  memset(result, ZERO, GCM_BUF_SZ);
  
  /***** ENCRYPT *****/
  gcmPtr->GCM_Encrypt(ciphertext, key, plaintext, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(testSize + (2*BLK_SZ) + GCM_IV_SZ, size);
  offset = GCM_IV_SZ;
  offset += testSize;
  
  EXPECT_EQ(SUCCESS, memcmp(ciphertext+offset, sizeBlk, BLK_SZ));
  offset += BLK_SZ;
  
  /***** PRINT AES-256 GCM ENCRYPTION RESULTS *****/
  std::cout << "AES-256 GCM IV:" << std::endl;
  PrintBlocks(ciphertext, BLK_SZ);
  offset = GCM_IV_SZ;
  
  std::cout << "AES-256 GCM CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext+offset, testSize);
  offset += testSize;
  
  std::cout << "AES-256 GCM LENGTH OF CIPHERTEXT:" << std::dec << size << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  offset += BLK_SZ;
  
  std::cout << "AES-256 GCM TAG:" << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  
  /***** DECRYPT *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(ciphertext, key, result, size));
  EXPECT_EQ(testSize, size);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT:" << std::dec << size << std::endl;
  PrintBlocks(result, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result, plaintext, size));
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, EndToEnd_104)


TEST(GCM, EndToEnd_128){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  const U8  key[]        = {0x31, 0xbd, 0xad, 0xd9, 0x66, 0x98, 0xc2, 0x04, // 08 // pointer to key material
                            0xaa, 0x9c, 0xe1, 0x44, 0x8e, 0xa9, 0x4a, 0xe1, // 16
                            0xfb, 0x4a, 0x9a, 0x0b, 0x3c, 0x9d, 0x77, 0x3b, // 24
                            0x51, 0xbb, 0x18, 0x22, 0x66, 0x6b, 0x8f, 0x22};// 32
  const U8  plaintext[]  = {0x2d, 0xb5, 0x16, 0x8e, 0x93, 0x25, 0x56, 0xf8, // 08 // pointer to plaintext blocks
                            0x08, 0x9a, 0x06, 0x22, 0x98, 0x1d, 0x01, 0x7d};// 16
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80};// 16
  
  const U32 testSize = 16;

  U8  ciphertext[GCM_BUF_SZ];                 // pointer to locations within ciphertext block
  U8  result[GCM_BUF_SZ];                     // pointer to result block
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize;                    // bytes of plaintext / ciphertext in this test
  
  memset(result, ZERO, GCM_BUF_SZ);
  
  /***** ENCRYPT *****/
  gcmPtr->GCM_Encrypt(ciphertext, key, plaintext, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(testSize + (2*BLK_SZ) + GCM_IV_SZ, size);
  offset = GCM_IV_SZ;
  offset += testSize;
  
  EXPECT_EQ(SUCCESS, memcmp(ciphertext+offset, sizeBlk, BLK_SZ));
  offset += BLK_SZ;
  
  /***** PRINT AES-256 GCM ENCRYPTION RESULTS *****/
  std::cout << "AES-256 GCM IV:" << std::endl;
  PrintVector(ciphertext, GCM_IV_SZ);
  offset = GCM_IV_SZ;
  
  std::cout << "AES-256 GCM CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext+offset, testSize);
  offset += testSize;
  
  std::cout << "AES-256 GCM LENGTH OF CIPHERTEXT:" << std::dec << size << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  offset += BLK_SZ;
  
  std::cout << "AES-256 GCM TAG:" << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  
  /***** DECRYPT *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(ciphertext, key, result, size));
  EXPECT_EQ(testSize, size);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT:" << std::dec << size << std::endl;
  PrintBlocks(result, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result, plaintext, size));
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, EndToEnd_128)


TEST(GCM, EndToEnd_256){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  const U8  key[]        = {0x26, 0x8e, 0xd1, 0xb5, 0xd7, 0xc9, 0xc7, 0x30, // 08 // pointer to key material
                            0x4f, 0x9c, 0xae, 0x5f, 0xc4, 0x37, 0xb4, 0xcd, // 16
                            0x3a, 0xeb, 0xe2, 0xec, 0x65, 0xf0, 0xd8, 0x5c, // 24
                            0x39, 0x18, 0xd3, 0xd3, 0xb5, 0xbb, 0xa8, 0x9b};// 32
  const U8  plaintext[]  = {0xfe, 0x29, 0xa4, 0x0d, 0x8e, 0xbf, 0x57, 0x26, // 08 // pointer to plaintext blocks
                            0x2b, 0xdb, 0x87, 0x19, 0x1d, 0x01, 0x84, 0x3f, // 16
                            0x4c, 0xa4, 0xb2, 0xde, 0x97, 0xd8, 0x82, 0x73, // 24
                            0x15, 0x4a, 0x0b, 0x7d, 0x9e, 0x2f, 0xdb, 0x80};// 32
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00};// 16
  
  const U32 testSize = 32;

  U8  ciphertext[GCM_BUF_SZ];                 // pointer to locations within ciphertext block
  U8  result[GCM_BUF_SZ];                     // pointer to result block
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize;                    // bytes of plaintext / ciphertext in this test
  
  memset(result, ZERO, GCM_BUF_SZ);
  
  /***** ENCRYPT *****/
  gcmPtr->GCM_Encrypt(ciphertext, key, plaintext, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(testSize + (2*BLK_SZ) + GCM_IV_SZ, size);
  offset = GCM_IV_SZ;
  offset += testSize;
  
  EXPECT_EQ(SUCCESS, memcmp(ciphertext+offset, sizeBlk, BLK_SZ));
  offset += BLK_SZ;
  
  /***** PRINT AES-256 GCM ENCRYPTION RESULTS *****/
  std::cout << "AES-256 GCM IV:" << std::endl;
  PrintVector(ciphertext, GCM_IV_SZ);
  offset = GCM_IV_SZ;
  
  std::cout << "AES-256 GCM CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext+offset, testSize);
  offset += testSize;
  
  std::cout << "AES-256 GCM LENGTH OF CIPHERTEXT:" << std::dec << size << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  offset += BLK_SZ;
  
  std::cout << "AES-256 GCM TAG:" << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  
  /***** DECRYPT *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(ciphertext, key, result, size));
  EXPECT_EQ(testSize, size);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT:" << std::dec << size << std::endl;
  PrintBlocks(result, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result, plaintext, size));
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, EndToEnd_256)


TEST(GCM, EndToEnd_408){
  /***** SETUP *****/
  GCM *gcmPtr = new GCM();                // pointer to GCM encryption engine
  
  const U8  key[]        = {0x1f, 0xde, 0xd3, 0x2d, 0x59, 0x99, 0xde, 0x4a, // 08 // pointer to key material
                            0x76, 0xe0, 0xf8, 0x08, 0x21, 0x08, 0x82, 0x3a, // 16
                            0xef, 0x60, 0x41, 0x7e, 0x18, 0x96, 0xcf, 0x42, // 24
                            0x18, 0xa2, 0xfa, 0x90, 0xf6, 0x32, 0xec, 0x8a};// 32
  const U8  plaintext[]  = {0x06, 0xb2, 0xc7, 0x58, 0x53, 0xdf, 0x9a, 0xeb, // 08 // pointer to plaintext blocks
                            0x17, 0xbe, 0xfd, 0x33, 0xce, 0xa8, 0x1c, 0x63, // 16
                            0x0b, 0x0f, 0xc5, 0x36, 0x67, 0xff, 0x45, 0x19, // 24
                            0x9c, 0x62, 0x9c, 0x8e, 0x15, 0xdc, 0xe4, 0x1e, // 32
                            0x53, 0x0a, 0xa7, 0x92, 0xf7, 0x96, 0xb8, 0x13, // 40
                            0x8e, 0xea, 0xb2, 0xe8, 0x6c, 0x7b, 0x7b, 0xee, // 48
                            0x1d, 0x40, 0xb0};                              // 51
  const U8  sizeBlk[]    = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, // 08 // pointer to size block
                            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x98};// 16
  
  const U32 testSize = 51;

  U8  ciphertext[GCM_BUF_SZ];                 // pointer to locations within ciphertext block
  U8  result[GCM_BUF_SZ];                     // pointer to result block
  U32 offset = ZERO;                      // copy offset
  U32 size = testSize;                    // bytes of plaintext / ciphertext in this test
  
  memset(result, ZERO, GCM_BUF_SZ);
  
  /***** ENCRYPT *****/
  gcmPtr->GCM_Encrypt(ciphertext, key, plaintext, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(testSize + (2*BLK_SZ) + GCM_IV_SZ, size);
  offset = GCM_IV_SZ;
  offset += testSize;
  
  EXPECT_EQ(SUCCESS, memcmp(ciphertext+offset, sizeBlk, BLK_SZ));
  offset += BLK_SZ;
  
  /***** PRINT AES-256 GCM ENCRYPTION RESULTS *****/
  std::cout << "AES-256 GCM IV:" << std::endl;
  PrintVector(ciphertext, GCM_IV_SZ);
  offset = GCM_IV_SZ;
  
  std::cout << "AES-256 GCM CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext+offset, testSize);
  offset += testSize;
  
  std::cout << "AES-256 GCM LENGTH OF CIPHERTEXT:" << std::dec << size << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  offset += BLK_SZ;
  
  std::cout << "AES-256 GCM TAG:" << std::endl;
  PrintBlocks(ciphertext+offset, BLK_SZ);
  
  /***** DECRYPT *****/
  EXPECT_EQ(SUCCESS, gcmPtr->GCM_Decrypt(ciphertext, key, result, size));
  EXPECT_EQ(testSize, size);
  
  /***** PRINT AES-256 GCM DECRYPTION RESULTS *****/
  std::cout << "AES-256 GCM LENGTH OF PLAINTEXT:" << std::dec << size << std::endl;
  PrintBlocks(result, size);
  
  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(result, plaintext, size));
  
  /***** TEARDOWN *****/
  if(gcmPtr){
    delete gcmPtr;
    gcmPtr = nullptr;
  }
} // (GCM, EndToEnd_408)

#endif // TEST_GCM_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
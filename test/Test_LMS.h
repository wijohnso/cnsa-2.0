/**
 ** @file: Test_LMS.h
 ** @brief:    header file for testing LMS
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <string>

#include "IO.h"
#include "LMS.h"

#ifndef TEST_LMS_H
#define TEST_LMS_H

TEST(LMS, MakeSeed){
  /***** SETUP *****/
  U8  digestBuf[]  = {0x24, 0x8d, 0x6a, 0x61, 0xd2, 0x06, 0x38, 0xb8, // 08
                      0xe5, 0xc0, 0x26, 0x93, 0x0c, 0x3e, 0x60, 0x39, // 16
                      0xA3, 0x3C, 0xE4, 0x59, 0x64, 0xFF, 0x21, 0x67, // 24
                      0xF6, 0xEC, 0xED, 0xD4, 0x19, 0xDB, 0x06, 0xC1};// 32
  
  BUF digest = {SHA_256_DGST_SZ, digestBuf};
  
  std::string stateFile = "file.txt";
  std::string keyFile = "key.txt";
  
  LMS *lmsPtr = new LMS();                  // pointer to LMS engine
  
  lmsPtr->Initialize(keyFile, stateFile);
  
  for(int i=ZERO; i<16; i++){
    if(lmsPtr->Sign(keyFile) == FAIL){
      std::cout << "keystore depleted at key " << i << std::endl;
    }
  }
  
  if(lmsPtr){
    delete lmsPtr;
    lmsPtr = nullptr;
  }

} // 


#endif // TEST_LMS_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/**
 ** @file: Test_DRBG.h
 ** @brief:    header file for testing Deterministic Random
 **            bit generator (DRBG)
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "DRBG.h"
#include "IO.h"

#ifndef TEST_DRBG_H
#define TEST_DRBG_H

// universal DRBG constants
#define      SEED_LIM     32        // reseed limit for DRBG

TEST(DRBG, Generate_12){
  /***** SETUP *****/
  DRBG *drbgPtr = new DRBG();             // pointer to DRBG engine
  
  const U32 ONETWO = 12;
  
  U8  randomData[ONETWO];              // pointer to random data
  U8  zeros[ONETWO];                   // pointer to zeros (for comparison)
  
  memset(zeros, ZERO, ONETWO);
  
  for(U32 i=ZERO; i<SEED_LIM; i++){
    /***** GENERATE AND COMPARE RESULTS *****/
    memset(randomData, ZERO, ONETWO);
    EXPECT_EQ(SUCCESS, drbgPtr->Generate(randomData, ONETWO));
    
    /***** PRINT RANDOM DATA *****/
    std::cout << "12 BYTES DRBG RANDOM DATA:" << std::endl;
    PrintVector(randomData, ONETWO);
  }
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, ONETWO);
  EXPECT_EQ(FAIL, drbgPtr->Generate(randomData, ONETWO));
  EXPECT_EQ(SUCCESS, memcmp(randomData, zeros, ONETWO));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "12 BYTES DRBG ZERO DATA:" << std::endl;
  PrintVector(randomData, ONETWO);
  
  /**** RESEED *****/
  drbgPtr->Reseed();
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, ONETWO);
  EXPECT_EQ(SUCCESS, drbgPtr->Generate(randomData, ONETWO));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "12 BYTES DRBG RANDOM DATA:" << std::endl;
  PrintVector(randomData, ONETWO);
  
  /***** TEARDOWN *****/
  if(drbgPtr){
    delete drbgPtr;
    drbgPtr = nullptr;
  }
} // (DRBG, Generate_12)


TEST(DRBG, Generate_16){
  /***** SETUP *****/
  DRBG *drbgPtr = new DRBG();             // pointer to DRBG engine
  
  const U32 ONESIX = 16;
  
  U8  randomData[ONESIX];                // pointer to random data
  U8  zeros[ONESIX];                     // pointer to zeros (for comparison)
  
  memset(zeros, ZERO, ONESIX);
  
  for(U32 i=ZERO; i<SEED_LIM; i++){
    /***** GENERATE AND COMPARE RESULTS *****/
    memset(randomData, ZERO, ONESIX);
    EXPECT_EQ(SUCCESS, drbgPtr->Generate(randomData, ONESIX));
    
    /***** PRINT RANDOM DATA *****/
    std::cout << "16 BYTES DRBG RANDOM DATA:" << std::endl;
    PrintVector(randomData, ONESIX);
  }
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, ONESIX);
  EXPECT_EQ(FAIL, drbgPtr->Generate(randomData, ONESIX));
  EXPECT_EQ(SUCCESS, memcmp(randomData, zeros, ONESIX));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "16 BYTES DRBG ZERO DATA:" << std::endl;
  PrintVector(randomData, ONESIX);
  
  /**** RESEED *****/
  drbgPtr->Reseed();
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, ONESIX);
  EXPECT_EQ(SUCCESS, drbgPtr->Generate(randomData, ONESIX));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "16 BYTES DRBG RANDOM DATA:" << std::endl;
  PrintVector(randomData, ONESIX);
  
  /***** TEARDOWN *****/
  if(drbgPtr){
    delete drbgPtr;
    drbgPtr = nullptr;
  }
} // (DRBG, Generate_16)


TEST(DRBG, Generate_32){
  /***** SETUP *****/
  DRBG *drbgPtr = new DRBG();             // pointer to DRBG engine
  
  const U32 THREE_TWO = 32;
  
  U8  randomData[THREE_TWO];            // pointer to random data
  U8  zeros[THREE_TWO];                 // pointer to zeros (for comparison)
  
  memset(zeros, ZERO, THREE_TWO);
  
  for(U32 i=ZERO; i<SEED_LIM; i++){
    /***** GENERATE AND COMPARE RESULTS *****/
    memset(randomData, ZERO, THREE_TWO);
    EXPECT_EQ(SUCCESS, drbgPtr->Generate(randomData, THREE_TWO));
    
    /***** PRINT RANDOM DATA *****/
    std::cout << "32 BYTES DRBG RANDOM DATA:" << std::endl;
    PrintBlocks(randomData, THREE_TWO);
  }
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, THREE_TWO);
  EXPECT_EQ(FAIL, drbgPtr->Generate(randomData, THREE_TWO));
  EXPECT_EQ(SUCCESS, memcmp(randomData, zeros, THREE_TWO));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "32 BYTES DRBG ZERO DATA:" << std::endl;
  PrintBlocks(randomData, THREE_TWO);
  
  /**** RESEED *****/
  drbgPtr->Reseed();
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, THREE_TWO);
  EXPECT_EQ(SUCCESS, drbgPtr->Generate(randomData, THREE_TWO));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "32 BYTES DRBG RANDOM DATA:" << std::endl;
  PrintBlocks(randomData, THREE_TWO);
  
  /***** TEARDOWN *****/
  if(drbgPtr){
    delete drbgPtr;
    drbgPtr = nullptr;
  }
} // (DRBG, Generate_32)


TEST(DRBG, Generate_64){
  /***** SETUP *****/
  DRBG *drbgPtr = new DRBG();             // pointer to DRBG engine
  
  const U32 SIX_FOUR = 64;
  
  U8  randomData[SIX_FOUR];               // pointer to random data
  U8  zeros[SIX_FOUR];                    // pointer to zeros (for comparison)
  
  memset(zeros, ZERO, SIX_FOUR);
  
  for(U32 i=ZERO; i<SEED_LIM; i++){
    /***** GENERATE AND COMPARE RESULTS *****/
    memset(randomData, ZERO, SIX_FOUR);
    EXPECT_EQ(SUCCESS, drbgPtr->Generate(randomData, SIX_FOUR));
    
    /***** PRINT RANDOM DATA *****/
    std::cout << "64 BYTES DRBG RANDOM DATA:" << std::endl;
    PrintBlocks(randomData, SIX_FOUR);
  }
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, SIX_FOUR);
  EXPECT_EQ(FAIL, drbgPtr->Generate(randomData, SIX_FOUR));
  EXPECT_EQ(SUCCESS, memcmp(randomData, zeros, SIX_FOUR));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "64 BYTES DRBG ZERO DATA:" << std::endl;
  PrintBlocks(randomData, SIX_FOUR);
  
  /**** RESEED *****/
  drbgPtr->Reseed();
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, SIX_FOUR);
  EXPECT_EQ(SUCCESS, drbgPtr->Generate(randomData, SIX_FOUR));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "64 BYTES DRBG RANDOM DATA:" << std::endl;
  PrintBlocks(randomData, SIX_FOUR);
  
  /***** TEARDOWN *****/
  if(drbgPtr){
    delete drbgPtr;
    drbgPtr = nullptr;
  }
} // (DRBG, Generate_64)


TEST(DRBG, Generate_FAIL_SIZE){
  /***** SETUP *****/
  DRBG *drbgPtr = new DRBG();             // pointer to DRBG engine
  
  const U32 SIX_FIVE = 65;
  
  U8  randomData[SIX_FIVE];               // pointer to random data
  U8  zeros[SIX_FIVE];                    // pointer to zeros (for comparison)
  
  memset(zeros, ZERO, SIX_FIVE);
  
  /***** GENERATE AND COMPARE RESULTS *****/
  memset(randomData, ZERO, SIX_FIVE);
  EXPECT_EQ(FAIL, drbgPtr->Generate(randomData, SIX_FIVE));
  
  /***** PRINT RANDOM DATA *****/
  std::cout << "DRBG ZERO DATA:" << std::endl;
  PrintBlocks(randomData, SIX_FIVE);
  
  /***** TEARDOWN *****/
  if(drbgPtr){
    delete drbgPtr;
    drbgPtr = nullptr;
  }
} // (DRBG, Generate_FAIL_SIZE)

#endif // TEST_DRBG_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
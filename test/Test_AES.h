/**
 ** @file: Test_AES.h
 ** @brief:    header file for testing AES-256 encryption
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "AES.h"
#include "IO.h"

#ifndef TEST_AES_H
#define TEST_AES_H

// test constants
#define BLK_SZ        16      // AES block size in bytes
#define AES_BUF_SZ    128     // test buffer size in bytes

TEST(AES, EndToEnd){
  /***** SETUP *****/
  AES *aesPtr    = new AES;               // AES-256 encryption engine
  
  const U8  corrCipher[] = {0xf3, 0xee, 0xd1, 0xbd, 0xb5, 0xd2, 0xa0, 0x3c, // 08 // AES-256 correctly encrypted blocks
                            0x06, 0x4b, 0x5a, 0x7e, 0x3d, 0xb1, 0x81, 0xf8, // 16
                            0x59, 0x1c, 0xcb, 0x10, 0xd4, 0x10, 0xed, 0x26, // 24
                            0xdc, 0x5b, 0xa7, 0x4a, 0x31, 0x36, 0x28, 0x70, // 32
                            0xb6, 0xed, 0x21, 0xb9, 0x9c, 0xa6, 0xf4, 0xf9, // 40
                            0xf1, 0x53, 0xe7, 0xb1, 0xbe, 0xaf, 0xed, 0x1d, // 48
                            0x23, 0x30, 0x4b, 0x7a, 0x39, 0xf9, 0xf3, 0xff, // 56
                            0x06, 0x7d, 0x8d, 0x8f, 0x9e, 0x24, 0xec, 0xc7};// 64
  const U8  key[]        = {0x60, 0x3D, 0xEB, 0x10, 0x15, 0xCA, 0x71, 0xBE, // 08 // pointer to key
                            0x2B, 0x73, 0xAE, 0xF0, 0x85, 0x7D, 0x77, 0x81, // 16
                            0x1F, 0x35, 0x2C, 0x07, 0x3B, 0x61, 0x08, 0xD7, // 24
                            0x2D, 0x98, 0x10, 0xA3, 0x09, 0x14, 0xDF, 0xF4};// 32
        U8  plaintext[]  = {0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9F, 0x96, // 08 // pointer to plaintext block
                            0xE9, 0x3D, 0x7E, 0x11, 0x73, 0x93, 0x17, 0x2A, // 16
                            0xAE, 0x2D, 0x8A, 0x57, 0x1E, 0x03, 0xAC, 0x9C, // 24
                            0x9E, 0xB7, 0x6F, 0xAC, 0x45, 0xAF, 0x8E, 0x51, // 32
                            0x30, 0xC8, 0x1C, 0x46, 0xA3, 0x5C, 0xE4, 0x11, // 40
                            0xE5, 0xFB, 0xC1, 0x19, 0x1A, 0x0A, 0x52, 0xEF, // 48
                            0xF6, 0x9F, 0x24, 0x45, 0xDF, 0x4F, 0x9B, 0x17, // 56
                            0xAD, 0x2B, 0x41, 0x7B, 0xE6, 0x6C, 0x37, 0x10};// 64

  U8  ciphertext[AES_BUF_SZ];                 // pointer to ciphertext block
  U8  *ciphertextIndex;                   // pointer to locations within ciphertext block
  U8  *plaintextIndex;                    // pointer to locations within plaintext block
  U8  result[AES_BUF_SZ];                     // pointer to result block
  U8  *resultIndex;                       // pointer to locations within result block
  const U8  size = 64;                    // bytes of plaintext / ciphertext in this test
  
  memset(result, ZERO, AES_BUF_SZ);
  
  /***** AES-256 KEY *****/
  aesPtr->SetKey(key);
  
  /***** AES-256 ENCRYPT *****/
  for(int i=0; i<size; i+=BLK_SZ){
    plaintextIndex = plaintext+i;
    ciphertextIndex = ciphertext+i;
    aesPtr->Encrypt(ciphertextIndex, plaintextIndex);
    
    /***** COMPARE RESULTS *****/
    EXPECT_EQ(SUCCESS, memcmp(ciphertextIndex, corrCipher+i, BLK_SZ));
  }

  /***** PRINT AES-256 ENCRYPTION RESULTS *****/
  std::cout << "AES-256 ENCRYPTED CIPHERTEXT:" << std::endl;
  PrintBlocks(ciphertext, size);
  
  /***** AES-256 DECRYPT *****/
  for(int i=0; i<size; i+=BLK_SZ){
    ciphertextIndex = ciphertext+i;
    resultIndex = result+i;
    aesPtr->Decrypt(ciphertextIndex, resultIndex);
    
    /***** COMPARE RESULTS *****/
    EXPECT_EQ(SUCCESS, memcmp(resultIndex, plaintext+i, BLK_SZ));
  }
  
  /***** TEARDOWN *****/
  if(aesPtr){
    delete aesPtr;
    aesPtr = nullptr;
  }
}

#endif // TEST_AES_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
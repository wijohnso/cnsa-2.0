/**
 ** @file: Test_SHA.h
 ** @brief:    header file for testing SHA-512 and SHA-512/256
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "SHA.h"
#include "IO.h"

#ifndef TEST_SHA_H
#define TEST_SHA_H

TEST(SHA, 256_192_Long){
  /***** SETUP *****/
  SHA *shatPtr = new SHA(TRNC);           // SHA-512/256 hash engine
  
  const U8  corrHash[] = {0x24, 0x8d, 0x6a, 0x61, 0xd2, 0x06, 0x38, 0xb8, // 08 // correct hash value
                          0xe5, 0xc0, 0x26, 0x93, 0x0c, 0x3e, 0x60, 0x39, // 16
                          0xA3, 0x3C, 0xE4, 0x59, 0x64, 0xFF, 0x21, 0x67};// 24
  const U8  input[]  = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";

  U8  digestPtr[SHA_192_DGST_SZ];         // returned hash digest

  /***** SHA-512/256 HASH *****/
  shatPtr->SHA256(input, digestPtr, static_cast<U64>(strlen((char *)input)));

  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(digestPtr, corrHash, SHA_192_DGST_SZ));

  /***** PRINT SHA-512/256 DIGEST *****/
  std::cout << "SHA-256/192 DIGEST:" << std::endl;
  PrintBlocks(digestPtr, SHA_192_DGST_SZ);
  
  /***** TEARDOWN *****/
  if(shatPtr){
    delete shatPtr;
    shatPtr = nullptr;
  }
} // (SHA, 256_192_Long)

TEST(SHA, 256_192_Short){
  /***** SETUP *****/
  SHA *shatPtr = new SHA(TRNC);           // SHA-512/256 hash engine
  
  const U8  corrHash[] = {0xba, 0x78, 0x16, 0xbf, 0x8f, 0x01, 0xcf, 0xea, // 08 // correct hash value 
                          0x41, 0x41, 0x40, 0xde, 0x5d, 0xae, 0x22, 0x23, // 16
                          0xb0, 0x03, 0x61, 0xa3, 0x96, 0x17, 0x7a, 0x9c};// 24
    const U8  input[]  = "abc";
    
    U8  digestPtr[SHA_192_DGST_SZ];       // returned hash digest
    
    /***** SHA-512/256 HASH *****/
    shatPtr->SHA256(input, digestPtr, static_cast<U64>(strlen((char *)input)));
    
    /***** COMPARE RESULTS *****/
    EXPECT_EQ(SUCCESS, memcmp(digestPtr, corrHash, SHA_192_DGST_SZ));
    
    /***** PRINT SHA-512/256 DIGEST *****/
    std::cout << "SHA-256/192 DIGEST:" << std::endl;
    PrintBlocks(digestPtr, SHA_192_DGST_SZ);
    
    /***** TEARDOWN *****/
    if(shatPtr){
      delete shatPtr;
      shatPtr = nullptr;
    }
} // (SHA, 256_192_Short)

TEST(SHA, 256_Long){
  /***** SETUP *****/
  SHA *shatPtr = new SHA(NORM);           // SHA-512/256 hash engine
  
  const U8  corrHash[] = {0x24, 0x8d, 0x6a, 0x61, 0xd2, 0x06, 0x38, 0xb8, // 08 // correct hash value
                          0xe5, 0xc0, 0x26, 0x93, 0x0c, 0x3e, 0x60, 0x39, // 16
                          0xA3, 0x3C, 0xE4, 0x59, 0x64, 0xFF, 0x21, 0x67, // 24
                          0xF6, 0xEC, 0xED, 0xD4, 0x19, 0xDB, 0x06, 0xC1};// 32
  const U8  input[]  = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";

  U8  digestPtr[SHA_256_DGST_SZ];         // returned hash digest

  /***** SHA-512/256 HASH *****/
  shatPtr->SHA256(input, digestPtr, static_cast<U64>(strlen((char *)input)));

  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(digestPtr, corrHash, SHA_256_DGST_SZ));

  /***** PRINT SHA-512/256 DIGEST *****/
  std::cout << "SHA-256 DIGEST:" << std::endl;
  PrintBlocks(digestPtr, SHA_256_DGST_SZ);
  
  /***** TEARDOWN *****/
  if(shatPtr){
    delete shatPtr;
    shatPtr = nullptr;
  }
} // (SHA, 256_Long)

TEST(SHA, 256_Short){
  /***** SETUP *****/
  SHA *shatPtr = new SHA(NORM);           // SHA-512/256 hash engine
  
  const U8  corrHash[] = {0xba, 0x78, 0x16, 0xbf, 0x8f, 0x01, 0xcf, 0xea, // 08 // correct hash value 
                          0x41, 0x41, 0x40, 0xde, 0x5d, 0xae, 0x22, 0x23, // 16
                          0xb0, 0x03, 0x61, 0xa3, 0x96, 0x17, 0x7a, 0x9c, // 24
                          0xb4, 0x10, 0xff, 0x61, 0xf2, 0x00, 0x15, 0xad};// 32
    const U8  input[]  = "abc";
    
    U8  digestPtr[SHA_256_DGST_SZ];       // returned hash digest
    
    /***** SHA-512/256 HASH *****/
    shatPtr->SHA256(input, digestPtr, static_cast<U64>(strlen((char *)input)));
    
    /***** COMPARE RESULTS *****/
    EXPECT_EQ(SUCCESS, memcmp(digestPtr, corrHash, SHA_256_DGST_SZ));
    
    /***** PRINT SHA-512/256 DIGEST *****/
    std::cout << "SHA-256 DIGEST:" << std::endl;
    PrintBlocks(digestPtr, SHA_256_DGST_SZ);
    
    /***** TEARDOWN *****/
    if(shatPtr){
      delete shatPtr;
      shatPtr = nullptr;
    }
} // (SHA, 256_Short)

TEST(SHA, 512_256_Long){
  /***** SETUP *****/
  SHA *shatPtr = new SHA(TRNC);           // SHA-512/256 hash engine
  
  const U8  corrHash[] = {0x39, 0x28, 0xe1, 0x84, 0xfb, 0x86, 0x90, 0xf8, // 08 // correct hash value
                          0x40, 0xda, 0x39, 0x88, 0x12, 0x1d, 0x31, 0xbe, // 16
                          0x65, 0xcb, 0x9d, 0x3e, 0xf8, 0x3e, 0xe6, 0x14, // 24
                          0x6f, 0xea, 0xc8, 0x61, 0xe1, 0x9b, 0x56, 0x3a};// 32
  const U8  input[]  = "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu";

  U8  digestPtr[SHA_256_DGST_SZ];         // returned hash digest

  /***** SHA-512/256 HASH *****/
  shatPtr->SHA512(input, digestPtr, static_cast<U64>(strlen((char *)input)));

  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(digestPtr, corrHash, SHA_256_DGST_SZ));

  /***** PRINT SHA-512/256 DIGEST *****/
  std::cout << "SHA-512/256 DIGEST:" << std::endl;
  PrintBlocks(digestPtr, SHA_256_DGST_SZ);
  
  /***** TEARDOWN *****/
  if(shatPtr){
    delete shatPtr;
    shatPtr = nullptr;
  }
} // (SHA, 512_256_Long)


TEST(SHA, 512_256_Short){
  /***** SETUP *****/
  SHA *shatPtr = new SHA(TRNC);           // SHA-512/256 hash engine
  
  const U8  corrHash[] = {0x53, 0x04, 0x8e, 0x26, 0x81, 0x94, 0x1e, 0xf9, // 08 // correct hash value
                          0x9b, 0x2e, 0x29, 0xb7, 0x6b, 0x4c, 0x7d, 0xab, // 16
                          0xe4, 0xc2, 0xd0, 0xc6, 0x34, 0xfc, 0x6d, 0x46, // 24
                          0xe0, 0xe2, 0xf1, 0x31, 0x07, 0xe7, 0xaf, 0x23};// 32
    const U8  input[]  = "abc";
    
    U8  digestPtr[SHA_256_DGST_SZ];       // returned hash digest
    
    /***** SHA-512/256 HASH *****/
    shatPtr->SHA512(input, digestPtr, static_cast<U64>(strlen((char *)input)));
    
    /***** COMPARE RESULTS *****/
    EXPECT_EQ(SUCCESS, memcmp(digestPtr, corrHash, SHA_256_DGST_SZ));
    
    /***** PRINT SHA-512/256 DIGEST *****/
    std::cout << "SHA-512/256 DIGEST:" << std::endl;
    PrintBlocks(digestPtr, SHA_256_DGST_SZ);
    
    /***** TEARDOWN *****/
    if(shatPtr){
      delete shatPtr;
      shatPtr = nullptr;
    }
} // (SHA, 512_256_Short)


TEST(SHA, 512_Long){
  /***** SETUP *****/
  SHA *shanPtr = new SHA(NORM);           // SHA-512 hash engine
  
  const U8  corrHash[] = {0x8e, 0x95, 0x9b, 0x75, 0xda, 0xe3, 0x13, 0xda, // 08 // correct hash value
                          0x8c, 0xf4, 0xf7, 0x28, 0x14, 0xfc, 0x14, 0x3f, // 16
                          0x8f, 0x77, 0x79, 0xc6, 0xeb, 0x9f, 0x7f, 0xa1, // 24
                          0x72, 0x99, 0xae, 0xad, 0xb6, 0x88, 0x90, 0x18, // 32
                          0x50, 0x1d, 0x28, 0x9e, 0x49, 0x00, 0xf7, 0xe4, // 40
                          0x33, 0x1b, 0x99, 0xde, 0xc4, 0xb5, 0x43, 0x3a, // 48
                          0xc7, 0xd3, 0x29, 0xee, 0xb6, 0xdd, 0x26, 0x54, // 56
                          0x5e, 0x96, 0xe5, 0x5b, 0x87, 0x4b, 0xe9, 0x09};// 64
  const U8  input[]  = "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu";
  
  U8  digestPtr[SHA_512_DGST_SZ];         // returned hash digest
  
  /***** SHA-512 HASH *****/
  shanPtr->SHA512(input, digestPtr, static_cast<U64>(strlen((char *)input)));

  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(digestPtr, corrHash, SHA_512_DGST_SZ));

  /***** PRINT SHA-512 DIGEST *****/
  std::cout << "SHA-512 DIGEST:" << std::endl;
  PrintBlocks(digestPtr, SHA_512_DGST_SZ);

  /***** TEARDOWN *****/
  if(shanPtr){
    delete shanPtr;
    shanPtr = nullptr;
  }
} // (SHA, 512_Long)


TEST(SHA, 512_Short){
  /***** SETUP *****/
  SHA *shanPtr = new SHA(NORM);           // SHA-512 hash engine
  
  const U8  corrHash[] = {0xdd, 0xaf, 0x35, 0xa1, 0x93, 0x61, 0x7a, 0xba, // 08 // correct hash value
                          0xcc, 0x41, 0x73, 0x49, 0xae, 0x20, 0x41, 0x31, // 16
                          0x12, 0xe6, 0xfa, 0x4e, 0x89, 0xa9, 0x7e, 0xa2, // 24
                          0x0a, 0x9e, 0xee, 0xe6, 0x4b, 0x55, 0xd3, 0x9a, // 32
                          0x21, 0x92, 0x99, 0x2a, 0x27, 0x4f, 0xc1, 0xa8, // 40
                          0x36, 0xba, 0x3c, 0x23, 0xa3, 0xfe, 0xeb, 0xbd, // 48
                          0x45, 0x4d, 0x44, 0x23, 0x64, 0x3c, 0xe8, 0x0e, // 56
                          0x2a, 0x9a, 0xc9, 0x4f, 0xa5, 0x4c, 0xa4, 0x9f};// 64
  const U8  input[]  = "abc";
  
  U8  digestPtr[SHA_512_DGST_SZ];         // returned hash digest
  
  /***** SHA-512 HASH *****/
  shanPtr->SHA512(input, digestPtr, static_cast<U64>(strlen((char *)input)));

  /***** COMPARE RESULTS *****/
  EXPECT_EQ(SUCCESS, memcmp(digestPtr, corrHash, SHA_512_DGST_SZ));

  /***** PRINT SHA-512 DIGEST *****/
  std::cout << "SHA-512 DIGEST:" << std::endl;
  PrintBlocks(digestPtr, SHA_512_DGST_SZ);

  /***** TEARDOWN *****/
  if(shanPtr){
    delete shanPtr;
    shanPtr = nullptr;
  }
} // (SHA, 512_Short)

#endif // TEST_SHA_H

/**
/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
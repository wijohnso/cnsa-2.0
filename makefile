############################################################################## #
# @Filename : makefile
# @brief : builds objects AES, CBC, DRBG, GCM, HMAC, IO, KDF, and SHA
# builds executable driver Crypto
# optionally runs executable driver Crypto
# optionally runs executable driver Crypto with valgrind to test
# for memory leaks
#
# @copyright(C) 2024, Wil Johnson, All rights reserved
#
# This program is free software; you can redistribute it and / or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU General Public License for more details.
#
###############################################################################
#target name
TARGET := Crypto

#object(class) file names(without suffix)
OBJECTS := AES CBC DRBG GCM HMAC IO KDF LMS SHA

# maintain a directory structure as follows:
#	./bin		includes the target executable
#			(created and deleted by this makefile)
#
#	./include	includes all header (.h) files
#
#	./lib		includes all library (.a) files
#
#	./obj		includes build artifact object (.o) files
#			(created and deleted by this makefile)
#
#	./src		includes all c++ source (.cc) files
#
# recipes are as follows:
#	make (all)	creates the base executable with verbose compiler
#			warning and error output
#
#	make debug	creates the base executable with verbose compiler
#			warning and error output; defines DEBUG_OUTPUT in
#			target executable
#
#	make gtest	creates the gtest-all and gtest_main static libraries
#
#	make clean	removes build artifact objects and binaries
#
#	make run	runs the target executable
#
#	make test	runs the target executable while checking for memory
#			leaks
#
# Code below this line is generic and should not be modified
###############################################################################
BIN := ./bin
INC := ./include
LIB := ./lib
OBJ := ./obj
REP := ./rep
RND := ./rand
SRC := ./src
TST := ./test

TARG := $(addprefix $(BIN)/,$(TARGET))
OBJS := $(addsuffix .o, $(addprefix $(OBJ)/,$(OBJECTS))) $(addsuffix .o, $(addprefix $(OBJ)/,$(TARGET)))

GTEST := gtest-all gtest_main
LIBS := $(addsuffix .a, $(addprefix $(LIB)/,$(GTEST)))

CXX := clang++
CXX_FLAGS := -std=c++17 -pedantic -Wall -Wextra -Werror -Wshadow -Wsign-conversion -O2
CLANG_FLAGS := -fprofile-instr-generate -fcoverage-mapping
CPP_FLAGS := -isystem $(INC) -isystem $(TST)

all : clean mkdirs $(TARG)

debug: CXX_FLAGS += -DDEBUG_OUTPUT
debug: CXX_FLAGS += -g -v
debug: all

gtest : $(LIBS)

$(OBJ)/%.o : $(SRC)/$(SRC)/%.cc
	$(CXX) $(CPP_FLAGS) -I$(INC) -I$(SRC) -I$(TST) $(CXX_FLAGS) -c $< -o $@

$(OBJ)/%.o : $(SRC)/%.cc
	$(CXX) $(CPP_FLAGS) $(CXX_FLAGS) -c $< -o $@

$(LIB)/%.a : $(OBJ)/%.o
	$(AR) $(ARFLAGS) $@ $^

$(TARG) : $(OBJS) $(LIBS)
	$(CXX) $(CPP_FLAGS) $(CXX_FLAGS) -pthread $^ -o $@

clean :
	#cleaning build artifacts
	@-rm -r $(BIN) $(OBJ) $(REP) $(RND) default.profraw 

mkdirs :
	#creating directory structure
	@-mkdir $(BIN) $(LIB) $(OBJ) $(RND)
	@-clear

coverage: CXX_FLAGS += $(CLANG_FLAGS)
coverage : clean mkdirs $(TARG)
	@-LLVM_PROFILE_FILE=$(REP)/report.raw $(TARG)
	@-llvm-profdata merge -sparse $(REP)/report.raw -o $(REP)/report
	@-llvm-cov show $(TARG) -instr-profile=$(REP)/report > $(REP)/report.txt -show-line-counts-or-regions
	@-llvm-cov report $(TARG) -instr-profile=$(REP)/report

run:
	$(TARG) --gtest_filter=LMS*
test :
	valgrind --tool=memcheck --leak-check=full --track-origins=yes -s $(TARG) --gtest_filter=LMS*

.PHONY: all debug coverage run test

# TODO: Add @pre and @post to all public function declarations

# TODO: change IO to Utils

# TODO: change GCM::_resultPtr to BUF

# TODO: DRBG Test Vectors
	
# TODO: implement LMS (Leighton-Micali Signatures) for firmware signing (SP 800-208)
# TODO: implement Crystals-Kyber for key exchange
# TODO: implement Crystals-Dilithium for digital signatures

# TODO: implement histogram to verivy encryption

###############################################################################
# @copyright(C) 2024, Wil Johnson, All rights reserved
#
# This program is free software; you can redistribute it and / or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU General Public License for more details.
#
###############################################################################
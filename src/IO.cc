/**
 ** @file: IO.cc
 ** @brief:    implementation file for file and console input and output
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "IO.h"

#define BYTS_PER_BLK      128       // bytes per block (for printing)

/************************ FUNCTION DEFINITIONS ***********************/
BUF BUF_Create(U32 size){
  BUF newBuffer = {size, new U8[size]};;
  memset(newBuffer.buffer, ZERO, newBuffer.size);
  
  return newBuffer;
} // BUF_Create()



void BUF_Destroy(BUF &oldBuffer){
  if(oldBuffer.buffer){
    memset(oldBuffer.buffer, ZERO, oldBuffer.size);
    delete [] oldBuffer.buffer;
    oldBuffer.buffer = nullptr;
    oldBuffer.size = ZERO;
  }
} // BUF_Destroy()



void BUF_ToFile(BUF buffer, std::string fileName, U32 offset){
  int fd = open(fileName.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666);
  
  pwrite(fd, buffer.buffer, buffer.size, offset);
  
  if(fd)
    close(fd);
} // BUF_ToFile()



void FileToBUF(BUF buffer, std::string fileName, U32 offset){
  int fd = fd = open(fileName.c_str(), O_RDWR);
  
  pread(fd, buffer.buffer, buffer.size, offset);
  
  if(fd)
    close(fd);
} // FileToBUF()



void PrintBlocks(U8 *buffer, U32 size){
  // for each 128-byte block
  for(U32 i=ZERO; i<size; i+=BYTS_PER_BLK){
    // for each 8-byte word
    for(U32 j=ZERO; j<BYTS_PER_BLK; j+=NIBS_PER_U64){
      if((i+j) < size){
        std::cout << std::dec << std::setfill('0') << std::setw(3) << i+j << " ";
        // for each byte
        for(U32 k=ZERO; k<NIBS_PER_U64; k++){
          if((i+j+k) < size)
            std::cout << std::hex << std::setfill('0') << std::setw(NIBS_PER_U8) << static_cast<U32>(buffer[i+j+k]) << " ";
        }
        std::cout << std::endl;
      }
    }
    std::cout << std::endl;
  }
} // PrintBlocks()



void PrintVector(U8 *buffer, U32 size){
  // for all bytes in buffer
  for(U32 j=ZERO; j<size; j++)
    std::cout << std::hex << std::setfill('0') << std::setw(NIBS_PER_U8) << static_cast<U32>(buffer[j]) << " ";
  std::cout << std::endl << std::endl;
} // PrintVector()



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
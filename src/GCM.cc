/**
 ** @file: GCM.cc
 ** @brief:    implementation file for AES-256 GCM in accordance with FIPS 800-38d
 **            includes encryption, decryption and all supporting functions
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cstring>

#include "GCM.h"

#if defined DEBUG_OUTPUT
#include "IO.h"
#endif // defined DEBUG_OUTPUT

// universal AES-256 constants from FIPS 197 and FIPS 800-38d
#define GCM_IV_PAD_SZ     4         // gcm initialization vector padding size in bytes

/*********************** FORWARD DECLARATIONS ************************/
#if defined DEBUG_OUTPUT
/* PrintSize()
 * @brief: prints _sizeBlkPtr and _size values
 * @params: U8 *sizePtr       : pointer to state block
 *          U32 size         : size in non-vector format
 * @return: none
 */
void GCM_PrintSize(U8 *sizePtr, U32 size);
#endif // defined DEBUG_OUTPUT

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
GCM::GCM(void){
  // allocate heap memory
  _tagPrime = {AES_BLK_SZ, new U8[AES_BLK_SZ]};
  _cbPtr        = new U8[AES_BLK_SZ];
  _hashSubKey   = new U8[AES_BLK_SZ];
  _sPtr         = new U8[AES_BLK_SZ];
  
  // initialize heap memory
  memset(_cbPtr, ZERO, AES_BLK_SZ);
  memset(_hashSubKey, ZERO, AES_BLK_SZ);
  memset(_sPtr, ZERO, AES_BLK_SZ);
  memset(_tagPrime.buffer, ZERO, _tagPrime.size);
  
  // zeroizing other variables
  _ciphertextPtr = _j0_Ptr = _resultPtr = _sizePtr = _tagPtr = nullptr;
  _padSize = _size = ZERO;
} // GCM()



GCM::~GCM(void){
  // zeroize and free heap memory
  if(_cbPtr){
    memset(_cbPtr, ZERO, AES_BLK_SZ);
    delete [] _cbPtr;
    _cbPtr = nullptr;
  }
  if(_hashSubKey){
    memset(_hashSubKey, ZERO, AES_BLK_SZ);
    delete [] _hashSubKey;
    _hashSubKey = nullptr;
  }
  if(_sPtr){
    memset(_sPtr, ZERO, AES_BLK_SZ);
    delete [] _sPtr;
    _sPtr = nullptr;
  }
  if(_tagPrime.buffer){
    memset(_tagPrime.buffer, ZERO, _tagPrime.size);
    delete [] _tagPrime.buffer;
    _tagPrime.buffer = nullptr;
  }
  
  _padSize = _size = _tagPrime.size = ZERO;
} // ~GCM()



bool GCM::GCM_Decrypt(const U8 *ciphertextPtr, const U8 *keyPtr, U8 *plaintextPtr, U32 &size){
  bool returnVal = SUCCESS;       // return value

  // set GCM block sizes
  SetSizeDec(size);
  
  // AES expanded key
  SetKey(keyPtr);
  
  // algorithm 5, step 2: hash subkey | H=CIPHk(0)
  GenerateSubKey();

  // algorithm 5, step 3: Use IV to create J0 | J0=IV||0||1
  returnVal = Unpack(ciphertextPtr, size);
  SetCounterBlock();

  // skip step 5 to check the GCM tag first

  // algorithm 5, steps 5-6: ghash AAD+ciphertext
  memset(_sPtr, ZERO, AES_BLK_SZ);
  Ghash(_resultPtr, GCM_IV_SZ+GCM_IV_PAD_SZ+_size+_padSize+AES_BLK_SZ);

  // algorithm 5, step 7: make tag | T=GCTR(J0,S)
  SetCounterBlock();
  Gctr(_sPtr, _tagPrime.buffer, _tagPrime.size);
  
  // algorithm 5, step 8: compare GCM tags
  if(memcmp(_tagPrime.buffer, _tagPtr, _tagPrime.size))
    returnVal = FAIL;
  
  // if tag is valid, decrypt the data
  // algorithm 5, step 4: encrypt the counter | C=GCTR(inc32(J0), P)
  SetCounterBlock();
  IncrementCB();
  Gctr(_ciphertextPtr, plaintextPtr, _size);

  if(_resultPtr){
    // TODO: memset ZERO
    delete [] _resultPtr;
    _ciphertextPtr = _j0_Ptr = _resultPtr = _sizePtr = _tagPtr = nullptr;
  }
  
  return returnVal;
} // GCM_Decrypt()



void GCM::GCM_Encrypt(U8 *ciphertextPtr, const U8 *keyPtr, const U8 *plaintextPtr, U32 &size){
  // set GCM block sizes
  SetSizeEnc(size);

  // AES expanded key
  SetKey(keyPtr);

  // algorithm 4, step 1: hash subkey | H=CIPHk(0)
  GenerateSubKey();

  // algorithm 4, step 2: Use IV to create J0 | J0=IV||0||1
  GCM_MakeJ0();
  
  // algorithm 4, step 3: encrypt the counter | C=GCTR(inc32(J0), P)
  IncrementCB();
  Gctr(plaintextPtr, _ciphertextPtr, _size);
  
  // algorithm 4, steps 4-5: ghash AAD+ciphertext
  memset(_sPtr, ZERO, AES_BLK_SZ);
  Ghash(_resultPtr, GCM_IV_SZ+GCM_IV_PAD_SZ+_size+_padSize+AES_BLK_SZ);
  
  // algorithm 4, step 6: make tag | T=GCTR(J0,S)
  SetCounterBlock();
  Gctr(_sPtr, _tagPtr, AES_BLK_SZ);
  
#if defined DEBUG_OUTPUT
  std::cout << "GCM Tag:" << std::endl;
  PrintBlocks(_tagPtr, AES_BLK_SZ);
#endif // defined DEBUG_OUTPUT

  // Pack plaintext size, IV and GCM tag
  Pack(ciphertextPtr);
  
  if(_resultPtr){
    // TODO: memset ZERO
    delete [] _resultPtr;
    _ciphertextPtr = _j0_Ptr = _resultPtr = _sizePtr = _tagPtr = nullptr;
  }
} // GCM_Encrypt()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void GCM::Gctr(const U8 *inputPtr, U8 *outputPtr, U32 size){
  U32 pos = (size/AES_BLK_SZ)*AES_BLK_SZ;
  U32 rem = size-pos;
  U8 in[AES_BLK_SZ];
  U8 out[AES_BLK_SZ];
  
  // encrypt and xor full blocks
  if(size >= AES_BLK_SZ){
    for(U32 i=ZERO; i<pos; i+=AES_BLK_SZ){
      Encrypt(&outputPtr[i], _cbPtr);
      XOR(&inputPtr[i], &outputPtr[i]);
      IncrementCB();
    }
  }

  // encrypt and xor partial block
  if(rem){
    memcpy(in, &inputPtr[pos], rem);
    Encrypt(out, _cbPtr);
    XOR(in, out);
    memcpy(&outputPtr[pos], out, rem);
  }
} // Gctr()



void GCM::GenerateSubKey(void){
  memset(_hashSubKey, ZERO, AES_BLK_SZ);
  Encrypt(_hashSubKey, _hashSubKey);
} // GenerateSubKey()



void GCM::Ghash(U8 *inputPtr, U32 size){
  // for each block of input
  for(U32 i=ZERO; i<size; i+=AES_BLK_SZ){
    XOR(&inputPtr[i], _sPtr);
    Multiply(_hashSubKey, _sPtr);
  }
} // Ghash()



void GCM::IncrementCB(void){
  U32 counter = static_cast<U32>(_cbPtr[GCM_IV_SZ+3]&255)
              | static_cast<U32>(_cbPtr[GCM_IV_SZ+2]<<8)
              | static_cast<U32>(_cbPtr[GCM_IV_SZ+1]<<16)
              | static_cast<U32>(_cbPtr[GCM_IV_SZ]<<24);
  
  counter ++;
  
  _cbPtr[GCM_IV_SZ+3] = counter&255;
  _cbPtr[GCM_IV_SZ+2] = counter>>8;
  _cbPtr[GCM_IV_SZ+1] = counter>>16;
  _cbPtr[GCM_IV_SZ] = counter>>24;
} // IncrementCB()



void GCM::GCM_MakeJ0(void){
  // randomize the first 96 bits
  Generate(_j0_Ptr, GCM_IV_SZ);
  
  // last byte
  _j0_Ptr[GCM_IV_SZ+GCM_IV_PAD_SZ-1] = ONE;
  
#if defined DEBUG_OUTPUT
  std::cout << "96-bit IV:" << std::endl;
  PrintBlocks(_j0_Ptr, AES_BLK_SZ);
#endif // defined DEBUG_OUTPUT
  
  // set the initial counter block
  SetCounterBlock();
} // GCM_MakeJ0()



void GCM::Multiply(U8 *xPtr, U8 *yPtr){
  U8 r = 225;             // 11100001
  U8 vPtr[AES_BLK_SZ];    // placeholder for intermediate calculationsf
  U8 zPtr[AES_BLK_SZ];    // placeholder for intermediate calculationsf
  
  // set initial values
  memset(zPtr, ZERO, AES_BLK_SZ);
  memcpy(vPtr, yPtr, AES_BLK_SZ);
  
  // for each byte in the block, from left (0) to right (15)
  for(U8 i=ZERO; i<AES_BLK_SZ; i++){
    // for each bit in the byte from left (0) to right (8)
    for(U8 j=ZERO; j<BITS_PER_U8; j++){
      // if xPtr at this bit is 0x01
      if(xPtr[i] & (ONE << (7-j)))
        XOR(vPtr, zPtr);
      // if the rightmost bit in vPtr is 0x01
      if(vPtr[15] & ONE){
        RightShift(vPtr);
        vPtr[0] ^= r;
      } else {
        RightShift(vPtr);
      }
    }
  }
  // return zPtr in yPtr
  memcpy(yPtr, zPtr, AES_BLK_SZ);
} // Multiply()



void GCM::Pack(U8 *ciphertext){
  U32 offset = ZERO;

  // copy _j0_Ptr into ciphertext
  memcpy(ciphertext, _j0_Ptr, GCM_IV_SZ);
  offset += GCM_IV_SZ;
  
  // copy ciphertext
  memcpy(ciphertext+offset, _ciphertextPtr, _size);
  offset += _size;
  
  // copy _sizePtr into ciphertext
  memcpy(ciphertext+offset, _sizePtr, AES_BLK_SZ);
  offset += AES_BLK_SZ;
  
  // copy _tagPtr into ciphertext
  memcpy(ciphertext+offset, _tagPtr, AES_BLK_SZ);
} // Pack()



void GCM::SetCounterBlock(void){
  // set the initial counter block
  memcpy(_cbPtr, _j0_Ptr, AES_BLK_SZ);
} // SetCounterBlock()

/* SetSizeDec()
 * @brief: sets the ciphertext size, founded up to the nearest block and saves the value
 *         in the _size member
 * @params: U32 ptSize        : size of plaintext in bytes
 * @return: none
 */
void GCM::SetSizeDec(U32 &ptSize){
  // padded size for result buffer
  U32 resultSize = ptSize + GCM_IV_PAD_SZ;
  _padSize = (resultSize%AES_BLK_SZ);
  if(_padSize)
    _padSize = AES_BLK_SZ - _padSize;
  resultSize += _padSize;
  
  // allocate memory for ciphertext
  _resultPtr = new U8[resultSize];
  memset(_resultPtr, ZERO, resultSize);
  
  // return size to caller
  ptSize -= ((2*AES_BLK_SZ) + GCM_IV_SZ);
  _size = ptSize;

  // set quick access pointers
  _j0_Ptr        = _resultPtr;
  _ciphertextPtr = _resultPtr + GCM_IV_SZ + GCM_IV_PAD_SZ;
  _sizePtr       = _resultPtr + GCM_IV_SZ + GCM_IV_PAD_SZ + _size + _padSize;
  _tagPtr        = _resultPtr + GCM_IV_SZ + GCM_IV_PAD_SZ + _size + _padSize + AES_BLK_SZ;
} // SetSizeDec()



void GCM::SetSizeEnc(U32 &ptSize){
  U32 index;
  U32 resultSize;
  U32 tempSize;
  
  // padded size for result buffer
  _size = ptSize;
  _padSize = (ptSize%AES_BLK_SZ);
  if(_padSize)
    _padSize = AES_BLK_SZ - _padSize;
  resultSize = GCM_IV_SZ + GCM_IV_PAD_SZ + _size + _padSize + (2*AES_BLK_SZ);
  ptSize = resultSize - _padSize - GCM_IV_PAD_SZ;
  
  // allocate memory for ciphertext
  _resultPtr = new U8[resultSize];
  memset(_resultPtr, ZERO, resultSize);
  
  // set quick access pointers
  _j0_Ptr        = _resultPtr;
  _ciphertextPtr = _resultPtr + GCM_IV_SZ + GCM_IV_PAD_SZ;
  _sizePtr       = _resultPtr + GCM_IV_SZ + GCM_IV_PAD_SZ + _size + _padSize;
  _tagPtr        = _resultPtr + GCM_IV_SZ + GCM_IV_PAD_SZ + _size + _padSize + AES_BLK_SZ;
  
  // set _sizeBlkPtr to encapsulate aad size in ciphertext block
  index = BYTS_PER_U32;
  tempSize = GCM_IV_SZ * BITS_PER_U8;
  _sizePtr[index]   = tempSize>>24;
  _sizePtr[++index] = tempSize>>16;
  _sizePtr[++index] = tempSize>>8;
  _sizePtr[++index] = tempSize&255;

  // set _sizeBlkPtr to encapsulate plaintext size in ciphertext block
  index+=BYTS_PER_U32;
  tempSize = _size * BITS_PER_U8;
  _sizePtr[++index] = tempSize>>24;
  _sizePtr[++index] = tempSize>>16;
  _sizePtr[++index] = tempSize>>8;
  _sizePtr[++index] = tempSize&255;
  
#if defined DEBUG_OUTPUT
  GCM_PrintSize(_sizePtr, _size);
#endif // defined DEBUG_OUTPUT
} // SetSizeEnc()



void GCM::RightShift(U8 *inputPtr){
  for(U8 w=AES_BLK_SZ-1; w>ZERO; w--){
    inputPtr[w] >>= ONE;
    if(inputPtr[w-1] & ONE){
      inputPtr[w] += 128;
    }
  }
  inputPtr[ZERO] >>= ONE;
} // RightShift()



bool GCM::Unpack(const U8 *ciphertext, U32 &size){
  U32 index;
  U32 offset;
  U32 tempSize;
  
  // extract _j0_Ptr from ciphertext
  memcpy(_j0_Ptr, ciphertext, GCM_IV_SZ);
  memset(_j0_Ptr+AES_BLK_SZ-1, ONE, ONE);

  offset=GCM_IV_SZ;
  
#if defined DEBUG_OUTPUT
  std::cout << "96-bit IV:" << std::endl;
  PrintBlocks(_j0_Ptr, AES_BLK_SZ);
#endif // defined DEBUG_OUTPUT

  // copy ciphertext
  memcpy(_ciphertextPtr, ciphertext+offset, _size);
  offset += _size;
  
  // extract _sizePtr from ciphertext
  memcpy(_sizePtr, ciphertext+offset, AES_BLK_SZ);
  offset+=AES_BLK_SZ;
  
#if defined DEBUG_OUTPUT
  GCM_PrintSize(_sizePtr, _size);
#endif // defined DEBUG_OUTPUT
  
  // extract _tagPtr from ciphertext
  memcpy(_tagPtr, ciphertext+offset, AES_BLK_SZ);
  
#if defined DEBUG_OUTPUT
  std::cout << "GCM Tag:" << std::endl;
  PrintBlocks(_tagPtr, AES_BLK_SZ);
#endif // defined DEBUG_OUTPUT
  
  // validate size
  index = BYTS_PER_U32;
  tempSize = static_cast<U32>(_sizePtr[index]<<24)
           | static_cast<U32>(_sizePtr[index+1]<<16)
           | static_cast<U32>(_sizePtr[index+2]<<8)
           | static_cast<U32>(_sizePtr[index+3]&255);
  
  tempSize /= BITS_PER_U8;
  
  if(tempSize != GCM_IV_SZ)
    return FAIL;
  
  // validate size
  index = 3*BYTS_PER_U32;
  tempSize = static_cast<U32>(_sizePtr[index]<<24)
           | static_cast<U32>(_sizePtr[index+1]<<16)
           | static_cast<U32>(_sizePtr[index+2]<<8)
           | static_cast<U32>(_sizePtr[index+3]&255);
  
  tempSize /= BITS_PER_U8;

  if(tempSize != size)
    return FAIL;
  
  return SUCCESS;
} // Unpack()



void GCM::XOR(const U8 *fromPtr, U8 *toPtr){
  for(U8 w=ZERO; w<AES_BLK_SZ; w++)
    toPtr[w] ^= fromPtr[w];
} // XOR()



#if defined DEBUG_OUTPUT
void GCM_PrintSize(U8 *sizePtr, U32 size){
  std::cout << std::dec << "_size: " << size << std::endl;
  for(U32 i=ZERO; i<AES_BLK_SZ; i++)
    std::cout << std::dec << "_sizePtr[" << static_cast<U32>(i) << "]: " << std::hex << static_cast<U32>(sizePtr[i]) << std::endl;
  std::cout << std::endl;
} // PrintSize()
#endif // defined DEBUG_OUTPUT



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
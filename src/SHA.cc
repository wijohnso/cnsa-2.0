/**
 ** @file: SHA.cc
 ** @brief:    implementation file for SHA-512 and SHA-512/256 hashing in accordance with FIPS 180-4
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cstring>

#include "SHA.h"

#if defined DEBUG_OUTPUT
#include "IO.h"
#endif // defined DEBUG_OUTPUT

// universal debug constants
#define LNGS_PER_ROW      8         // bytes per row (for printing)

// universal SHA-512 constants from FIPS 180-4
#define SHA_512_BLK_SZ    128       // size of SHA-512 block in bytes
#define SHA_256_BLK_SZ    64        // size of SHA-256 block in bytes

// SHA-512/256 and SHA-512 calculation constants
#define SHA_WRDS_PER_BLK  16        // SHA block size in words (SHA_512_BLK_SZ/BYTS_PER_U64) or (SHA_256_BLK_SZ/BYTS_PER_U32)
#define SHA_WRDS_PER_DGST 8         // SHA hash size in words (SHA_512_DGST_SZ/BYTS_PER_U64) or (SHA_256_DGST_SZ/BYTS_PER_U32)
#define SHA_512_ROUNDS    80        // number of rounds for each intermediate SHA-512 hash calculation
#define SHA_256_ROUNDS    64        // number of rounds for each intermediate SHA-256 hash calculation

/*********************** FORWARD DECLARATIONS ************************/
#if defined DEBUG_OUTPUT
/* SHA_256_PrintRound()
 * @brief: prints the private member _v64Ptr
 * @params: U32 t             : iteration count
 *          U32 vPtr          : result of SHA-256 round
 * @return: none
 */
void SHA_256_PrintRound(U32 t, U32 *vPtr);

/* SHA_512_PrintRound()
 * @brief: prints the private member _v64Ptr
 * @params: U32 t             : iteration count
 *          U64 vPtr          : result of SHA-512 or SHA-512/256 round
 * @return: none
 */
void SHA_512_PrintRound(U32 t, U64 *vPtr);

/* SHA_512_PrintSchedule()
 * @brief: prints the private member _w64Ptr
 * @params: U32 vPtr          : SHA-256 message schedule
 * @return: none
 */
void SHA_256_PrintSchedule(U32 *wPtr);

/* SHA_512_PrintSchedule()
 * @brief: prints the private member _w64Ptr
 * @params: U64 vPtr          : SHA-512 or SHA-512/256 message schedule
 * @return: none
 */
void SHA_512_PrintSchedule(U64 *wPtr);
#endif // defined DEBUG_OUTPUT

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
SHA::SHA(void){
  // set mode
  _mode = NORM;
} // SHA()



SHA::SHA(bool mode){
  // set mode
  _mode = mode;
} // SHA()



SHA::~SHA(void){

} // ~SHA()



void SHA::SHA256(const U8 *inputPtr, U8 *outputPtr, U64 size){
  InitHash_256();
  
  Pad_256(inputPtr, size);
  
  for(U32 n=ZERO; n<_inputBlocks; n++){
#if defined DEBUG_OUTPUT
    std::cout << "round " << n+1 << std::endl;
#endif // defined DEBUG_OUTPUT
    
    // step 1: prepare message schedule
    MessageSchedule_256(n);
    
    // step 2: initialize working variables
    HashRound_256();
  }

  HashDigest_256(outputPtr);
} // SHA256()



void SHA::SHA512(const U8 *inputPtr, U8 *outputPtr, U64 size){
  InitHash_512();
  
  Pad_512(inputPtr, size);
  
  for(U32 n=ZERO; n<_inputBlocks; n++){
#if defined DEBUG_OUTPUT
    std::cout << "round " << n+1 << std::endl;
#endif // defined DEBUG_OUTPUT
    
    // step 1: prepare message schedule
    MessageSchedule_512(n);
    
    // step 2: initialize working variables
    HashRound_512();
  }

  HashDigest_512(outputPtr);
} // SHA512()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
U32 SHA::Ch(U32 x, U32 y, U32 z){
  return (x&y)^((~x)&z);
} // Ch()



U64 SHA::Ch(U64 x, U64 y, U64 z){
  return (x&y)^((~x)&z);
} // Ch()



void SHA::HashDigest_256(U8 *outputPtr){
  U8 *hSPtr = reinterpret_cast<U8*>(_h32Ptr);
  U8 limit = ZERO;
  
  // if SHA-256/192 copy 24 bytes
  if(_mode == TRNC)
    limit = SHA_192_DGST_SZ;
  // if SHA-256 copy 32 bytes
  else
    limit = SHA_256_DGST_SZ;
  
  // for each word in the hash
  for(U8 u=ZERO; u<limit; u+=BYTS_PER_U32)
    // for each byte in the word
    for(U8 b=ZERO; b<BYTS_PER_U32; b++)
      outputPtr[u+b] = hSPtr[u+(3-b)];
    
#if defined DEBUG_OUTPUT
  U8 size = _mode == TRNC ? SHA_192_DGST_SZ : SHA_256_DGST_SZ;
  std::cout << "SHA-256 final digest" << std::endl;
  PrintBlocks(outputPtr, size);
#endif // defined DEBUG_OUTPUT
  
  // zeroize and free heap memory
  if(_h32Ptr){
    memset(_h32Ptr, ZERO, BYTS_PER_U32*SHA_WRDS_PER_DGST);
    delete [] _h32Ptr;
    _h32Ptr = nullptr;
  }
  if(_inputStringPtr){
    memset(_inputStringPtr, ZERO, _inputBytes);
    delete [] _inputStringPtr;
    _inputStringPtr = nullptr;
  }
  if(_v32Ptr){
    memset(_v32Ptr, ZERO, BYTS_PER_U32*SHA_WRDS_PER_DGST);
    delete [] _v32Ptr;
    _v32Ptr = nullptr;
  }
  if(_w32Ptr){
    memset(_w32Ptr, ZERO, BYTS_PER_U32*SHA_256_ROUNDS);
    delete [] _w32Ptr;
    _w32Ptr = nullptr;
  }
  _inputBlocks = ZERO;
  _inputBytes = _inputWords = ZERO;
} // HashDigest_256()



void SHA::HashDigest_512(U8 *outputPtr){
  U8 *hSPtr = reinterpret_cast<U8*>(_h64Ptr);
  U8 limit = ZERO;
  
  // if SHA-512/256 copy 32 bytes
  if(_mode == TRNC)
    limit = SHA_256_DGST_SZ;
  // if SHA-512 copy 64 bytes
  else
    limit = SHA_512_DGST_SZ;
  
  // for each word in the hash
  for(U8 u=ZERO; u<limit; u+=SHA_WRDS_PER_DGST)
    // for each byte in the word
    for(U8 b=ZERO; b<BYTS_PER_U64; b++)
      outputPtr[u+b] = hSPtr[u+(7-b)];

#if defined DEBUG_OUTPUT
  U8 size = _mode == TRNC ? SHA_256_DGST_SZ : SHA_512_DGST_SZ;
  std::cout << "SHA-512 final digest" << std::endl;
  PrintBlocks(outputPtr, size);
#endif // defined DEBUG_OUTPUT
  
  // zeroize and free heap memory
  if(_h64Ptr){
    memset(_h64Ptr, ZERO, BYTS_PER_U64*SHA_WRDS_PER_DGST);
    delete [] _h64Ptr;
    _h64Ptr = nullptr;
  }
  if(_inputStringPtr){
    memset(_inputStringPtr, ZERO, _inputBytes);
    delete [] _inputStringPtr;
    _inputStringPtr = nullptr;
  }
  if(_v64Ptr){
    memset(_v64Ptr, ZERO, BYTS_PER_U64*SHA_WRDS_PER_DGST);
    delete [] _v64Ptr;
    _v64Ptr = nullptr;
  }
  if(_w64Ptr){
    memset(_w64Ptr, ZERO, BYTS_PER_U64*SHA_512_ROUNDS);
    delete [] _w64Ptr;
    _w64Ptr = nullptr;
  }
  _inputBlocks = ZERO;
  _inputBytes = _inputWords = ZERO;
} // HashDigest_512()



void SHA::HashRound_256(void){
  const U32 K[] = {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5, // SHA-256 and SHA-256/192 constants
                   0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
                   0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
                   0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
                   0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
                   0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
                   0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
                   0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};
  U32 temp1 = ZERO;    // intermediate calculation variable
  U32 temp2 = ZERO;    // intermediate calculation variable
  
  // initialize working variables a - h
  memcpy(_v32Ptr, _h32Ptr, BYTS_PER_U32*SHA_WRDS_PER_DGST);
  
  // calculate round hashes
  for(U32 t=ZERO; t<SHA_256_ROUNDS; t++){
    temp1 = _v32Ptr[7] +
            Sigma(_v32Ptr[4], 1) +
            Ch(_v32Ptr[4], _v32Ptr[5], _v32Ptr[6]) +
            K[t] +
            _w32Ptr[t];
    temp2 = Sigma(_v32Ptr[0], 0) +
            Maj(_v32Ptr[0], _v32Ptr[1], _v32Ptr[2]);
    
    _v32Ptr[7] = _v32Ptr[6];        // h = g
    _v32Ptr[6] = _v32Ptr[5];        // g = f
    _v32Ptr[5] = _v32Ptr[4];        // f = e
    _v32Ptr[4] = _v32Ptr[3]+temp1;  // e = d + T1
    _v32Ptr[3] = _v32Ptr[2];        // d = c
    _v32Ptr[2] = _v32Ptr[1];        // c = b
    _v32Ptr[1] = _v32Ptr[0];        // b = a
    _v32Ptr[0] = temp1+temp2;       // a = T1 + T2
    
#if defined DEBUG_OUTPUT
    SHA_256_PrintRound(t, _v32Ptr);
#endif // defined DEBUG_OUTPUT
  }
  
  for(U32 i=ZERO; i<SHA_WRDS_PER_DGST; i++)
    _h32Ptr[i] += _v32Ptr[i];
    
#if defined DEBUG_OUTPUT
  std::cout << std::endl << "SHA-256 round hash" << std::endl;
  PrintBlocks(reinterpret_cast<U8*>(_h32Ptr), BYTS_PER_U32*SHA_WRDS_PER_DGST);
#endif // defined DEBUG_OUTPUT
} // HashRound_256()



void SHA::HashRound_512(void){
  const U64 K[] = {0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,  // SHA-512 and SHA-512/256 constants
                   0x3956c25bf348b538, 0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118, 
                   0xd807aa98a3030242, 0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2, 
                   0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235, 0xc19bf174cf692694, 
                   0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65, 
                   0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5, 
                   0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4, 
                   0xc6e00bf33da88fc2, 0xd5a79147930aa725, 0x06ca6351e003826f, 0x142929670a0e6e70, 
                   0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df, 
                   0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b, 
                   0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30, 
                   0xd192e819d6ef5218, 0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8, 
                   0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8, 
                   0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3, 
                   0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec, 
                   0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b, 
                   0xca273eceea26619c, 0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178, 
                   0x06f067aa72176fba, 0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b, 
                   0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c, 
                   0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817};
  U64 temp1 = ZERO;    // intermediate calculation variable
  U64 temp2 = ZERO;    // intermediate calculation variable
  
  // initialize working variables a - h
  memcpy(_v64Ptr, _h64Ptr, BYTS_PER_U64*SHA_WRDS_PER_DGST);
  
  // calculate round hashes
  for(U32 t=ZERO; t<SHA_512_ROUNDS; t++){
    temp1 = _v64Ptr[7] +
            Sigma(_v64Ptr[4], 1) +
            Ch(_v64Ptr[4], _v64Ptr[5], _v64Ptr[6]) +
            K[t] +
            _w64Ptr[t];
    temp2 = Sigma(_v64Ptr[0], 0) +
            Maj(_v64Ptr[0], _v64Ptr[1], _v64Ptr[2]);
    
    _v64Ptr[7] = _v64Ptr[6];        // h = g
    _v64Ptr[6] = _v64Ptr[5];        // g = f
    _v64Ptr[5] = _v64Ptr[4];        // f = e
    _v64Ptr[4] = _v64Ptr[3]+temp1;  // e = d + T1
    _v64Ptr[3] = _v64Ptr[2];        // d = c
    _v64Ptr[2] = _v64Ptr[1];        // c = b
    _v64Ptr[1] = _v64Ptr[0];        // b = a
    _v64Ptr[0] = temp1+temp2;       // a = T1 + T2
    
#if defined DEBUG_OUTPUT
    SHA_512_PrintRound(t, _v64Ptr);
#endif // defined DEBUG_OUTPUT
  }
  
  for(U32 i=ZERO; i<SHA_WRDS_PER_DGST; i++)
    _h64Ptr[i] += _v64Ptr[i];
    
#if defined DEBUG_OUTPUT
  std::cout << std::endl << "SHA-512 round hash" << std::endl;
  PrintBlocks(reinterpret_cast<U8*>(_h64Ptr), BYTS_PER_U64*SHA_WRDS_PER_DGST);
#endif // defined DEBUG_OUTPUT
} // HashRound_512()



void SHA::InitHash_256(void){
  // allocate heap memory
  _h32Ptr = new U32[SHA_WRDS_PER_DGST]; // allocates 32 bytes
  _v32Ptr = new U32[SHA_WRDS_PER_DGST]; // allocates 32 bytes
  _w32Ptr = new U32[SHA_256_ROUNDS];    // allocates 256 bytes
  
  // initialize heap memory
  memset(_h32Ptr, ZERO, BYTS_PER_U32*SHA_WRDS_PER_DGST);
  memset(_v32Ptr, ZERO, BYTS_PER_U32*SHA_WRDS_PER_DGST);
  memset(_w32Ptr, ZERO, BYTS_PER_U32*SHA_256_ROUNDS);
  
  _inputBlocks = ZERO;
  _inputBytes = _inputWords = ZERO;
  
  const U32 H[] = {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, // SHA256 initial hash values
                   0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};
      
  memcpy(_h32Ptr, H, BYTS_PER_U32*SHA_WRDS_PER_DGST);
      
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "SHA-256 initial hash value" << std::endl;
  PrintBlocks(reinterpret_cast<U8*>(_h32Ptr), BYTS_PER_U32*SHA_WRDS_PER_DGST);
#endif // defined DEBUG_OUTPUT
} // InitHash_256()



void SHA::InitHash_512(void){
  // allocate heap memory
  _h64Ptr = new U64[SHA_WRDS_PER_DGST]; // allocates 64 bytes
  _v64Ptr = new U64[SHA_WRDS_PER_DGST]; // allocates 64 bytes
  _w64Ptr = new U64[SHA_512_ROUNDS];    // allocates 640 bytes
  
  // initialize heap memory
  memset(_h64Ptr, ZERO, BYTS_PER_U64*SHA_WRDS_PER_DGST);
  memset(_v64Ptr, ZERO, BYTS_PER_U64*SHA_WRDS_PER_DGST);
  memset(_w64Ptr, ZERO, BYTS_PER_U64*SHA_512_ROUNDS);
  
  _inputBlocks = ZERO;
  _inputBytes = _inputWords = ZERO;
  
  const U64 H_TRNC[] = {0x22312194FC2BF72C, 0x9F555FA3C84C64C2, 0x2393B86B6F53B151, 0x963877195940EABD,   // SHA-512/256 initial hash values
                        0x96283EE2A88EFFE3, 0xBE5E1E2553863992, 0x2B0199FC2C85B8AA, 0x0EB72DDC81C52CA2},
            H_NORM[] = {0x6A09E667F3BCC908, 0xBB67AE8584CAA73B, 0x3C6EF372FE94F82B, 0xA54FF53A5F1D36F1,   // SHA-512 initial hash values
                        0x510E527FADE682D1, 0x9B05688C2B3E6C1F, 0x1F83D9ABFB41BD6B, 0x5BE0CD19137E2179};
      
  if(_mode == TRNC)
    memcpy(_h64Ptr, H_TRNC, BYTS_PER_U64*SHA_WRDS_PER_DGST);
  else
    memcpy(_h64Ptr, H_NORM, BYTS_PER_U64*SHA_WRDS_PER_DGST);
      
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "SHA-512 initial hash value" << std::endl;
  PrintBlocks(reinterpret_cast<U8*>(_h64Ptr), BYTS_PER_U64*SHA_WRDS_PER_DGST);
#endif // defined DEBUG_OUTPUT
} // InitHash_512()



U32 SHA::Maj(U32 x, U32 y, U32 z){
  return (x&y)^(x&z)^(y&z);
} // Maj()



U64 SHA::Maj(U64 x, U64 y, U64 z){
  return (x&y)^(x&z)^(y&z);
} // Maj()



void SHA::MessageSchedule_256(U32 n){
  U64 offset = n*SHA_256_BLK_SZ; // byte offset within 32-bit unsigned integer
  
  memset(_w32Ptr, ZERO, SHA_256_BLK_SZ);
  
  // for sixteen (16) 32-bit words
  for(U32 t=ZERO; t<SHA_WRDS_PER_BLK; t++){
    // for each byte in each word
    for(U32 i=ZERO; i<BYTS_PER_U32; i++)
      _w32Ptr[t] |= static_cast<U32>(_inputStringPtr[offset+i+(t*4)]) << (32-((i+1)*8));
  }
  
  for(U32 t=SHA_WRDS_PER_BLK; t<SHA_256_ROUNDS; t++)
    _w32Ptr[t] = Rho(_w32Ptr[t-2], 1) + _w32Ptr[t-7] + Rho(_w32Ptr[t-15], 0) + _w32Ptr[t-16];
  
#if defined DEBUG_OUTPUT
  SHA_256_PrintSchedule(_w32Ptr);
#endif // defined DEBUG_OUTPUT
} // MessageSchedule_256()



void SHA::MessageSchedule_512(U32 n){
  U64 offset = n*SHA_512_BLK_SZ; // byte offset within 64-bit unsigned integer
  
  memset(_w64Ptr, ZERO, SHA_512_BLK_SZ);

  // for sixteen (16) 64-bit words
  for(U32 t=ZERO; t<SHA_WRDS_PER_BLK; t++){
    // for each byte in each word
    for(U32 i=ZERO; i<BYTS_PER_U64; i++)
      _w64Ptr[t] |= static_cast<U64>(_inputStringPtr[offset+i+(t*8)]) << (64-((i+1)*8));
  }
  
  for(U32 t=SHA_WRDS_PER_BLK; t<SHA_512_ROUNDS; t++)
    _w64Ptr[t] = Rho(_w64Ptr[t-2], 1) + _w64Ptr[t-7] + Rho(_w64Ptr[t-15], 0) + _w64Ptr[t-16];
  
#if defined DEBUG_OUTPUT
  SHA_512_PrintSchedule(_w64Ptr);
#endif // defined DEBUG_OUTPUT
} // MessageSchedule_512()



void SHA::Pad_256(const U8 *inputPtr, U64 size){
  U64 padLimit = (SHA_256_BLK_SZ-(2*SHA_WRDS_PER_DGST))-1; // size limit for padding to fit in last block
  U32 offset = ZERO;                                       // copy offset
  
  _inputBytes  = ((size/SHA_256_BLK_SZ)+1)*SHA_256_BLK_SZ;
  
  // if message size will not fit in last block
  if((size%SHA_256_BLK_SZ)>(padLimit))
    _inputBytes+=SHA_256_BLK_SZ;
  
  _inputBlocks = _inputBytes/SHA_256_BLK_SZ;
  _inputWords = _inputBytes/SHA_WRDS_PER_DGST;
  
  _inputStringPtr = new U8[_inputBytes];
  memset(_inputStringPtr, ZERO, _inputBytes);
  
  // copy all bytes to internal buffer
  memcpy(_inputStringPtr, inputPtr, size);
  
  // copy pad byte to internal buffer
  _inputStringPtr[size] = 0x80; // 10000000
  
  // copy intermediate zeros
  offset = size + ONE;
  memset(_inputStringPtr+offset, ZERO, _inputBytes-SHA_WRDS_PER_DGST-offset);
  
  // copy 64-bit size to end of last block
  // NOTE: the size in this implementation is truncated from the SHA-512 standard of 128 bits to a more reasonable
  // 64 bits, allowing this implementation to hash data up to 16 EiB (exbibytes) in size
  size*=SHA_WRDS_PER_DGST;
  U8 *byteSize = reinterpret_cast<U8*>(&size);
  for(U32 i=_inputBytes-SHA_WRDS_PER_DGST; i<_inputBytes; i++)
    _inputStringPtr[i] = byteSize[(_inputBytes-i)-1];
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "padded input block(s)" << std::endl;
  PrintBlocks(_inputStringPtr, _inputBytes);
#endif // defined DEBUG_OUTPUT
} // Pad_256()



void SHA::Pad_512(const U8 *inputPtr, U64 size){
  U64 padLimit = (SHA_512_BLK_SZ-(2*SHA_WRDS_PER_DGST))-1; // size limit for padding to fit in last block
  U32 offset = ZERO;                                       // copy offset
  
  _inputBytes  = ((size/SHA_512_BLK_SZ)+1)*SHA_512_BLK_SZ;   
  
  // if message size will not fit in last block
  if((size%SHA_512_BLK_SZ)>(padLimit))
    _inputBytes+=SHA_512_BLK_SZ;
  
  _inputBlocks = _inputBytes/SHA_512_BLK_SZ;
  
  _inputStringPtr = new U8[_inputBytes];
  memset(_inputStringPtr, ZERO, _inputBytes);
  
  // copy all bytes to internal buffer
  memcpy(_inputStringPtr, inputPtr, size);
  
  // copy pad byte to internal buffer
  _inputStringPtr[size] = 0x80; // 10000000
  
  // copy intermediate zeros
  offset = size + ONE;
  memset(_inputStringPtr+offset, ZERO, _inputBytes-SHA_WRDS_PER_DGST-offset);
  
  // copy 64-bit size to end of last block
  // NOTE: the size in this implementation is truncated from the SHA-512 standard of 128 bits to a more reasonable
  // 64 bits, allowing this implementation to hash data up to 16 EiB (exbibytes) in size
  size*=SHA_WRDS_PER_DGST;
  U8 *byteSize = reinterpret_cast<U8*>(&size);
  for(U32 i=_inputBytes-SHA_WRDS_PER_DGST; i<_inputBytes; i++)
    _inputStringPtr[i] = byteSize[(_inputBytes-i)-1];
  
  _inputWords = _inputBytes/SHA_WRDS_PER_DGST;
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "padded input block(s)" << std::endl;
  PrintBlocks(_inputStringPtr, _inputBytes);
#endif // defined DEBUG_OUTPUT
} // Pad_512()



U32 SHA::Rho(U32 x, U8 mode){
  if(mode)
    return ROTR(x, 17)^ROTR(x, 19)^SHR(x, 10);
  else
    return ROTR(x, 7)^ROTR(x, 18)^SHR(x, 3);
} // Rho()



U64 SHA::Rho(U64 x, U8 mode){
  if(mode)
    return ROTR(x, 19)^ROTR(x, 61)^SHR(x, 6);
  else
    return ROTR(x, 1)^ROTR(x, 8)^SHR(x, 7);
} // Rho()



U32 SHA::ROTR(U32 toShift, U8 shiftDistance){
  U32 high = SHR(toShift, shiftDistance);
  U32 low = toShift<<((BITS_PER_U32)-shiftDistance);
  return high | low;
} // ROTR()



U64 SHA::ROTR(U64 toShift, U8 shiftDistance){
  U64 high = SHR(toShift, shiftDistance);
  U64 low = toShift<<((BITS_PER_U64)-shiftDistance);
  return high | low;
} // ROTR()



U32 SHA::SHR(U32 toShift, U8 shiftDistance){
  return toShift>>shiftDistance;
} // SHR()



U64 SHA::SHR(U64 toShift, U8 shiftDistance){
  return toShift>>shiftDistance;
} // SHR()



U32 SHA::Sigma(U32 x, U8 mode){
  if(mode)
    return ROTR(x, 6)^ROTR(x, 11)^ROTR(x, 25);
  else
    return ROTR(x, 2)^ROTR(x, 13)^ROTR(x, 22);
} // Sigma()



U64 SHA::Sigma(U64 x, U8 mode){
  if(mode)
    return ROTR(x, 14)^ROTR(x, 18)^ROTR(x, 41);
  else
    return ROTR(x, 28)^ROTR(x, 34)^ROTR(x, 39);
} // Sigma()



#if defined DEBUG_OUTPUT
void SHA_256_PrintRound(U32 t, U32 *vPtr){
  std::cout << "t=" << std::dec << t << std::endl;
  for(U32 i=ZERO; i<SHA_WRDS_PER_DGST; i+=4){
    for(U32 j=0; j<4; j++)
      std::cout << std::hex << std::setfill('0') << std::setw(NIBS_PER_U32) << vPtr[i+j] << " ";
    std::cout << std::endl;
  }
} // SHA_256_PrintRound()



void SHA_512_PrintRound(U32 t, U64 *vPtr){
  std::cout << "t=" << std::dec << t << std::endl;
  for(U32 i=ZERO; i<SHA_WRDS_PER_DGST; i+=4){
    for(U32 j=0; j<4; j++)
      std::cout << std::hex << std::setfill('0') << std::setw(NIBS_PER_U64) << vPtr[i+j] << " ";
    std::cout << std::endl;
  }
} // SHA_512_PrintRound()



void SHA_256_PrintSchedule(U32 *wPtr){
  // for all rounds of hashing
  for(U32 i=ZERO; i<SHA_256_ROUNDS; i+=SHA_WRDS_PER_BLK){
    // for all words in a block
    for(U32 j=ZERO; j<SHA_WRDS_PER_BLK; j+=LNGS_PER_ROW){
      std::cout << std::dec << std::setw(3) << i+j << " ";
      // for each line of 2 words
      for(U32 k=ZERO; k<LNGS_PER_ROW; k++)
        std::cout << std::hex << std::setfill('0') << std::setw(NIBS_PER_U32) << wPtr[i+j+k] << " ";
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
} // SHA_256_PrintSchedule()



void SHA_512_PrintSchedule(U64 *wPtr){
  // for all rounds of hashing
  for(U32 i=ZERO; i<SHA_512_ROUNDS; i+=SHA_WRDS_PER_BLK){
    // for all words in a block
    for(U32 j=ZERO; j<SHA_WRDS_PER_BLK; j+=LNGS_PER_ROW){
      std::cout << std::dec << std::setw(3) << i+j << " ";
      // for each line of 2 words
      for(U32 k=ZERO; k<LNGS_PER_ROW; k++)
        std::cout << std::hex << std::setfill('0') << std::setw(NIBS_PER_U64) << wPtr[i+j+k] << " ";
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }
} // SHA_512_PrintSchedule()
#endif // defined DEBUG_OUTPUT



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
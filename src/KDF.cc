/**
 ** @file: KDF.cc
 ** @brief:    implementation file for PBKDF2 in accordance with NIST SP 800-132
 **            includes master key derivation algorithm
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cstring>

#include "KDF.h"

#if defined DEBUG_OUTPUT
#include "IO.h"
#endif // defined DEBUG_OUTPUT

// universal KDF constants
#define KDF_BLK_SZ        64        // size of KDF block in bytes

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
KDF::KDF(void){
  // allocate heap memory
  _k = {KDF_BLK_SZ, new U8[KDF_BLK_SZ]};
  _t = {KDF_BLK_SZ, new U8[KDF_BLK_SZ]};
  _u = {KDF_BLK_SZ, new U8[KDF_BLK_SZ]};
  
  // initialize heap memory
  memset(_k.buffer, ZERO, _k.size);
  memset(_t.buffer, ZERO, _t.size);
  memset(_u.buffer, ZERO, _u.size);
  
  // zeroizing other variables
  _a = _context = _hashKey = _label = {ZERO, nullptr};
} // KDF()



KDF::~KDF(void){
  // free heap memory
  if(_k.buffer){
    memset(_k.buffer, ZERO, _k.size);
    delete [] _k.buffer;
    _k.buffer = nullptr;
  }
  if(_t.buffer){
    memset(_t.buffer, ZERO, _t.size);
    delete [] _t.buffer;
    _t.buffer = nullptr;
  }
  if(_u.buffer){
    memset(_u.buffer, ZERO, _u.size);
    delete [] _u.buffer;
    _u.buffer = nullptr;
  }
  
  _k.size = _t.size = _u.size = ZERO;
} // ~KDF()



void KDF::DPK(const BUF context, BUF dpk, const BUF label, const BUF mk){
  DPK_Init(context, label, mk);
  
  DPK_Hash();
  
  // copy the requested key size into the dpk buffer
  memcpy(dpk.buffer, _k.buffer, dpk.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "data protection key:" << std::endl;
  PrintBlocks(dpk.buffer, dpk.size);
#endif // defined DEBUG_OUTPUT
  
  // cleanup
  if(_a.buffer){
    memset(_a.buffer, ZERO, _a.size);
    delete [] _a.buffer;
    _a.buffer = nullptr;
  }
  if(_context.buffer){
    memset(_context.buffer, ZERO, _context.size);
    delete [] _context.buffer;
    _context.buffer = nullptr;
  }
  if(_hashKey.buffer){
    memset(_hashKey.buffer, ZERO, _hashKey.size);
    delete [] _hashKey.buffer;
    _hashKey.buffer = nullptr;
  }
  if(_label.buffer){
    memset(_label.buffer, ZERO, _label.size);
    delete [] _label.buffer;
    _label.buffer = nullptr;
  }
  memset(_k.buffer, ZERO, _k.size);
  _a.size = _context.size = _hashKey.size = _label.size = _k.size = ZERO;
} // DPK()



void KDF::MK(const U32 hashIters, const BUF key, BUF mk, const BUF salt){
  MK_Init(key, salt);
  
  // iterate
  for(U32 i=ZERO; i<hashIters; i++)
    MK_Hash();
  
  // copy the requested key size into the mk buffer
  memcpy(mk.buffer, _t.buffer, mk.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "master key:" << std::endl;
  PrintBlocks(mk.buffer, mk.size);
#endif // defined DEBUG_OUTPUT
  
  // cleanup
  if(_hashKey.buffer){
    memset(_hashKey.buffer, ZERO, _hashKey.size);
    delete [] _hashKey.buffer;
    _hashKey.buffer = nullptr;
  }
  memset(_t.buffer, ZERO, _t.size);
  memset(_u.buffer, ZERO, _u.size);
  _hashKey.size = _t.size = _u.size = ZERO;
} // MK()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void KDF::DPK_Hash(void){
  U8 digest[KDF_BLK_SZ];
  U32 offset = ZERO;
  
  // _a = PRF(KIN, A(i−1))
  HMAC_Hash(digest, _a, _hashKey); 
  memcpy(_a.buffer, digest, KDF_BLK_SZ);
  
  // _a = A(i) || Label || 0x00 || Context
  offset += KDF_BLK_SZ;
  
  memcpy(_a.buffer+offset, _label.buffer, _label.size);
  offset += _label.size;
  
  memset(_a.buffer+offset, ZERO, ONE);
  offset += ONE;
  
  memcpy(_a.buffer+offset, _context.buffer, _context.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "DPK_Hash() _a:" << std::endl;
  PrintBlocks(_a.buffer, _a.size);
#endif // defined DEBUG_OUTPUT
  
  // _k.buffer = PRF(KIN, _a)
  HMAC_Hash(_k.buffer, _a, _hashKey);
} // DPK_Hash()



void KDF::DPK_Init(const BUF context, const BUF label, const BUF mk){
  U32 offset = ZERO;
  
  _context = {context.size, new U8[context.size]};
  memcpy(_context.buffer, context.buffer, _context.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "DPK_Init() _context: " << std::dec << _context.size  << std::endl;
  PrintBlocks(_context.buffer, _context.size);
#endif // defined DEBUG_OUTPUT
  
  _label = {label.size, new U8[label.size]};
  memcpy(_label.buffer, label.buffer, _label.size);

#if defined DEBUG_OUTPUT
  std::cout << "DPK_Init() _label: " << std::dec << _label.size << std::endl;
  PrintBlocks(_label.buffer, _label.size);
#endif // defined DEBUG_OUTPUT
  
  _a = {KDF_BLK_SZ + _context.size + _label.size + ONE, nullptr};
  _a.buffer = new U8[_a.size];
  
  // _a = Label || 0x00 || Context
  memset(_a.buffer, ZERO, _a.size);
  memcpy(_a.buffer, _label.buffer, _label.size);
  offset = _label.size + ONE;
  memcpy(_a.buffer+offset, _context.buffer, _context.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "DPK_Init() _a: " << std::dec << _a.size << std::endl;
  PrintBlocks(_a.buffer, _a.size);
#endif // defined DEBUG_OUTPUT
  
  memset(_k.buffer, ZERO, _k.size);

  _hashKey = {mk.size, new U8[mk.size]};
  memcpy(_hashKey.buffer, mk.buffer, _hashKey.size);
} // DPK_Init()



void KDF::MK_Hash(void){
  U8 digest[KDF_BLK_SZ];
  
  HMAC_Hash(digest, _u, _hashKey);
  memcpy(_u.buffer, digest, KDF_BLK_SZ);
  
  for(U32 j=ZERO; j<KDF_BLK_SZ; j++)
    _t.buffer[j] ^= _u.buffer[j];
} // MK_Hash()



void KDF::MK_Init(const BUF key, const BUF salt){
  memset(_t.buffer, ZERO, _t.size);
  
  memcpy(_u.buffer, salt.buffer, salt.size);
  _u.buffer[_u.size-1] = ONE;
  
#if defined DEBUG_OUTPUT
  std::cout << "MK_Init() _u:" << std::endl;
  PrintBlocks(_u.buffer, _u.size);
#endif // defined DEBUG_OUTPUT
  
  _hashKey = {key.size, new U8[key.size]};
  memcpy(_hashKey.buffer, key.buffer, _hashKey.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "MK_Init() _hashKey:" << std::endl;
  PrintBlocks(_hashKey.buffer, _hashKey.size);
#endif // defined DEBUG_OUTPUT
} // MK_Init()



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
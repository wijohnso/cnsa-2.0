/**
 ** @file: HMAC.cc
 ** @brief:    implementation file for HMAC generation in accordance with FIPS 198-1
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cstring>

#include "HMAC.h"

#if defined DEBUG_OUTPUT
#include "IO.h"
#endif // defined DEBUG_OUTPUT

// universal HMAC-SHA-512 constants from FIPS 198-1
#define HMAC_BYTS_PER_BLK 128       // size of HMAC block in bytes

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
HMAC::HMAC(void){
  // allocate heap memory
  _digestPtr = new U8[SHA_512_DGST_SZ];
  _k0Ptr     = new U8[HMAC_BYTS_PER_BLK];
  
  // initialize heap memory
  memset(_digestPtr, ZERO, SHA_512_DGST_SZ);
  memset(_k0Ptr, ZERO, HMAC_BYTS_PER_BLK);
  
  // zeroizing other variables
  _ipad = {ZERO, nullptr};
  _opad = {ZERO, nullptr};
} // HMAC()



HMAC::~HMAC(void){
  // zeroize and free heap memory
  if(_digestPtr){
    memset(_digestPtr, ZERO, SHA_512_DGST_SZ);
    delete [] _digestPtr;
    _digestPtr = nullptr;
  }
  if(_k0Ptr){
    memset(_k0Ptr, ZERO, HMAC_BYTS_PER_BLK);
    delete [] _k0Ptr;
    _k0Ptr = nullptr;
  }
} // ~HMAC()



void HMAC::HMAC_Hash(U8 *digestPtr, const BUF input, const BUF key){
  U8 digest[SHA_512_DGST_SZ];
  memset(digest, ZERO, SHA_512_DGST_SZ);
  
  // step 3: pad key to create K0
  HMAC_Init(key, input.size);
  
  // step 4: K0 XOR ipad
  XOR(_ipad.buffer);
  
  // step 5: concatenate input to inner pad
  memcpy(_ipad.buffer+HMAC_BYTS_PER_BLK, input.buffer, input.size);
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "ipad ^ k0 || input" << std::endl;
  PrintBlocks(_ipad.buffer, _ipad.size);
#endif // defined DEBUG_OUTPUT
  
  // step 6: hash ipad
  SHA512(_ipad.buffer, digest, static_cast<U64>(_ipad.size));
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "SHA512(ipad ^ k0 || input)" << std::endl;
  PrintBlocks(digest, SHA_512_DGST_SZ);
#endif // defined DEBUG_OUTPUT
  
  // step 7: K0 XOR opad
  XOR(_opad.buffer);
  
  // step 8: concatenate digest to outer pad
  memcpy(_opad.buffer+HMAC_BYTS_PER_BLK, digest, SHA_512_DGST_SZ);
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "opad ^ k0 || SHA512(ipad ^ k0 || input)" << std::endl;
  PrintBlocks(_opad.buffer, _opad.size);
#endif // defined DEBUG_OUTPUT
  
  // step 9: hash opad
  SHA512(_opad.buffer, _digestPtr, static_cast<U64>(_opad.size));
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "SHA512(opad ^ k0 || SHA512(ipad ^ k0 || input))" << std::endl;
  PrintBlocks(_digestPtr, SHA_512_DGST_SZ);
#endif // defined DEBUG_OUTPUT
  
  memcpy(digestPtr, _digestPtr, SHA_512_DGST_SZ);
  
  if(_ipad.buffer){
    memset(_ipad.buffer, ZERO, _ipad.size);
    delete [] _ipad.buffer;
    _ipad.buffer = nullptr;
  }
  if(_opad.buffer){
    memset(_opad.buffer, ZERO, _opad.size);
    delete [] _opad.buffer;
    _opad.buffer = nullptr;
  }
  _ipad.size = _opad.size = ZERO;
} // HMAC_Hash()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void HMAC::HMAC_Init(const BUF key, const U32 size){
  // initialize inner pad block
  _ipad = {HMAC_BYTS_PER_BLK + size, new U8[HMAC_BYTS_PER_BLK + size]};
  memset(_ipad.buffer, ZERO, _ipad.size);
  memset(_ipad.buffer, 0x36, HMAC_BYTS_PER_BLK);
  
  // initialize outer pad block
  _opad = {HMAC_BYTS_PER_BLK + SHA_512_DGST_SZ, new U8[HMAC_BYTS_PER_BLK + SHA_512_DGST_SZ]};
  memset(_opad.buffer, ZERO, _opad.size);
  memset(_opad.buffer, 0x5c, HMAC_BYTS_PER_BLK);
  
  // initialize K0
  memset(_k0Ptr, ZERO, HMAC_BYTS_PER_BLK);
  if(key.size > HMAC_BYTS_PER_BLK){
    U8 digest[SHA_512_DGST_SZ];
    SHA512(key.buffer, digest, static_cast<U64>(key.size));
    memcpy(_k0Ptr, digest, SHA_512_DGST_SZ);
  }else{
    memcpy(_k0Ptr, key.buffer, key.size);
  }
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "_k0Ptr:" << std::endl;
  PrintBlocks(_k0Ptr, HMAC_BYTS_PER_BLK);
#endif // defined DEBUG_OUTPUT
  
  // initialize _digestPtr
  memset(_digestPtr, ZERO, SHA_512_DGST_SZ);
} // HMAC_Init()



void HMAC::XOR(U8 *toPtr){
  for(U8 b=ZERO; b<HMAC_BYTS_PER_BLK; b++)
    toPtr[b] ^= _k0Ptr[b];
} // XOR()



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/**
 ** @file: AES.cc
 ** @brief:    implementation file for AES-256 encryption in accordance with FIPS 197
 **            includes encryption, decryption and all supporting functions
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cstring>

#include "AES.h"

#if defined DEBUG_OUTPUT
#include <iomanip>
#include <iostream>
#endif // defined DEBUG_OUTPUT

// AES-256 derived constants 
#define AES_EXP_KEY_SZ    60        // AES-256 key length in 32-bit words (AES_NB*(AES_NR+1))
#define AES_RND_CONST_SZ  7         // number of round constants for AES-256 (EXP_KEY_SZ/AES_NK)

// AES-256 calculation constants
#define AES_HIGH_BYT      0xF0      // upper 4 bits in a byte
#define AES_LOW_BYT       0x0F      // lower 4 bits in a byte

// universal AES-256 constants from FIPS 197 and FIPS 800-38d
#define AES_NB            4         // AES-256 block size in words
#define AES_NK            8         // AES-256 key length in words
#define AES_NR            14        // rounds of encryption for AES-256

/*********************** FORWARD DECLARATIONS ************************/
#if defined DEBUG_OUTPUT
/* AES_PrintRoundConstant()
 * @brief: prints the round constant for one round
 * @params: U8 *roundConstPtr : pointer to round constants
 * @return: none
 */
void AES_PrintRoundConstant(U32 *roundConstPtr);

/* AES_PrintStateBlock()
 * @brief: prints the state block
 * @params: U8 *stateBlockPtr : pointer to state block
 * @return: none
 */
void AES_PrintStateBlock(U8 *stateBlockPtr);
#endif // defined DEBUG_OUTPUT

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
AES::AES(void){
  // allocate heap memory
  _expandedKeyPtr = new U32[AES_EXP_KEY_SZ];
  _roundConstPtr  = new U32[AES_RND_CONST_SZ];
  _stateBlock = {AES_BLK_SZ, new U8[AES_BLK_SZ]};
  
  // initialize heap memory
  memset(_expandedKeyPtr, ZERO, BYTS_PER_U32*AES_EXP_KEY_SZ);
  memset(_roundConstPtr, ZERO, BYTS_PER_U32*AES_RND_CONST_SZ);
  memset(_stateBlock.buffer, ZERO, _stateBlock.size);
  
  // calculate round constants
  CalcRoundConst();
} // AES()



AES::~AES(void){
  // zeroize and free heap memory
  if(_expandedKeyPtr){
    memset(_expandedKeyPtr, ZERO, BYTS_PER_U32*AES_EXP_KEY_SZ);
    delete [] _expandedKeyPtr;
    _expandedKeyPtr = nullptr;
  }
  if(_roundConstPtr){
    memset(_roundConstPtr, ZERO, BYTS_PER_U32*AES_RND_CONST_SZ);
    delete [] _roundConstPtr;
    _roundConstPtr = nullptr;
  }
  if(_stateBlock.buffer){
    memset(_stateBlock.buffer, ZERO, _stateBlock.size);
    delete [] _stateBlock.buffer;
    _stateBlock.buffer = nullptr;
  }
  _stateBlock.size = ZERO;
} // ~AES()



void AES::Decrypt(U8 *ciphertextPtr, U8 *plaintextPtr){
  U32 keyOffset = 224;    // offset from the beginning of _expandedKeyPtr (256-32)
  
  // step 0: copy input to the internal state block
  memcpy(_stateBlock.buffer, ciphertextPtr, _stateBlock.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "round[0]" << std::endl << "INPUT: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
  
  // step 1: AddRoundKey()
  AddRoundKey(keyOffset);
  
  for(U32 i=AES_NR-1; i>ZERO; i--){
#if defined DEBUG_OUTPUT
    std::cout << std::dec << std::endl << "round[" << AES_NR-i << "]" << std::endl;
#endif // defined DEBUG_OUTPUT
    
    // step 2: ShiftColsEnc()
    ShiftColsDec();
    
    // step 3: SubBytesEnc()
    SubBytesDec();
    
    // step 4: AddRoundKey()
    keyOffset -= AES_BLK_SZ;
    AddRoundKey(keyOffset);
    
    // setp 5: MixColumnsDec()
    MixColsDec();
  }

#if defined DEBUG_OUTPUT
  std::cout << std::dec << std::endl << "round[14]" << std::endl;
#endif // defined DEBUG_OUTPUT

  // step 6: ShiftColsEnc()
  ShiftColsDec();
  
  // step 7: SubBytesEnc()
  SubBytesDec();
  
  // step 8: AddRoundKey()
  keyOffset -= AES_BLK_SZ;
  AddRoundKey(keyOffset);
  
  // step 9: copy plaintext input to the internal state block
  memcpy(plaintextPtr, _stateBlock.buffer, _stateBlock.size);
  
#if defined DEBUG_OUTPUT
  std::cout << std::endl;
#endif // defined DEBUG_OUTPUT
} // Decrypt()



void AES::Encrypt(U8 *ciphertextPtr, U8 *plaintextPtr){
  U32 keyOffset = ZERO;      // offset from the beginning of _expandedKeyPtr
  
  // step 0: copy input to the internal state block
  memcpy(_stateBlock.buffer, plaintextPtr, _stateBlock.size);
  
#if defined DEBUG_OUTPUT
  std::cout << std::endl << "round[0]" << std::endl << "INPUT: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
  
  // step 1: AddRoundKey()
  AddRoundKey(keyOffset);
  
  for(U32 i=1; i<AES_NR; i++){
#if defined DEBUG_OUTPUT
    std::cout << std::dec << std::endl << "round[" << i << "]:" << std::endl;
#endif // defined DEBUG_OUTPUT
    
    // step 2: SubBytesEnc()
    SubBytesEnc();
    
    // step 3: ShiftColsEnc()
    ShiftColsEnc();
    
    // setp 4: MixColumnsEnc()
    MixColsEnc();
    
    // step 5: AddRoundKey()
    keyOffset += AES_BLK_SZ;
    AddRoundKey(keyOffset);
  }

#if defined DEBUG_OUTPUT
  std::cout << std::dec << std::endl << "round[14]" << std::endl;
#endif // defined DEBUG_OUTPUT

  // step 6: SubBytesEnc()
  SubBytesEnc();
  
  // step 7: ShiftColsEnc()
  ShiftColsEnc();
  
  // step 8: AddRoundKey()
  keyOffset += AES_BLK_SZ;
  AddRoundKey(keyOffset);
  
  // step 9: copy plaintext input to the internal state block
  memcpy(ciphertextPtr, _stateBlock.buffer, _stateBlock.size);
  
#if defined DEBUG_OUTPUT
  std::cout << std::endl;
#endif // defined DEBUG_OUTPUT
} // Encrypt()

/* SetKey()
 * @brief: Calculates AES-256 key expansion
 * @params: U8 *keyPtr        : pointer to key data
 * @return: none
 */
void AES::SetKey(const U8 *keyPtr){
  //calculate key expansion
  KeyExpansion(keyPtr);
} // SetKey()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void AES::AddRoundKey(U32 keyOffset){
  // create a pointer to the section of the expanded key for this round
  U8 *expKeyStart = reinterpret_cast<U8*>(_expandedKeyPtr) + keyOffset;
  
  // XOR the expanded key with the state block
  for(U32 i=ZERO; i<_stateBlock.size; i++)
    _stateBlock.buffer[i] ^= expKeyStart[i];
  
#if defined DEBUG_OUTPUT
  std::cout << "K_SCH: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
} // AddRoundKey()



void AES::CalcRoundConst(void){
  U8 b[AES_RND_CONST_SZ];     // array of most significant bytes in each round constant
  
  // first round constant is always 1
  b[ZERO] = 1;
  
  // calculate the round constants two (2) through seven (7)
  for(U32 i=1; i<AES_RND_CONST_SZ; i++)
    b[i] = b[i-1] * 2;
  
  // create words from the results
  for(U32 i=ZERO; i<AES_RND_CONST_SZ; i++)
    _roundConstPtr[i] = MakeWord(b[i], ZERO, ZERO, ZERO);
  
#if defined DEBUG_OUTPUT
  AES_PrintRoundConstant(_roundConstPtr);
#endif // defined DEBUG_OUTPUT
} // CalcRoundConst()



void AES::KeyExpansion(const U8 *keyPtr){
  // copy first AES_NK words directly from original key to expanded key
  for(U32 w=ZERO; w<AES_NK; w++)
    _expandedKeyPtr[w] = MakeWord(keyPtr[BYTS_PER_U32*w],
                                  keyPtr[(BYTS_PER_U32*w)+1],
                                  keyPtr[(BYTS_PER_U32*w)+2],
                                  keyPtr[(BYTS_PER_U32*w)+3]);
  
  // expand the remaining bytes until AES_EXP_KEY_SZ
  for(U32 w=AES_NK; w<AES_EXP_KEY_SZ; w++){
    U32 temp = _expandedKeyPtr[w-1];
    
    // do this every eight (8) words
    if((w%AES_NK)==ZERO)
      temp = SubWordEnc(RotWord(temp))^(_roundConstPtr[(w/AES_NK)-1]);
    // do this every four (4) words
    else if((w%AES_NK)==4)
      temp = SubWordEnc(temp);
    
    // do this for all words
    _expandedKeyPtr[w] = _expandedKeyPtr[w-AES_NK]^temp;
  }
  
#if defined DEBUG_OUTPUT
  std::cout << "EXPANDED KEY:" << std::endl;
  for(U32 i=ZERO; i<AES_EXP_KEY_SZ; i++)
    std::cout << std::dec << i << " 0x" << std::hex << std::setfill('0') << std::setw(NIBS_PER_U32) << _expandedKeyPtr[i] << std::endl;
#endif // defined DEBUG_OUTPUT
} // KeyExpansion()



U32 AES::MakeWord(U8 byte0, U8 byte1, U8 byte2, U8 byte3){
  U32 w = static_cast<U32>((byte0 | (byte1 << 8) | (byte2 << 16) | (byte3 << 24)));
  return w;
} // MakeWord()



void AES::MixColsDec(void){
  U8 b[AES_BLK_SZ];   // temporary state block
  
  // copy the state block to the temporary array
  for(U32 i=ZERO; i<AES_BLK_SZ; i++)
    b[i] = _stateBlock.buffer[i];
  
  // gallois multiplication
  for(U32 i=ZERO; i<BYTS_PER_U32; i++){
    _stateBlock.buffer[4*i+0] = XTime(b[4*i+0], 0x0E)^XTime(b[4*i+1], 0x0B)^XTime(b[4*i+2], 0x0D)^XTime(b[4*i+3], 0x09);  // S'0
    _stateBlock.buffer[4*i+1] = XTime(b[4*i+0], 0x09)^XTime(b[4*i+1], 0x0E)^XTime(b[4*i+2], 0x0B)^XTime(b[4*i+3], 0x0D);  // S'1
    _stateBlock.buffer[4*i+2] = XTime(b[4*i+0], 0x0D)^XTime(b[4*i+1], 0x09)^XTime(b[4*i+2], 0x0E)^XTime(b[4*i+3], 0x0B);  // S'2
    _stateBlock.buffer[4*i+3] = XTime(b[4*i+0], 0x0B)^XTime(b[4*i+1], 0x0D)^XTime(b[4*i+2], 0x09)^XTime(b[4*i+3], 0x0E);  // S'3
  }
  
#if defined DEBUG_OUTPUT
  std::cout << "M_COL: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
} // MixColsDec()



void AES::MixColsEnc(void){
  U8 b[AES_BLK_SZ];   // temporary state block
  
  // copy the state block to the temporary array
  for(U32 i=ZERO; i<AES_BLK_SZ; i++)
    b[i] = _stateBlock.buffer[i];
  
  // gallois multiplication
  for(U32 i=ZERO; i<BYTS_PER_U32; i++){
    _stateBlock.buffer[4*i+0] = XTime(b[4*i+0], 0x02)^XTime(b[4*i+1], 0x03)^XTime(b[4*i+2], 0x01)^XTime(b[4*i+3], 0x01);  // S'0
    _stateBlock.buffer[4*i+1] = XTime(b[4*i+0], 0x01)^XTime(b[4*i+1], 0x02)^XTime(b[4*i+2], 0x03)^XTime(b[4*i+3], 0x01);  // S'1
    _stateBlock.buffer[4*i+2] = XTime(b[4*i+0], 0x01)^XTime(b[4*i+1], 0x01)^XTime(b[4*i+2], 0x02)^XTime(b[4*i+3], 0x03);  // S'2
    _stateBlock.buffer[4*i+3] = XTime(b[4*i+0], 0x03)^XTime(b[4*i+1], 0x01)^XTime(b[4*i+2], 0x01)^XTime(b[4*i+3], 0x02);  // S'3
  }
  
#if defined DEBUG_OUTPUT
  std::cout << "M_COL: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
} // MixColsEnc()



U32 AES::RotWord(U32 inputWord){
  U32 high = inputWord >> BITS_PER_U8;   // high 24 bits corrected for endianness
  U32 low  = inputWord << 24;            // low 8 bits corrected for endianness
  
  return high | low;
} // RotWord()



void AES::ShiftColsDec(void){
  // for each column
  for(U32 col=ZERO; col<BYTS_PER_U32; col++){
    // for each required shift
    for(U32 shift=ZERO; shift<col; shift++){
      // rotate one position
      U8 temp = _stateBlock.buffer[col+12];
      _stateBlock.buffer[col+12] = _stateBlock.buffer[col+8];
      _stateBlock.buffer[col+8]  = _stateBlock.buffer[col+4];
      _stateBlock.buffer[col+4]  = _stateBlock.buffer[col];
      _stateBlock.buffer[col]    = temp;
    }
  }
  
#if defined DEBUG_OUTPUT
  std::cout << "S_ROW: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
} // ShiftColsDec()



void AES::ShiftColsEnc(void){
  // for each column
  for(U32 col=ZERO; col<BYTS_PER_U32; col++){
    // for each required shift
    for(U32 shift=ZERO; shift<col; shift++){
      // rotate one position
      U8 temp = _stateBlock.buffer[col];
      _stateBlock.buffer[col]    = _stateBlock.buffer[col+4];
      _stateBlock.buffer[col+4]  = _stateBlock.buffer[col+8];
      _stateBlock.buffer[col+8]  = _stateBlock.buffer[col+12];
      _stateBlock.buffer[col+12] = temp;
    }
  }
  
#if defined DEBUG_OUTPUT
  std::cout << "S_ROW: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
} // ShiftColsEnc()



void AES::SubBytesDec(void){
  // cast state block to an array of words
  U32 *stateWords = reinterpret_cast<U32*>(_stateBlock.buffer);
  
  // substitute from S_BOX
  for(U32 w=ZERO; w<AES_NB; w++)
    stateWords[w] = SubWordDec(stateWords[w]);
  
#if defined DEBUG_OUTPUT
  std::cout << "S_BOX: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
} // SubBytesDec()



void AES::SubBytesEnc(void){
  // cast state block to an array of words
  U32 *stateWords = reinterpret_cast<U32*>(_stateBlock.buffer);
  
  // substitute from S_BOX
  for(U32 w=ZERO; w<AES_NB; w++)
    stateWords[w] = SubWordEnc(stateWords[w]);
  
#if defined DEBUG_OUTPUT
  std::cout << "S_BOX: ";
  AES_PrintStateBlock(_stateBlock.buffer);
#endif // defined DEBUG_OUTPUT
} // SubBytesEnc()



U32 AES::SubWordDec(U32 inputWord){
  //                           col0  col1  col2  col3  col4  col5  col6  col7  col8  col9  cola  colb  colc  cold  cole  colf
  const U8 sBoxDec[16][16] = {{0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB}, // row0
                              {0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB}, // row1
                              {0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E}, // row2
                              {0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25}, // row3
                              {0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92}, // row4
                              {0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84}, // row5
                              {0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06}, // row6
                              {0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B}, // row7
                              {0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73}, // row8
                              {0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E}, // row9
                              {0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B}, // rowa
                              {0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4}, // rowb
                              {0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F}, // rowc
                              {0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF}, // rowd
                              {0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61}, // rowe
                              {0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D}};// rowf
  U8 b[BYTS_PER_U32];   // bytes that comprise the returnVal
  
  for(U32 i=ZERO; i<BYTS_PER_U32; i++){
    U32 colShift = i * 8;        // number of bits to shift for column
    U32 rowShift = colShift + 4; // number of bits to shift for row
    
    // capture the low and high bytes
    U8 col = (inputWord & (static_cast<U32>(AES_LOW_BYT) << colShift)) >> colShift;  // column index
    U8 row = (inputWord & (static_cast<U32>(AES_HIGH_BYT) << colShift)) >> rowShift; // row index
    
    // substitute from sBoxDec
    b[i] = sBoxDec[row][col];
  }
  
  return MakeWord(b[0], b[1], b[2], b[3]);
} // SubWordDec()



U32 AES::SubWordEnc(U32 inputWord){
  //                           col0  col1  col2  col3  col4  col5  col6  col7  col8  col9  cola  colb  colc  cold  cole  colf
  const U8 sBoxEnc[16][16] = {{0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76}, // row0
                              {0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0}, // row1
                              {0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15}, // row2
                              {0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75}, // row3
                              {0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84}, // row4
                              {0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF}, // row5
                              {0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8}, // row6
                              {0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2}, // row7
                              {0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73}, // row8
                              {0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB}, // row9
                              {0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79}, // rowa
                              {0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08}, // rowb
                              {0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A}, // rowc
                              {0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E}, // rowd
                              {0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF}, // rowe
                              {0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16}};// rowf
  U8 b[BYTS_PER_U32];   // bytes that comprise the returnVal
  
  for(U32 i=ZERO; i<BYTS_PER_U32; i++){
    U32 colShift = i * BITS_PER_U8; // number of bits to shift for column
    U32 rowShift = colShift + 4;    // number of bits to shift for row
    
    // capture the low and high bytes
    U8  col = (inputWord & (static_cast<U32>(AES_LOW_BYT) << colShift)) >> colShift;  // low byte
    U8  row = (inputWord & (static_cast<U32>(AES_HIGH_BYT) << colShift)) >> rowShift; // high byte
    
    // substitute from sBoxEnc
    b[i] = sBoxEnc[row][col];
  }
  
  return MakeWord(b[0], b[1], b[2], b[3]);
} // SubWordEnc()



U8 AES::XTime(U8 poly1, U8 poly2){
  U8 returnVal   = ZERO;    // return value
  bool highP1    = false;   // true when bit 8 of poly1 is high 1xxx xxxx
  bool lowP2     = false;   // true when bit 1 of poly2 is high xxxx xxx1
  
  for(U32 bit=ZERO; bit<BITS_PER_U8; bit++){
    // if bit 8 of poly1 is high
    if(poly1 & 0x80)
      highP1 = true;
    
    // if bit 1 of poly2 is high
    if(poly2 & 0x01)
      lowP2 = true;
    
    // update returnVal and shift poly1 left
    if(lowP2)
      returnVal ^= poly1;
    poly1 <<= 0x01;
    
    // update poly1 and shift poly2 right
    if(highP1)
      poly1 ^= 0x1B;
    poly2 >>= 0x01;
    
    // reset flags
    highP1 = lowP2 = false;
  }
  
  return returnVal;
} // XTime()


#if defined DEBUG_OUTPUT
void AES_PrintRoundConstant(U32 *roundConstPtr){
  std::cout << "ROUND CONSTANTS:" << std::endl;
  for(U32 i=ZERO; i<AES_RND_CONST_SZ; i++)
    std::cout << std::dec << i+1 << " 0x" << std::hex << std::setfill('0') << std::setw(NIBS_PER_U32) << roundConstPtr[i] << " ";
  std::cout << std::endl << std::endl;
} // AES_PrintRoundConstant()
#endif // defined DEBUG_OUTPUT



#if defined DEBUG_OUTPUT
void AES_PrintStateBlock(U8 *stateBlockPtr){
  for(U32 i=ZERO; i<AES_NB; i++){
    for(U32 j=ZERO; j<BYTS_PER_U32; j++)
      std::cout << std::hex << std::setfill('0') << std::setw(NIBS_PER_U8) << static_cast<U32>(stateBlockPtr[BYTS_PER_U32*i+j]);
    std::cout << " ";
  }
  std::cout << std::endl;
} // PrintStateBlock()
#endif // defined DEBUG_OUTPUT



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
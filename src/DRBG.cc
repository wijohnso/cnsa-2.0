/**
 ** @file: DRBG.cc
 ** @brief:    implementation file for DRBG in accordance with NIST SP 800-90A
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cstring>
#include <sys/random.h>

#include "DRBG.h"

#define WITH_ENTROPY    true
#define WITHOUT_ENTROPY false

// universal DRBG constants
#define DRBG_SEED_LIM     32        // reseed limit for DRBG

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
DRBG::DRBG(void){
  // allocate heap memory
  _key = {SHA_512_DGST_SZ, new U8[SHA_512_DGST_SZ]};
  _v = {SHA_512_DGST_SZ, new U8[SHA_512_DGST_SZ]};
  
  // initialize heap memory
  memset(_key.buffer, ZERO, _key.size);
  memset(_v.buffer, ONE, _v.size);
  
  // zeroizing other variables
  _reseedCtr = ZERO;
  
  Update(WITH_ENTROPY);
} // DRBG()



DRBG::~DRBG(void){
  // zeroize and free heap memory
  if(_key.buffer){
    memset(_key.buffer, ZERO, _key.size);
    delete [] _key.buffer;
    _key.buffer = nullptr;
  }
  if(_v.buffer){
    memset(_v.buffer, ZERO, _v.size);
    delete [] _v.buffer;
    _v.buffer = nullptr;
  }
  
  _key.size = _v.size = ZERO;
  
  _reseedCtr = ZERO;
} // DRBG()



bool DRBG::Generate(U8 *bufferPtr, const U32 bufferSize){
  U8 digestPtr[SHA_512_DGST_SZ];
  
  if(bufferSize > SHA_512_DGST_SZ)
    return FAIL;
  
  if(++_reseedCtr > DRBG_SEED_LIM)
    return FAIL;
  
  // V = HMAC(K, V)
  HMAC_Hash(digestPtr, _v, _key);
  memcpy(bufferPtr, digestPtr, bufferSize);
  
  Update(WITHOUT_ENTROPY);
  
  return SUCCESS;
} // Generate()



void DRBG::Reseed(void){
  Update(WITH_ENTROPY);
  _reseedCtr = ZERO;
} // Reseed()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void DRBG::Update(const bool addEntropy){
  SSIZE bytesRead = ZERO;
  
  U32 buffOffset = ZERO;
  BUF buff{(2*SHA_512_DGST_SZ)+ONE, new U8[(2*SHA_512_DGST_SZ)+ONE]};
  
  BUF digest{SHA_512_DGST_SZ, new U8[SHA_512_DGST_SZ]};
  
  // 32 bytes entropy + 32 bytes nonce = 64 bytes random data
  BUF random{SHA_512_DGST_SZ, new U8[SHA_512_DGST_SZ]};
  U8 *randomOffset = random.buffer;
  
  if(addEntropy){
    // read 64 bytes of random data
    while(random.size > ZERO){
      bytesRead = getrandom(randomOffset, random.size, ZERO);
      if(bytesRead > ZERO){
        randomOffset += bytesRead;
        random.size -= bytesRead;
      }
    }
  }
  random.size = SHA_512_DGST_SZ;
  
  // K = HMAC(K, V || 0x00 || random)
  memcpy(buff.buffer, _v.buffer, _v.size);
  buffOffset = SHA_512_DGST_SZ;
  memset(buff.buffer+buffOffset, ZERO, ONE);
  buffOffset += ONE;
  if(addEntropy)
    memcpy(buff.buffer+buffOffset, random.buffer, random.size);
  else
    memset(buff.buffer+buffOffset, ZERO, SHA_512_DGST_SZ);
  HMAC_Hash(digest.buffer, buff, _key);
  memcpy(_key.buffer, digest.buffer, digest.size);
  
  // V = HMAC(K, V)
  HMAC_Hash(digest.buffer, _v, _key);
  memcpy(_v.buffer, digest.buffer, _v.size);
  
  if(addEntropy){
    // K = HMAC(K, V || 0x01 || random)
    memcpy(buff.buffer, _v.buffer, _v.size);
    buffOffset = _v.size;
    memset(buff.buffer+buffOffset, ONE, ONE);
    buffOffset += ONE;
    memcpy(buff.buffer+buffOffset, random.buffer, random.size);
    HMAC_Hash(digest.buffer, buff, _key);
    memcpy(_key.buffer, digest.buffer, digest.size);
    
    // V = HMAC(K, V)
    HMAC_Hash(digest.buffer, _v, _key);
    memcpy(_v.buffer, digest.buffer, digest.size);
  }
  
  if(buff.buffer){
    memset(buff.buffer, ZERO, buff.size);
    delete [] buff.buffer;
    buff.buffer = nullptr;
  }
  if(digest.buffer){
    memset(digest.buffer, ZERO, digest.size);
    delete [] digest.buffer;
    digest.buffer = nullptr;
  }
  if(random.buffer){
    memset(random.buffer, ZERO, random.size);
    delete [] random.buffer;
    random.buffer = nullptr;
  }
  
  buff.size = digest.size = random.size = ZERO;
} // Update()


/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/**
 ** @file: LMS.cc
 ** @brief:    implementation file for Leighton-Micali Signatures (LMS)
 **            in accordance with NIST SP 800-132
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cmath>
#include <cstring>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "IO.h"
#include "LMS.h"

// #define LMS_DEBUG

#if defined DEBUG_OUTPUT || defined LMS_DEBUG
#define ITERS           0x1000      // iterations for KDF::MK() in debug mode
#define TREE_HEIGHT     15          // height of Merkle tree
#else
#include <iostream>

#define ITERS           0x100000    // iterations for KDF::MK()
#define TREE_HEIGHT     25          // height of Merkle tree
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG

// size defines
#define CHAIN_LENGTH    256         // hashes per hash chain
#define CHKSUM_HIGH     32          // index for most significant byte of checksum
#define CHKSUM_LOW      33          // index for least significant byte of checksum
#define HASH_PER_KEY    34          // hashes per signing key
#define KEY_SZ          32          // size of each hash in bytes
#define SEED_SZ         64          // seed size in bytes

// file offset defines
#define PUB_KEY_OFFSET  ZERO
#define SEED_OFFSET     KEY_SZ
#define INDEX_OFFSET    KEY_SZ+SEED_SZ

const U8 ID_CHAIN  =    0x00000004; // unique identifier for Winternitz chain
const U8 ID_TREE   =    0x00000009; // unique identifier for Merkel tree
const U8 ID_UNIQUE =    0xc3;       // unique identifier for this signature scheme // TODO: this should be the name of seedFile

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
LMS::LMS(void){
  _merkleTree = new BUF[TREE_HEIGHT];
  for(U32 i=ZERO; i<TREE_HEIGHT; i++)
    _merkleTree[i] = BUF_Create(KEY_SZ);
  
  _signature = new BUF[HASH_PER_KEY];
  for(U32 i=ZERO; i<HASH_PER_KEY; i++)
    _signature[i] = BUF_Create(KEY_SZ);
  
  _context    = BUF_Create(SEED_SZ);
  _label      = BUF_Create(SEED_SZ);
  _privateKey = BUF_Create(KEY_SZ);
  _publicKey  = BUF_Create(KEY_SZ);
  _seed       = BUF_Create(SEED_SZ);
} // LMS()



LMS::~LMS(void){
  if(_merkleTree){
    for(U32 i=ZERO; i<TREE_HEIGHT; i++)
      BUF_Destroy(_merkleTree[i]);
    memset(_merkleTree, ZERO, TREE_HEIGHT*sizeof(BUF));
    delete [] _merkleTree;
    _merkleTree = nullptr;
  }
  if(_signature){
    for(U32 i=ZERO; i<HASH_PER_KEY; i++)
      BUF_Destroy(_signature[i]);
    memset(_signature, ZERO, HASH_PER_KEY*sizeof(BUF));
    delete [] _signature;
    _signature = nullptr;
  }
  
  BUF_Destroy(_context);
  BUF_Destroy(_label);
  BUF_Destroy(_privateKey);
  BUF_Destroy(_publicKey);
  BUF_Destroy(_seed);
} // ~LMS()



void LMS::Initialize(std::string seedFile){
  // seed the LMS scheme
  MakeSeed();
  
  // generate public key
  MakePublicKey();
  
  // reset current index
  _currentIndex = ZERO;
  
  WriteState(seedFile);
} // Initialize()



void LMS::Sign(BUF buffer, std::string keyFile, std::string seedFile){
  // allocate heap memory
  BUF digest  = BUF_Create(HASH_PER_KEY);
  
  U16 checksum = ZERO;
  
  // read state from seed file
  ReadState(seedFile);
  
  // hash the buffer
  SHA256(buffer.buffer, digest.buffer, buffer.size);
  
  // calculate the checksum and add two bytes to the digest
  for(U8 i=ZERO; i<KEY_SZ; i ++)
    checksum += digest.buffer[i];
  digest.buffer[CHKSUM_HIGH] = checksum >> BITS_PER_U8;
  digest.buffer[CHKSUM_LOW]  = checksum & 0xFF;
  
  // for each byte in the digest
  for(U8 b=ZERO; b<HASH_PER_KEY; b++){
    // calculate the private key
    MakePrivateKey(b);
    
    // hash the private key into the signature
    U32 chainLength = digest.buffer[b];
    Winternitz(chainLength, b);
    memcpy(_signature[b].buffer, _privateKey.buffer, _privateKey.size);
  }
  
  // calculate the Merkle tree components
  
  
  // append the signature and Merkle tree components to the buffer
  
  
  // increment the index as this key is consumed
  _currentIndex ++;
  
  // write the updated state to the seed file and the public key to the key file
  WriteIndex(seedFile);
  WritePublicKey(keyFile);
  
  // zeroize and free heap memory
  BUF_Destroy(digest);
} // Sign()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void LMS::InsertKeyInTree(U8 digit){
  // allocate heap memory
  BUF concatenate = BUF_Create(3*KEY_SZ);
  
  U32 level = ZERO;
  
  while(level<TREE_HEIGHT){
    // if this level is not empty
    if(memcmp(_merkleTree[level].buffer, ZERO,KEY_SZ)){
      // insert unique identifier for public key (4x 1 byte)
      for(U32 b=ZERO; b<BYTS_PER_U32; b++)
        memcpy(concatenate.buffer+b, &ID_UNIQUE, ONE);
      // insert unique identifier for Merkel tree (1x 4 bytes)
      memcpy(concatenate.buffer+BYTS_PER_U32, &ID_TREE, BYTS_PER_U32);
      // insert _currentIndex value (2x 4 bytes)
      for(U32 b=BYTS_PER_U64; b<2*BYTS_PER_U64; b+=BYTS_PER_U32)
        memcpy(concatenate.buffer+b, &_currentIndex, BYTS_PER_U32);
      // insert digit value (8x 1 byte)
      for(U32 b=2*BYTS_PER_U64; b<3*BYTS_PER_U64; b++)
        memcpy(concatenate.buffer+b, &digit, ONE);
      // insert level value (8x 1 byte)
      for(U32 b=3*BYTS_PER_U64; b<4*BYTS_PER_U64; b++)
        memcpy(concatenate.buffer+b, reinterpret_cast<U8*>(&level), ONE);
      memcpy(concatenate.buffer+KEY_SZ, _privateKey.buffer, _privateKey.size);
      // concatenate _merkleTree[level] with _privateKey
      memcpy(concatenate.buffer+(2*KEY_SZ), _merkleTree[level].buffer, _merkleTree[level].size);
      memcpy(concatenate.buffer+_merkleTree[level].size, _privateKey.buffer, _privateKey.size);
      
      // hash the concatenated key
      SHA256(concatenate.buffer, _privateKey.buffer, concatenate.size);
      
      // zeroize the previous tree value
      memset(_merkleTree[level].buffer, ZERO, _merkleTree[level].size);
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
      std::cout << "removed key from tree at level " << std::dec << level << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG

    }else{
      // store the private key in the tree
      memcpy(_merkleTree[level].buffer, _privateKey.buffer, _privateKey.size);
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
      std::cout << "added key to tree at level " << std::dec << level << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
      
      break;
    }
    level++;
  }
  
  // zeroize and free heap memory
  BUF_Destroy(concatenate);
} // InsertKeyInTree()



void LMS::MakePrivateKey(U8 keyIndex){
  // set each word in the context
  for(U32 w=ZERO; w<SEED_SZ; w+=sizeof(U32))
    memcpy(_context.buffer+w, &_currentIndex, sizeof(U32));
  
  // set each word in the label
  for(U32 b=ZERO; b<SEED_SZ; b++)
    memcpy(_label.buffer+b, &keyIndex, ONE);
  
  DPK(_context, _privateKey, _label, _seed);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "context[" << std::dec << keyIndex << "]" << std::endl;
  PrintBlocks(_context.buffer, _context.size);
  
  std::cout << "label[" << std::dec << keyIndex << "]" << std::endl;
  PrintBlocks(_label.buffer, _label.size);
  
  std::cout << "private key [" << std::dec << keyIndex << "]" << std::endl;
  PrintBlocks(_privateKey.buffer, _privateKey.size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
} // MakePrivateKey()



void LMS::MakePublicKey(void){
  U32 numSets = pow(2, (TREE_HEIGHT-ONE)) / HASH_PER_KEY;
  U32 remainingKeys = pow(2, (TREE_HEIGHT-ONE)) - (numSets*HASH_PER_KEY);
  
  // for each set of keys
  for(_currentIndex=ZERO; _currentIndex<=numSets; _currentIndex++){
    
    U32 numKeys = HASH_PER_KEY;
    if(_currentIndex == numSets)
      numKeys = remainingKeys;
    
    // for each key in this keyset
    for(U8 digit=ZERO; digit<numKeys; digit++){
      MakePrivateKey(digit);
      Winternitz(CHAIN_LENGTH, digit);
      InsertKeyInTree(digit);
    }
  }
  
  // copy the root of the Merkle tree into _publicKey.buffer
  memcpy(_publicKey.buffer, _merkleTree[TREE_HEIGHT-ONE].buffer, _merkleTree[TREE_HEIGHT-ONE].size);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "public key:" << std::endl;
  PrintBlocks(_publicKey.buffer, _publicKey.size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
} // MakePublicKey()



void LMS::MakeSeed(void){
  // allocate heap memory
  BUF key  = BUF_Create(SEED_SZ);
  BUF salt = BUF_Create(SEED_SZ);
  
  // generate key and salt
  Generate(key.buffer, key.size);
  Generate(salt.buffer, salt.size);
  
  // generate seed
  MK(ITERS, key, _seed, salt);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "seed" << std::endl;
  PrintBlocks(_seed.buffer, _seed.size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
  // zeroize and free heap memory
  BUF_Destroy(key);
  BUF_Destroy(salt);
} // MakeSeed()



void LMS::ReadPublicKey(std::string seedFile){ // TODO: remove
  int fd = ZERO;
  
  fd = open(seedFile.c_str(), O_RDWR);
  
  pread(fd, _publicKey.buffer, _publicKey.size, PUB_KEY_OFFSET);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "public key" << std::endl;
  PrintBlocks(_publicKey.buffer, _publicKey.size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG

  if(fd)
    close(fd);
} // ReadPublicKey()



void LMS::ReadState(std::string seedFile){
  int fd = ZERO;
  
  fd = open(seedFile.c_str(), O_RDWR);
  
  pread(fd, _publicKey.buffer, _publicKey.size, PUB_KEY_OFFSET);
  pread(fd, _seed.buffer, _seed.size, SEED_OFFSET);
  pread(fd, &_currentIndex, sizeof(U32), INDEX_OFFSET);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "public key" << std::endl;
  PrintBlocks(_publicKey.buffer, _publicKey.size);

  std::cout << "seed" << std::endl;
  PrintBlocks(_seed.buffer, _seed.size);
  
  std::cout << "current index" << std::endl;
  PrintVector(reinterpret_cast<U8*>(&_currentIndex), sizeof(U32));
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
  if(fd)
    close(fd);
} // ReadState()



void LMS::Winternitz(U32 chainLength, U8 digit){
  // allocate heap memory
  BUF tempKey = BUF_Create(2*KEY_SZ);
  
  // for each link in a hash chain
  for(U32 winternitz=ZERO; winternitz<chainLength; winternitz++){
    // insert unique identifier for public key (4x 1 byte)
    for(U32 b=ZERO; b<BYTS_PER_U32; b++)
      memcpy(tempKey.buffer+b, &ID_UNIQUE, ONE);
    // insert unique identifier for Merkel tree (1x 4 bytes)
    memcpy(tempKey.buffer+BYTS_PER_U32, &ID_CHAIN, BYTS_PER_U32);
    // insert _currentIndex value (2x 4 bytes)
    for(U32 b=BYTS_PER_U64; b<2*BYTS_PER_U64; b+=BYTS_PER_U32)
      memcpy(tempKey.buffer+b, &_currentIndex, BYTS_PER_U32);
    // insert digit value (8x 1 byte)
    for(U32 b=2*BYTS_PER_U64; b<3*BYTS_PER_U64; b++)
      memcpy(tempKey.buffer+b, &digit, ONE);
    // insert winternitz value (8x 1 byte)
    for(U32 b=3*BYTS_PER_U64; b<4*BYTS_PER_U64; b++)
      memcpy(tempKey.buffer+b, reinterpret_cast<U8*>(&winternitz), ONE);
    memcpy(tempKey.buffer+KEY_SZ, _privateKey.buffer, _privateKey.size);
    
    SHA256(tempKey.buffer, _privateKey.buffer, tempKey.size);
  }
  
  // zeroize and free heap memory
  BUF_Destroy(tempKey);
} // Winternitz()



void LMS::WriteIndex(std::string seedFile){
  int fd = open(seedFile.c_str(), O_WRONLY, 0666);

  pwrite(fd, &_currentIndex, sizeof(U32), INDEX_OFFSET);
  
  if(fd)
    close(fd);
} // WriteIndex()



void LMS::WritePublicKey(std::string seedFile){
  int fd = open(seedFile.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666);
  
  pwrite(fd, _publicKey.buffer, _publicKey.size, PUB_KEY_OFFSET);
  
  if(fd)
    close(fd);
} // WritePublicKey()



void LMS::WriteState(std::string seedFile){
  int fd = ZERO;
  
  fd = open(seedFile.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666);
  
  pwrite(fd, _publicKey.buffer, _publicKey.size, PUB_KEY_OFFSET);
  pwrite(fd, _seed.buffer, _seed.size, SEED_OFFSET);
  pwrite(fd, &_currentIndex, sizeof(U32), INDEX_OFFSET);
  
  if(fd)
    close(fd);
} // WriteState()



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/* @Filename: RNG.cc
 * @brief:    implementation file for CSPRNG
 * 
 * @copyright (C) 2024, Wil Johnson, All rights reserved
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <cstring>
#include <fstream>
#include <sys/random.h>
#include <sys/time.h>

#include "IO.h"
#include "RNG.h"

#define RNG_RAND_SZ       256       // bytes of random data to read from getrandom()
#define RNG_SEED_FILE     "rand/seed.txt"

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
RNG::RNG(){
  _random     = new U8[RNG_RAND_SZ];
  _seedDigest = new U8[SHA_512_DGST_SZ];
} // RNG()



RNG::~RNG(){
  if(_random){
    memset(_random, ZERO, RNG_RAND_SZ);
    delete [] _random;
    _random = nullptr;
  }
  
  if(_seedDigest){
    memset(_seedDigest, ZERO, SHA_512_DGST_SZ);
    delete [] _seedDigest;
    _seedDigest = nullptr;
  }
} // ~RNG()



bool RNG::CBC_MakeIV(U8 *ivPtr){
  return MakeBuffer(ivPtr, CBC_IV_SZ);
} // CBC_MakeIV()



bool RNG::GCM_MakeIV(U8 *ivPtr){
  return MakeBuffer(ivPtr, GCM_IV_SZ);
} // GCM_MakeIV()



bool RNG::MakeSalt(U8 *saltPtr){
  return MakeBuffer(saltPtr, PBKDF_SALT_SZ);
} // MakeSalt()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void RNG::GetRandom(void){
  memset(_random, ZERO, RNG_RAND_SZ);
  
  // read 256 bytes into seed buffer
  getrandom(_random, RNG_RAND_SZ, ZERO);
} // GetRandom()



bool RNG::MakeBuffer(U8 *bufferPtr, U32 bufferSize){
  bool returnVal;         // return value
  
  U8  digest[SHA_512_DGST_SZ]; // local array for hashed random number
  U8  index;              // index within _random
  
  U64 *offsetPtr;         // right 8 bytes of _seedDigest ^ _random
  
  // read and hash seed.txt
  returnVal = ReadSeed();
  
# if defined DEBUG_OUTPUT
  std::cout << "ReadSeed(): " << std::endl;
  PrintBlocks(_seedDigest, SHA_512_DGST_SZ);
# endif // defined DEBUG_OUTPUT
  
  Hash(_seedDigest, digest, SHA_512_DGST_SZ);
  memcpy(_seedDigest, digest, SHA_512_DGST_SZ);
  
  // read 64 bytes from CSPRNG, then copy first 64
  GetRandom();
  Hash(_random, digest, RNG_RAND_SZ);
  memcpy(_random, digest, SHA_512_DGST_SZ);
  
  // set offset to right 8 bytes of _seedDigest ^ _random
  for(U8 i=ZERO; i<SHA_512_DGST_SZ; i++)
    _seedDigest[i] ^= _random[i];
  
  // set bufferPtr from random bytes of random data of size bufferSize
  offsetPtr = (U64*)&_random[56];
  index = *offsetPtr % (SHA_512_DGST_SZ-bufferSize);
  memcpy(bufferPtr, _seedDigest + index, bufferSize);

# if defined DEBUG_OUTPUT
  std::cout << "OFFSET RANGE FROM: " << std::dec << (U32)index << " TO " << (U32)(index + bufferSize) << std::endl;
  PrintVector(bufferPtr, bufferSize);
# endif // defined DEBUG_OUTPUT
  
  // write _seedDigest to seed.txt
  Hash(_seedDigest, digest, SHA_512_DGST_SZ);
  memcpy(_seedDigest, digest, SHA_512_DGST_SZ);
  
# if defined DEBUG_OUTPUT
  std::cout << "WriteSeed(): " << std::endl;
  PrintBlocks(_seedDigest, SHA_512_DGST_SZ);
# endif // defined DEBUG_OUTPUT
  
  WriteSeed();
  
  return returnVal;
} // MakeBuffer()



bool RNG::ReadSeed(void){
  bool returnVal = SUCCESS;
  
  std::ifstream fin;      // input stream object
  
  memset(_seedDigest, ZERO, SHA_512_DGST_SZ);
  
  fin.open(RNG_SEED_FILE, std::ios_base::in | std::ios_base::binary);
  if(fin.fail()){
    returnVal = FAIL;
    Reseed();
  }
  fin.read((char*)_seedDigest, SHA_512_DGST_SZ);
  fin.close();
  
  return returnVal;
} // ReadSeed()



void RNG::Reseed(void){
  U8 seedPtr[SHA_512_DGST_SZ];       // ASCII seed data
  
  memset(seedPtr, ZERO, SHA_512_DGST_SZ);

  struct timeval now;
  gettimeofday(&now, NULL);
  
  memcpy(seedPtr, &now.tv_usec, sizeof(suseconds_t));
  memcpy(seedPtr + sizeof(suseconds_t), &now.tv_sec, sizeof(time_t));
  
  for(U32 i=ZERO; i<HMAC_HSH_ITERS; i++){
    Hash(seedPtr, _seedDigest, SHA_512_DGST_SZ);
    memcpy(seedPtr, _seedDigest, SHA_512_DGST_SZ);
  }
  
# if defined DEBUG_OUTPUT
  std::cout << "WriteSeed(): " << std::endl;
  PrintBlocks(_seedDigest, SHA_512_DGST_SZ);
# endif // defined DEBUG_OUTPUT
  
  WriteSeed();
} // Reseed()



void RNG::WriteSeed(void){
  std::ofstream fout;     // output stream object
  
  fout.open(RNG_SEED_FILE);
  for(U16 i=ZERO; i<SHA_512_DGST_SZ; i++)
    fout << _seedDigest[i];
  fout.close();
} // WriteSeed()



/* @copyright (C) 2024, Wil Johnson, All rights reserved
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
/**
 ** @file: LMS.cc
 ** @brief:    implementation file for Leighton-Micali Signatures (LMS)
 **            in accordance with NIST SP 800-132
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cmath>
#include <cstring>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "IO.h"
#include "LMS.h"

// #define LMS_DEBUG

#if defined DEBUG_OUTPUT || defined LMS_DEBUG
#define ITERS           0x1000        // iterations for KDF::MK() in debug mode
#define TREE_HEIGHT     15            // height of Merkle tree
#else
#include <iostream>                                               // TODO: remove
#define ITERS           0x100         // iterations for KDF::MK() // TODO: remove
#define TREE_HEIGHT     10            // height of Merkle tree    // TODO: remove

// #define ITERS           0x100000      // iterations for KDF::MK()
// #define TREE_HEIGHT     25            // height of Merkle tree
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG

// size defines
#define CHAIN_LENGTH    256           // hashes per hash chain
#define KEYS_PER_SIG    34            // hashes per signing key
#define KEY_SZ          32            // size of each hash in bytes
#define SEED_SZ         64            // seed size in bytes
#define KEYFILE_MAX     20            // maximum string length for keyFile
#define SUBTREE_HEIGHT  6             // bottom layers of Merkle tree not stored in the key file

// file offsets
#define KEYSET_OFFSET   ZERO          // file offset for _currentKeyset
#define MERKLE_OFFSET   68            // file offset for _merkleTree
#define SEED_OFFSET     BYTS_PER_U32  // file offset for _seed

// unique identifiers
const U32 ID_CHAIN  =   0x00000004;   // unique identifier for Winternitz chain
const U32 ID_TREE   =   0x00000009;   // unique identifier for Merkel tree

// tree constants
const U32 TOP_TREE_HEIGHT = TREE_HEIGHT-SUBTREE_HEIGHT+ONE;       // height of top tree
const U32 SUBTREE_WIDTH   = pow(BASE_TWO, (SUBTREE_HEIGHT-ONE));  // width of a subtree of height SUBTREE_HEIGHT
const U32 NUM_SUBTREES    = pow(BASE_TWO, (TOP_TREE_HEIGHT-ONE)); // width of a subtree of height SUBTREE_HEIGHT
const U32 TREE_WIDTH      = pow(BASE_TWO, (TREE_HEIGHT-ONE));     // width of tree of height TREE_HEIGHT
const U32 NUM_KEYSETS     = TREE_WIDTH/KEYS_PER_SIG;              // number of 34-byte keysets in this signature scheme

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
LMS::LMS(void){
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "TREE_HEIGHT="     << TREE_HEIGHT     << std::endl;
  std::cout << "SUBTREE_HEIGHT="  << SUBTREE_HEIGHT  << std::endl;
  std::cout << "TOP_TREE_HEIGHT=" << TOP_TREE_HEIGHT << std::endl;
  std::cout << "SUBTREE_WIDTH="   << SUBTREE_WIDTH   << std::endl;
  std::cout << "NUM_SUBTREES="    << NUM_SUBTREES    << std::endl;
  std::cout << "TREE_WIDTH="      << TREE_WIDTH      << std::endl;
  std::cout << "NUM_KEYSETS="     << NUM_KEYSETS     << std::endl;
  std::cout << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
  _merkleTree = new BUF*[TOP_TREE_HEIGHT];
  for(U32 level=ZERO; level<TOP_TREE_HEIGHT; level++){
    U32 index = TOP_TREE_HEIGHT-level-ONE;
    U32 width = pow(BASE_TWO, level);
    
    _merkleTree[index] = new BUF[width];
    for(U32 j=ZERO; j<width; j++)
      _merkleTree[index][j] = BUF_Create(KEY_SZ);
    
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
    std::cout << "LMS Merkle L" << index << " " << std::setfill('0') << std::setw(3) << width << " keys" << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  }
  
  _keys = new BUF[SUBTREE_WIDTH*2];
  for(U32 i=ZERO; i<SUBTREE_WIDTH*2; i++)
    _keys[i] = BUF_Create(KEY_SZ);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "LMS _keys      " << SUBTREE_WIDTH*2 << " keys" << std::endl;
  std::cout << "LMS _signature " << SUBTREE_WIDTH*2 << " keys" << std::endl;
  std::cout << "LMS _subtreeL  " << SUBTREE_HEIGHT << " keys" << std::endl;
  std::cout << "LMS _subtreeR  " << SUBTREE_HEIGHT << " keys" << std::endl;
  std::cout << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
  _signature = new BUF[SUBTREE_WIDTH*2];
  for(U32 i=ZERO; i<SUBTREE_WIDTH*2; i++)
    _signature[i] = BUF_Create(KEY_SZ);
  
  _subtreeL = new BUF[SUBTREE_HEIGHT];
  for(U32 i=ZERO; i<SUBTREE_HEIGHT; i++)
    _subtreeL[i] = BUF_Create(KEY_SZ);
  
  _subtreeR = new BUF[SUBTREE_HEIGHT];
  for(U32 i=ZERO; i<SUBTREE_HEIGHT; i++)
    _subtreeR[i] = BUF_Create(KEY_SZ);
  
  
  _MK_Key  = BUF_Create(SEED_SZ);
  _MK_Salt = BUF_Create(SEED_SZ);
  _seed    = BUF_Create(SEED_SZ);
  
  _subIndices = new U32[SUBTREE_HEIGHT];
  memset(_subIndices, ZERO, SUBTREE_HEIGHT*sizeof(U32));
  
  _topIndices = new U32[TOP_TREE_HEIGHT];
  memset(_topIndices, ZERO, TOP_TREE_HEIGHT*sizeof(U32));
  
  _subLevels = new bool[SUBTREE_HEIGHT];
  memset(_subLevels, ZERO, SUBTREE_HEIGHT*sizeof(bool));
  
  _topLevels = new bool[TOP_TREE_HEIGHT];
  memset(_topLevels, ZERO, TOP_TREE_HEIGHT*sizeof(bool));
} // LMS()



LMS::~LMS(void){
  if(_merkleTree){
    for(U32 i=ZERO; i<TOP_TREE_HEIGHT; i++){
      U32 index = TOP_TREE_HEIGHT-i-1;
      U32 width = pow(BASE_TWO, i);
      
      for(U32 j=ZERO; j<width; j++)
        BUF_Destroy(_merkleTree[index][j]);
      memset(_merkleTree[index], ZERO, width*sizeof(BUF));
      delete [] _merkleTree[index];
      _merkleTree[index] = nullptr;
    }
    delete [] _merkleTree;
    _merkleTree = nullptr;
  }
  
  if(_keys){
    for(U32 i=ZERO; i<SUBTREE_WIDTH*2; i++)
      BUF_Destroy(_keys[i]);
    memset(_keys, ZERO, SUBTREE_WIDTH*2*sizeof(BUF));
    delete [] _keys;
    _keys = nullptr;
  }
  
  if(_signature){
    for(U32 i=ZERO; i<SUBTREE_WIDTH*2; i++)
      BUF_Destroy(_signature[i]);
    memset(_signature, ZERO, SUBTREE_WIDTH*2*sizeof(BUF));
    delete [] _signature;
    _signature = nullptr;
  }
  
  if(_subtreeL){
    for(U32 i=ZERO; i<SUBTREE_HEIGHT; i++)
      BUF_Destroy(_subtreeL[i]);
    memset(_subtreeL, ZERO, SUBTREE_HEIGHT*sizeof(BUF));
    delete [] _subtreeL;
    _subtreeL = nullptr;
  }
    
  if(_subtreeR){
    for(U32 i=ZERO; i<SUBTREE_HEIGHT; i++)
      BUF_Destroy(_subtreeR[i]);
    memset(_subtreeR, ZERO, SUBTREE_HEIGHT*sizeof(BUF));
    delete [] _subtreeR;
    _subtreeR = nullptr;
  }

  BUF_Destroy(_MK_Key);
  BUF_Destroy(_MK_Salt);
  BUF_Destroy(_seed);
  
  if(_subIndices){
    memset(_subIndices, ZERO, SUBTREE_HEIGHT*sizeof(U32));
    delete [] _subIndices;
    _subIndices = nullptr;
  }
  
  if(_topIndices){
    memset(_topIndices, ZERO, TOP_TREE_HEIGHT*sizeof(U32));
    delete [] _topIndices;
    _topIndices = nullptr;
  }
  
  if(_subLevels){
    memset(_subLevels, ZERO, SUBTREE_HEIGHT*sizeof(bool));
    delete [] _subLevels;
    _subLevels = nullptr;
  }
  
  if(_topLevels){
    memset(_topLevels, ZERO, TOP_TREE_HEIGHT*sizeof(bool));
    delete [] _topLevels;
    _topLevels = nullptr;
  }
} // ~LMS()



bool LMS::Initialize(std::string keyFile, std::string stateFile){
  U32 subtreeEnd   = ZERO;  // ending keyNum
  U32 keyset       = ZERO;  // current keySet number
  U32 keySplit     = ZERO;  // next keySet number
  U32 subtreeStart = ZERO;  // starting keyNum
  U32 keysIndex    = ZERO;  // index into _keys array
  
  if(keyFile.size() > KEYFILE_MAX)
    return FAIL;
  
  // seed the LMS scheme
  MakeSeed();
  
  // generate public key
  for(U32 subtreeNum=ZERO; subtreeNum<NUM_SUBTREES; subtreeNum++){
    subtreeStart = subtreeNum*SUBTREE_WIDTH;
    keyset       = subtreeStart/KEYS_PER_SIG;
    keySplit     = (keyset+1)*KEYS_PER_SIG;
    subtreeEnd   = subtreeStart+SUBTREE_WIDTH;
    keysIndex    = keySplit-subtreeStart;
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
    std::cout << "LMS left  " << keyset     << " range " << subtreeStart << "-" << keySplit   << std::endl;
    std::cout << "LMS right " << keyset+ONE << " range " << keySplit     << "-" << subtreeEnd << std::endl << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
      
    if(keySplit < subtreeEnd){
      MakePrivateKeys(keySplit, ZERO, keyset, subtreeStart);
      MakePrivateKeys(subtreeEnd, keysIndex, ++keyset, keySplit);
    }else{
      MakePrivateKeys(subtreeEnd, ZERO, keyset, subtreeStart);
    }
    
    MakePublicKeys(subtreeEnd, keyFile, ZERO, subtreeStart);
    MakeSubtree(subtreeEnd, keyFile, subtreeStart, _subtreeL);
    InsertKeyInMerkleTree(keyFile);
  }

#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  for(U32 i=ZERO; i<TOP_TREE_HEIGHT; i++){
    std::cout << "top tree " << std::dec << i << std::endl;
    for(U32 j=ZERO; j<_topIndices[i]; j++){
      std::cout << "index " << std::dec << j << std::endl;
      PrintBlocks(_merkleTree[i][j].buffer, _merkleTree[i][j].size);
    }
    std::cout << std::endl << std::endl;
  }
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
  // reset current index
  _currentKeyset = ZERO;
  
  WriteState(stateFile);
  WritePublicKey(keyFile);
  
  return SUCCESS;
} // Initialize()



bool LMS::Sign(std::string keyFile){
  if(keyFile.size() > KEYFILE_MAX)
    return FAIL;
  
  if(_currentKeyset==NUM_KEYSETS)
    return FAIL;
  
  U32 keyStart     = _currentKeyset*KEYS_PER_SIG;             // starting key KeyNum
  U32 keyEnd       = keyStart+KEYS_PER_SIG;                   // ending key KeyNum
  U32 subtreeStart = (keyStart/SUBTREE_WIDTH)*SUBTREE_WIDTH;  // starting subtree keyNum
  U32 subtreeSplit = subtreeStart+SUBTREE_WIDTH;              // splitting keyNum between left and right subtrees
  U32 subtreeEnd   = subtreeSplit+SUBTREE_WIDTH;              // ending subtree keyNum
  U32 keysIndex    = keyStart-subtreeStart;                   // index into _keys array
    
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
    std::cout << "  left " << _currentKeyset-ONE << " range " << subtreeStart << "-" << keyStart     << std::endl;
    std::cout << "target " << _currentKeyset     << " range " << keyStart     << "-" << subtreeSplit << std::endl;
    std::cout << "target " << _currentKeyset     << " range " << subtreeSplit << "-" << keyEnd       << std::endl;
    std::cout << " right " << _currentKeyset+ONE << " range " << keyEnd       << "-" << subtreeEnd   << std::endl << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
  // left subtree
  if(keysIndex){
    MakePrivateKeys(keyStart, ZERO, _currentKeyset-ONE, subtreeStart);
    MakePublicKeys(keyStart, keyFile, ZERO, subtreeStart);
    
    MakePrivateKeys(subtreeSplit, keysIndex, _currentKeyset, keyStart);
    MakePublicKeys(subtreeSplit, keyFile, keysIndex, keyStart);
  }else{
    MakePrivateKeys(subtreeSplit, ZERO, _currentKeyset, subtreeStart);
    MakePublicKeys(subtreeSplit, keyFile, ZERO, subtreeStart);
  }
  
  // right subtree
  keysIndex = subtreeEnd-keyEnd;
  if(keysIndex){
    MakePrivateKeys(keyEnd, KEYS_PER_SIG, _currentKeyset, subtreeSplit);
    MakePublicKeys(keyEnd, keyFile, KEYS_PER_SIG, subtreeSplit);
    
    MakePrivateKeys(subtreeEnd, keysIndex, _currentKeyset+ONE, keyEnd);
    MakePublicKeys(subtreeEnd, keyFile, keysIndex, keyEnd);
  }else{
    MakePrivateKeys(subtreeEnd, ZERO, _currentKeyset, subtreeSplit);
    MakePublicKeys(subtreeEnd, keyFile, ZERO, subtreeSplit);
  }

  MakeSubtree(subtreeSplit, keyFile, subtreeStart, _subtreeL);
  MakeSubtree(subtreeEnd, keyFile, subtreeSplit, _subtreeR);

  // target key material
  MakePrivateKeys(keyEnd, keyStart-subtreeStart, ++_currentKeyset, keyStart);
  // TODO: Winternitz signature
  // TODO: Left Merkle tree values
  // TODO: Left subtree values
  // TODO: Right subtree values
  // TODO: Right Merkle tree values
  
  return SUCCESS;
} // Sign()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void LMS::InsertKeyInSubtree(std::string keyFile, U32 keysIndex, U32 keyNum, BUF *subtree){
  U32 level = ZERO;
  U8 concatenateBuffer[3*KEY_SZ];
  BUF concatenate = {3*KEY_SZ, concatenateBuffer};
  
  memset(concatenate.buffer, ZERO, concatenate.size);
  
  while(level<SUBTREE_HEIGHT){
    if(_subLevels[level]){
      // insert unique identifier for public key (1x 20 byte)
      memcpy(concatenate.buffer, keyFile.c_str(), keyFile.size());
      
      // insert unique identifier for Merkel tree (1x 4 bytes)
      memcpy(concatenate.buffer+KEYFILE_MAX, &ID_TREE, BYTS_PER_U32);
      
      // insert keyNum value (1x 4 bytes)
      memcpy(concatenate.buffer+KEYFILE_MAX+BYTS_PER_U32, &keyNum, BYTS_PER_U32);
      
      // insert level value (1x 4 bytes)
      memcpy(concatenate.buffer+KEYFILE_MAX+BYTS_PER_U64, &level, BYTS_PER_U32);
      
      // concatenate _keys[keysIndex] with subtree[level]
      memcpy(concatenate.buffer+KEY_SZ, _keys[keysIndex].buffer, _keys[keysIndex].size);
      memcpy(concatenate.buffer+(2*KEY_SZ), subtree[level].buffer, subtree[level].size);
      
      // hash the concatenated key
      SHA256(concatenate.buffer, _keys[keysIndex].buffer, concatenate.size);
      
      // zeroize the previous subtree value
      memset(subtree[level].buffer, ZERO, subtree[level].size);
      
      _subLevels[level] = false;
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
      std::cout << "R [" << std::dec << level << "] ";
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG

    }else{
      // store the private key in the subtree
      memcpy(subtree[level].buffer, _keys[keysIndex].buffer, _keys[keysIndex].size);
      
      _subLevels[level] = true;
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
      std::cout << "A [" << std::dec << level << "]" << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
      
      break;
    }
    level++;
  }
} // InsertKeyInSubtree()



void LMS::InsertKeyInMerkleTree(std::string keyFile){
  U32 level = ZERO;
  U8 concatenateBuffer[3*KEY_SZ];
  BUF concatenate = {3*KEY_SZ, concatenateBuffer};

  memset(concatenate.buffer, ZERO, concatenate.size);
  
  while(level<TOP_TREE_HEIGHT){
    if(_topLevels[level]){
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
      std::cout << "F [" << std::setw(2) << std::setfill('0') << level << "][" << std::setw(2) << std::setfill('0') << _topIndices[level] << "] ";
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
      
      // store the private key in the subtree
      memcpy(_merkleTree[level][_topIndices[level]].buffer, _subtreeL[SUBTREE_HEIGHT-ONE].buffer, _subtreeL[SUBTREE_HEIGHT-ONE].size);
      
      // insert unique identifier for public key (1x 20 byte)
      memcpy(concatenate.buffer, keyFile.c_str(), keyFile.size());
      
      // insert unique identifier for Merkel tree (1x 4 bytes)
      memcpy(concatenate.buffer+KEYFILE_MAX, &ID_TREE, BYTS_PER_U32);
      
      // insert _topIndices[level] value (1x 4 bytes)
      memcpy(concatenate.buffer+KEYFILE_MAX+BYTS_PER_U32, &_topIndices[level], BYTS_PER_U32);
      
      // insert level value (1x 4 bytes)
      memcpy(concatenate.buffer+KEYFILE_MAX+BYTS_PER_U64, &level, BYTS_PER_U32);
      
      // concatenate _merkleTree[level][_topIndices[level]-ONE] with _merkleTree[level][_topIndices[level]]
      memcpy(concatenate.buffer+KEY_SZ, _merkleTree[level][_topIndices[level]-ONE].buffer, _merkleTree[level][_topIndices[level]-ONE].size);
      memcpy(concatenate.buffer+(2*KEY_SZ), _merkleTree[level][_topIndices[level]].buffer, _merkleTree[level][_topIndices[level]].size);
      
      // hash the concatenated key
      SHA256(concatenate.buffer, _subtreeL[SUBTREE_HEIGHT-ONE].buffer, concatenate.size);
      
      _topLevels[level] = false;
      _topIndices[level]++;
      level++;
      
    }else{
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
      std::cout << "E [" << std::setw(2) << std::setfill('0') << level << "][" << std::setw(2) << std::setfill('0') << _topIndices[level] << "] ";
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
      
      // store the private key in the subtree
      memcpy(_merkleTree[level][_topIndices[level]].buffer, _subtreeL[SUBTREE_HEIGHT-ONE].buffer, _subtreeL[SUBTREE_HEIGHT-ONE].size);
      
      _topLevels[level] = true;
      _topIndices[level]++;
      level++;
      break;
    }
  }
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
} // InsertKeyInMerkleTree()



void LMS::MakePrivateKey(U32 keysIndex, U8 keysetIndex, U32 keyset){
  U8 contextBuf[SEED_SZ];
  BUF context = {SEED_SZ, contextBuf};
  
  U8 labelBuf[SEED_SZ];
  BUF label = {SEED_SZ, labelBuf};
  
  // set each word in the context
  for(U32 w=ZERO; w<SEED_SZ; w+=sizeof(U32))
    memcpy(context.buffer+w, &keyset, sizeof(U32));
  
  // set each word in the label
  for(U32 b=ZERO; b<SEED_SZ; b++)
    memcpy(label.buffer+b, &keysetIndex, ONE);
  
  DPK(context, _keys[keysIndex], label, _seed);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "LMS private key [" << std::dec << keysIndex << "]:" << std::endl;
  PrintBlocks(_keys[keysIndex].buffer, _keys[keysIndex].size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
} // MakePrivateKey()



void LMS::MakePrivateKeys(U32 end, U32 keysIndex, U32 keyset, U32 start){
  U8 keysetIndex;
  
  for(U32 keyNum=start; keyNum<end; keyNum++){
    keysetIndex = keyNum%KEYS_PER_SIG;
    
    MakePrivateKey(keysIndex, keysetIndex, keyset);
    
    keysIndex++;
  }
} // MakePrivateKeys()



void LMS::MakePublicKey(std::string keyFile, U32 keysIndex, U32 keyNum){
  Winternitz(CHAIN_LENGTH, keyFile, keysIndex, keyNum);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "LMS public key [" << std::dec << keysIndex << "]:" << std::endl;
  PrintBlocks(_keys[keysIndex].buffer, _keys[keysIndex].size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
} // MakePublicKey



void LMS::MakePublicKeys(U8 end, std::string keyFile, U32 keysIndex, U8 start){
  for(U32 keyNum=start; keyNum<end; keyNum++){
    MakePublicKey(keyFile, keysIndex, keyNum);
    
    keysIndex++;
  }
} // MakePublicKeys()



void LMS::MakeSeed(void){
  // generate random key and random salt
  Generate(_MK_Key.buffer, _MK_Key.size);
  Generate(_MK_Salt.buffer, _MK_Salt.size);
  
  // generate seed from key and salt
  MK(ITERS, _MK_Key, _seed, _MK_Salt);
  
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << "LMS seed:" << std::endl;
  PrintBlocks(_seed.buffer, _seed.size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
} // MakeSeed()



void LMS::MakeSubtree(U8 end, std::string keyFile, U8 start, BUF *subtree){
  U32 keysIndex = ZERO;
  
  memset(_subLevels, ZERO, SUBTREE_HEIGHT*sizeof(bool));
  
  for(U32 keyNum=start; keyNum<end; keyNum++){
    InsertKeyInSubtree(keyFile, keysIndex, keyNum, subtree);
    keysIndex++;
  }

#if defined DEBUG_OUTPUT || defined LMS_DEBUG
  std::cout << std::endl;
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
  
} // MakeSubtree()



void LMS::ReadKeyset(std::string stateFile){
  int fd = open(stateFile.c_str(), O_RDWR);
  
  // write _currentKeyset
  pread(fd, &_currentKeyset, BYTS_PER_U32, KEYSET_OFFSET);
  
  if(fd)
    close(fd);
} // ReadKeyset()



void LMS::ReadPublicKey(std::string keyFile){
  int fd = open(keyFile.c_str(), O_RDWR);
  
  // read _merkleTree[TOP_TREE_HEIGHT-ONE][ZERO].buffer
  pread(fd, _merkleTree[TOP_TREE_HEIGHT-ONE][ZERO].buffer, _merkleTree[TOP_TREE_HEIGHT-ONE][ZERO].size, ZERO);

  if(fd)
    close(fd);
} // ReadPublicKey()



void LMS::ReadState(std::string stateFile){
  U32 index = ZERO;
  U32 offset = ZERO;
  U32 width = ZERO;
  
  int fd = open(stateFile.c_str(), O_RDWR);
  
  // read _currentKeyset
  pread(fd, &_currentKeyset, BYTS_PER_U32, KEYSET_OFFSET);
  
  // read _seed
  pread(fd, _seed.buffer, _seed.size, SEED_OFFSET);
  
  // read _merkleTree
  offset = MERKLE_OFFSET;
  for(U32 level=ZERO; level<TOP_TREE_HEIGHT; level++){
    index = TOP_TREE_HEIGHT-level-ONE;
    width = pow(BASE_TWO, level);
    
    for(U32 j=ZERO; j<width; j++){
      pread(fd, _merkleTree[index][j].buffer, _merkleTree[index][j].size, offset);
      offset += _merkleTree[index][j].size;
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
      std::cout << "index [" << std::dec << index << "][" << std::dec << j << "]" << std::endl;
      PrintBlocks(_merkleTree[index][j].buffer, _merkleTree[index][j].size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
      
    }
    offset += (width*KEY_SZ);
  }
  
  if(fd)
    close(fd);
} // ReadState()



void LMS::Winternitz(U32 chainLength, std::string keyFile, U32 keysIndex, U32 keyNum){
  // allocate heap memory
  U8 tempBuf[2*KEY_SZ];
  BUF temp = {2*KEY_SZ, tempBuf};
  
  memset(temp.buffer, ZERO, 2*KEY_SZ);
  
  // for each link in a hash chain
  for(U32 winternitz=ZERO; winternitz<chainLength; winternitz++){
    // insert unique identifier for public key (1x 20 byte)
    memcpy(temp.buffer, keyFile.c_str(), keyFile.size());
    
    // insert unique identifier for Merkel tree (1x 4 bytes)
    memcpy(temp.buffer+KEYFILE_MAX, &ID_CHAIN, BYTS_PER_U32);
    
    // insert keyNum value (1x 4 bytes)
    memcpy(temp.buffer+KEYFILE_MAX+BYTS_PER_U32, &keyNum, BYTS_PER_U32);
    
    // insert winternitz values 2x (1x 4 bytes)
    memcpy(temp.buffer+KEYFILE_MAX+BYTS_PER_U64, &winternitz, BYTS_PER_U32);
    
    // insert _keys[keysIndex] (1x 32 bytes)
    memcpy(temp.buffer+KEY_SZ, _keys[keysIndex].buffer, _keys[keysIndex].size);
    
    SHA256(temp.buffer, _keys[keysIndex].buffer, temp.size);
  }
} // Winternitz()



void LMS::WriteKeyset(std::string stateFile){
  int fd = open(stateFile.c_str(), O_WRONLY, 0666);
  
  // write _currentKeyset
  pwrite(fd, &_currentKeyset, BYTS_PER_U32, KEYSET_OFFSET);
  
  if(fd)
    close(fd);
} // WriteKeyset()



void LMS::WritePublicKey(std::string keyFile){
  int fd = open(keyFile.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666);
  
  // write _merkleTree[TOP_TREE_HEIGHT-ONE][ZERO].buffer
  pwrite(fd, _merkleTree[TOP_TREE_HEIGHT-ONE][ZERO].buffer, _merkleTree[TOP_TREE_HEIGHT-ONE][ZERO].size, ZERO);
  
  if(fd)
    close(fd);
} // WritePublicKey()



void LMS::WriteState(std::string stateFile){
  U32 index  = ZERO;
  U32 offset = ZERO;
  U32 width  = ZERO;
  
  int fd = open(stateFile.c_str(), O_WRONLY|O_CREAT|O_TRUNC, 0666);
  
  // write _currentKeyset
  pwrite(fd, &_currentKeyset, BYTS_PER_U32, KEYSET_OFFSET);
  
  // write _seed
  pwrite(fd, _seed.buffer, _seed.size, SEED_OFFSET);
  
  // write _merkleTree
  offset = MERKLE_OFFSET;
  for(U32 level=ZERO; level<TOP_TREE_HEIGHT; level++){
    index = TOP_TREE_HEIGHT-level-ONE;
    width = pow(BASE_TWO, level);
    
    for(U32 j=ZERO; j<width; j++){
      
#if defined DEBUG_OUTPUT || defined LMS_DEBUG
      std::cout << "index [" << std::dec << index << "][" << std::dec << j << "]" << std::endl;
      PrintBlocks(_merkleTree[index][j].buffer, _merkleTree[index][j].size);
#endif // defined DEBUG_OUTPUT || defined LMS_DEBUG
      
      pwrite(fd, _merkleTree[index][j].buffer, _merkleTree[index][j].size, offset);
      offset += _merkleTree[index][j].size;
    }
  }
  
  if(fd)
    close(fd);
} // WriteState()



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
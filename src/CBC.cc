/**
 ** @file: CBC.cc
 ** @brief:    implementation file for AES-256 CBC in accordance with FIPS 800-38a
 **           includes FIPS 800-38a addendum for ciphertext stealing with CBC-CS1
 **           includes encryption, decryption and all supporting functions
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <cstring>

#include "CBC.h"

#if defined DEBUG_OUTPUT
#include "IO.h"
#endif // defined DEBUG_OUTPUT

/******************** PUBLIC FUNCTION DEFINITIONS ********************/
CBC::CBC(void){
  // allocate heap memory
  _iv = {CBC_IV_SZ, new U8[CBC_IV_SZ]};
  
  // initialize heap memory
  memset(_iv.buffer, ZERO, _iv.size);
  
  // zeroizing other variables
  _ciphertextPtr = _plaintextPtr = nullptr;
  _d = _n = _size = ZERO;
} // CBC()



CBC::~CBC(void){
  // zeroize and free heap memory
  if(_iv.buffer){
    memset(_iv.buffer, ZERO, CBC_IV_SZ);
    delete [] _iv.buffer;
    _iv.buffer = nullptr;
  }
  
  _d = _iv.size = _n = _size = ZERO;
} // ~CBC()



void CBC::CBC_Decrypt(const BUF ciphertext, const U8 *keyPtr, BUF &plaintext){
  // size must be at least one full AES block
  if(ciphertext.size<AES_BLK_SZ)
    return;
  
  // set CBC block sizes
  plaintext.size = SetSizeDec(ciphertext.size);
  
  // AES expanded key
  SetKey(keyPtr);
  
  // read initialization vector
  Unpack(ciphertext.buffer);
  
  // decrypt first block using iv
  Decrypt(_ciphertextPtr, _plaintextPtr);
  XOR(_iv.buffer, _plaintextPtr);
  
  // decrypt remaining blocks using previous block
  for(U32 i=AES_BLK_SZ; i<_size; i+=AES_BLK_SZ){
    Decrypt(_ciphertextPtr+i, _plaintextPtr+i);
    XOR(_ciphertextPtr+i-AES_BLK_SZ, _plaintextPtr+i);
  }
  
  // insert plaintext
  memcpy(plaintext.buffer, _plaintextPtr, plaintext.size);
  
  // teardown
  Teardown();
} // CBC_Decrypt()



void CBC::CBC_Encrypt(BUF &ciphertext, const U8 *keyPtr, const BUF plaintext){
  // size must be at least one full AES block
  if(plaintext.size<AES_BLK_SZ)
    return;
  
  // set CBC block sizes
  ciphertext.size = SetSizeEnc(plaintext.size);
  
  // AES expanded key
  SetKey(keyPtr);
  
  // create initialization vector
  Generate(_iv.buffer, _iv.size);
  
  // extract plaintext
  memcpy(_plaintextPtr, plaintext.buffer, plaintext.size);
  
  // encrypt first block using iv
  XOR(_iv.buffer, _plaintextPtr);
  Encrypt(_ciphertextPtr, _plaintextPtr);
  
  // encrypt remaining blocks using previous block
  for(U32 i=AES_BLK_SZ; i<_size; i+=AES_BLK_SZ){
    XOR(_ciphertextPtr+i-AES_BLK_SZ, _plaintextPtr+i);
    Encrypt(_ciphertextPtr+i, _plaintextPtr+i);
  }
  
  // copy ciphertext to return to user
  Pack(ciphertext.buffer);
  
  // teardown
  Teardown();
} // CBC_Encrypt()



/******************** PRIVATE FUNCTION DEFINITIONS ********************/
void CBC::Pack(U8* ciphertextPtr){
  U32 limit;
  U32 offset;
  
#if defined DEBUG_OUTPUT
  std::cout << "Pack() _iv.buffer" << std::endl;
  PrintBlocks(_iv.buffer, _iv.size);
#endif // defined DEBUG_OUTPUT
  
  // insert initialization vector
  memcpy(ciphertextPtr, _iv.buffer, _iv.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "Pack() _ciphertextPtr" << std::endl;
  PrintBlocks(_ciphertextPtr, _n*AES_BLK_SZ);
#endif // defined DEBUG_OUTPUT
  
  if(_d)
    limit = _size-(CBC_IV_SZ+AES_BLK_SZ);
  else
    limit = _size-CBC_IV_SZ;
  
  // insert C1||C2||...||Cn-2
  offset = CBC_IV_SZ;
  memcpy(ciphertextPtr+offset, _ciphertextPtr, limit);
  
  // insert partial block Cn-1*
  offset = CBC_IV_SZ+limit;
  memcpy(ciphertextPtr+offset, _ciphertextPtr+limit, _d);
  
  // insert Cn
  offset = CBC_IV_SZ+limit+_d;
  if(_d)
    memcpy(ciphertextPtr+offset, _ciphertextPtr+limit+AES_BLK_SZ, AES_BLK_SZ);
  else
    memcpy(ciphertextPtr+offset, _ciphertextPtr+limit, AES_BLK_SZ);
} // Pack()



U32 CBC::SetSizeDec(const U32 ctSize){
  U32 ptSize = ctSize-CBC_IV_SZ;
  
  _n = ptSize/AES_BLK_SZ;
  _d = ptSize%AES_BLK_SZ;
  
  if(_d) // include partial block, if one exists
    _n ++;
  
  _size = _n*AES_BLK_SZ;
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "SetSizeDec() _size:" << _size << " size:" << ptSize << " _n:" << _n << " _d:" << _d << std::endl << std::endl;
#endif // defined DEBUG_OUTPUT
  
  _ciphertextPtr = new U8 [_size];
  _plaintextPtr = new U8[_size];
  
  memset(_ciphertextPtr, ZERO, _size);
  memset(_plaintextPtr, ZERO, _size);
  
  return ptSize;
} // SetSizeDec()



U32 CBC::SetSizeEnc(const U32 ptSize){
  U32 ctSize = ptSize+CBC_IV_SZ;
  
  _n = ptSize/AES_BLK_SZ;
  _d = ptSize%AES_BLK_SZ;
  
  if(_d) // include partial block, if one exists
    _n ++;
  
  _size = _n*AES_BLK_SZ;
  
#if defined DEBUG_OUTPUT
  std::cout << std::dec << "SetSizeEnc() _size:" << _size << " size:" << ctSize << " _n:" << _n << " _d:" << _d << std::endl << std::endl;
#endif // defined DEBUG_OUTPUT
  
  _ciphertextPtr = new U8 [_size];
  _plaintextPtr = new U8[_size];
  
  memset(_ciphertextPtr, ZERO, _size);
  memset(_plaintextPtr, ZERO, _size);

  return ctSize;
} // SetSizeEnc()



void CBC::Teardown(void){
  if(_ciphertextPtr){
    memset(_ciphertextPtr, ZERO, AES_BLK_SZ);
    delete [] _ciphertextPtr;
    _ciphertextPtr = nullptr;
  }
  if(_plaintextPtr){
    memset(_plaintextPtr, ZERO, AES_BLK_SZ);
    delete [] _plaintextPtr;
    _plaintextPtr = nullptr;
  }
} // Teardown()



void CBC::Unpack(const U8 *ciphertextPtr){
  U8  zBlock[AES_BLK_SZ];
  U32 limit;
  U32 offset;
  
  // extract initialization vector
  memcpy(_iv.buffer, ciphertextPtr, _iv.size);
  
#if defined DEBUG_OUTPUT
  std::cout << "Unpack() _iv.buffer" << std::endl;
  PrintBlocks(_iv.buffer, _iv.size);
#endif // defined DEBUG_OUTPUT
  
  if(_d)
    limit = _size-(CBC_IV_SZ+AES_BLK_SZ);
  else
    limit = _size-CBC_IV_SZ;
  
  // extract C1||C2||...||Cn-2
  offset = CBC_IV_SZ;
  memcpy(_ciphertextPtr, ciphertextPtr+offset, limit);
  
  // extract partial block Cn-1*
  offset = CBC_IV_SZ+limit;
  memcpy(_ciphertextPtr+limit, ciphertextPtr+offset, _d);
  
  // extract Cn
  offset = CBC_IV_SZ+limit+_d;
  if(_d)
    memcpy(_ciphertextPtr+limit+AES_BLK_SZ, ciphertextPtr+offset, AES_BLK_SZ);
  else
    memcpy(_ciphertextPtr+limit, ciphertextPtr+offset, AES_BLK_SZ);
  
  if(_d){
    // decrypt last block
    offset = _size-AES_BLK_SZ;
    Decrypt(_ciphertextPtr+offset, zBlock);
    
    // copy Z* into _plaintextPtr
    memcpy(_plaintextPtr+offset, zBlock, _d);
    
    // copy Z** into _ciphertextPtr
    offset = _size-(2*AES_BLK_SZ)+_d;
    memcpy(_ciphertextPtr+offset, zBlock+_d, AES_BLK_SZ-_d);
  }
  
#if defined DEBUG_OUTPUT
  std::cout << "Unpack() _ciphertextPtr" << std::endl;
  PrintBlocks(_ciphertextPtr, _n*AES_BLK_SZ);
#endif // defined DEBUG_OUTPUT
} // Unpack()



void CBC::XOR(const U8 *fromPtr, U8 *toPtr){
  for(U8 w=ZERO; w<AES_BLK_SZ; w++)
    toPtr[w] ^= fromPtr[w];
} // XOR()



/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
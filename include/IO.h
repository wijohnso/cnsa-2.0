/**
 ** @file: IO.h
 ** @brief:    header file for file and console input and output
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <iomanip>
#include <iostream>

#include "Defs.h"

#ifndef IO_H
#define IO_H

/** BUF_Create()
 ** @brief:  allocates memory for a buffer structure and returns a pointer
 ** @param:  U32 size         : requested size of the buffer structure
 ** @return: BUF              : buffer structure
 **/
BUF BUF_Create(U32 size);

/** BUF_Create()
 ** @brief:  deletes memory occupied by a buffer structure
 ** @param:  BUF oldBuffer    : buffer to delete
 ** @return: none
 **/
void BUF_Destroy(BUF &oldBuffer);

/** BUF_ToFile()
 ** @brief:  writes a buffer structure to a file
 ** @param:  BUF    *buffer   : buffer structure to write to file
 ** @param:  string fileName  : name of file to which to write
 ** @param:  U32 offset       : offset within the file where buffer should be placed
 **                             optional parameter; defaults to zero if omitted
 ** @return: none
 **/
void BUF_ToFile(BUF buffer, std::string fileName, U32 offset = ZERO);

/** FileToBUF()
 ** @brief:  reads a buffer structure from a file
 ** @param:  BUF    *buffer   : buffer structure where data should be placed
 ** @param:  string fileName  : name of file from which to read
 ** @param:  U32 offset       : offset within the file where buffer should be read
 **                             optional parameter; defaults to zero if omitted
 ** @return: none
 **/
void FileToBUF(BUF buffer, std::string fileName, U32 offset = ZERO);

/** PrintBlocks()
 ** @brief:  prints <size> bytes from <buffer>, 8 bytes per line in hex format
 ** @param:  U8  *buffer      : pointer to buffer to print
 ** @param   U32 size         : bytes of data to print
 ** @return: none
 **/
void PrintBlocks(U8 *buffer, U32 size);

/** PrintVector()
 ** @brief:  prints 12 bytes from <buffer> in hex format
 ** @param:  U8  *buffer       : pointer to buffer to print
 ** @param:  U32 size         : size of buffer to print
 ** @return: none
 **/
void PrintVector(U8 *buffer, U32 size);

#endif // IO_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
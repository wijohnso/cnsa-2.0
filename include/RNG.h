/* @Filename: RNG.h
 * @brief:    header file for CSPRNG
 *            inherits privately from class SHA
 * 
 * @copyright (C) 2023, Wil Johnson, All rights reserved
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "SHA.h"

#ifndef RNG_H
#define RNG_H

class RNG : private SHA {
public:
  /* RNG()
   * @brief: constructor
   *         declares heap memory and initializes pointer values
   *         copies a key passed as input to private member
   * @params: none
   * @return: none
   */
  RNG(void);
  
  /* ~RNG()
   * @brief: deletes allocated memory and sets pointers to nullptr
   * @params: none
   * @return: none
   */
  ~RNG(void);
  
  /* CBC_MakeIV()
   * @brief: creates a 96-bit IV for use with AES-GCM
   * @params: U8 *ivPtr         : pointer to random data buffer
   *                              allocated size should be 16 bytes
   * @return: bool              : SUCCESS (0) or FAIL (1)
   */
  bool CBC_MakeIV(U8 *ivPtr);
  
  /* GCM_MakeIV()
   * @brief: creates a 96-bit IV for use with AES-GCM
   * @params: U8 *ivPtr         : pointer to random data buffer
   *                              allocated size should be 12 bytes
   * @return: bool              : SUCCESS (0) or FAIL (1)
   */
  bool GCM_MakeIV(U8 *ivPtr);
  
  /* MakeSalt()
   * @brief: creates a 128-bit salt for use with PBKDF2
   * @params: U8 *saltPtr       : pointer to random data buffer
   *                              allocated size should be 16 bytes
   * @return: bool              : SUCCESS (0) or FAIL (1)
   */
  bool MakeSalt(U8 *saltPtr);
  
protected:

private:
  U8 *_random;            // random data
  U8 *_seedDigest;        // hashed seed data
  
  /* GetRandom()
   * @brief: reads 256 Bytes from Linux CSPRNG and hashes result into _random
   * @params: none
   * @return: none
   */
  void GetRandom(void);
  
  /* MakeBuffer()
   * @brief: creates a buffer of random bits
   * @params: U8 *bufferPtr     : pointer to buffer to contain random data
   *          U32 bufferSize    : number of bytes of random data
   * @return: bool              : SUCCESS (0) or FAIL (1)
   */
  bool MakeBuffer(U8 *bufferPtr, U32 bufferSize);
  
  /* ReadSeed()
   * @brief: reads 64 bytes of random data from file rng.txt
   * @params: none
   * @return: bool              : SUCCESS (0) or FAIL (1)
   */
  bool ReadSeed(void);
  
  /* Reseed()
   * @brief: reads seed parameters data, board_serial, chassis_serial and product_uuid;
   *         populates temp.txt, then populates this->_seed
   * @params: none
   * @return: none
   */
  void Reseed(void);
  
  /* WriteSeed()
   * @brief: writes 64 bytes of random data to file seed.txt
   * @params: none
   * @return: none
   */
  void WriteSeed(void);
};

#endif

/* @copyright (C) 2024, Wil Johnson, All rights reserved
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
 
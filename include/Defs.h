/**
 ** @file: Defs.h
 ** @brief:    header file for common constants in Crypto project
 **            includes typedefs for unsigned integers of various sizes
 **            includes universal size constants
 **            includes universal AES-256 constants from FIPS 197 and FIPS 800-38d
 **            includes universal SHA-512 constants from FIPS 180-4
 **            includes universal AES-256-GCM constants from FIPS 800-38d
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <bitset>
#include <cstring>

#ifndef DEFS_H
#define DEFS_H

// universal return constants
#define SUCCESS           false     // success
#define FAIL              true      // fail

// universal size constants
#define BASE_TWO          2         // base 2 for exponential calculations

#define BITS_PER_U8       8         // bits per uint8_t byte
#define BITS_PER_U16      16        // bits per uint16_t short
#define BITS_PER_U32      32        // bits per uint32_t word
#define BITS_PER_U64      64        // bits per uint64_t long

#define BYTS_PER_U16      2         // bytes per uint16_t short
#define BYTS_PER_U32      4         // bytes per uint32_t word
#define BYTS_PER_U64      8         // bytes per uint64_t long

#define NIBS_PER_U8       2         // nibbles per uint8_t byte
#define NIBS_PER_U16      4         // nibbles per uint16_t short
#define NIBS_PER_U32      8         // nibbles per uint32_t word
#define NIBS_PER_U64      16        // nibbles per uint64_t long

#define ONE               0x01      // literal one
#define ZERO              0x00      // literal zero

typedef uint8_t  U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;
typedef ssize_t  SSIZE;

typedef struct{
  U32 size;
  U8* buffer;
}BUF;

#endif // DEFS_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
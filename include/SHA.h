/** @file: SHA.h
 ** @brief:    header file for SHA-512 and SHA-512/256 hashing in accordance with FIPS 180-4
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "Defs.h"

#ifndef SHA_H
#define SHA_H

#define TRNC     true     // truncated mode for SHA-512/256 (32 byte digests)
#define NORM     false    // normal mode for SHA-512 (64 byte digests)

// universal SHA-512 constants from FIPS 180-4
#define SHA_192_DGST_SZ   24        // bytes in a SHA-256/192 digest
#define SHA_256_DGST_SZ   32        // bytes in a SHA-256 or SHA-512/256 digest
#define SHA_512_DGST_SZ   64        // bytes in a SHA-512 digest

class SHA {
public:
  /** SHA()
   ** @brief:  default constructor
   **          declares heap memory and initializes pointer values
   **          copies a key passed as input to private member
   ** @param:  none
   ** @return: none
   **/
  SHA(void);
  
  /** SHA()
   ** @brief:  constructor
   **          declares heap memory and initializes pointer values
   **          copies a key passed as input to private member
   ** @param:  bool mode         : bool representing mode of operation
   **                              TRNC = truncated SHA-512/256 | NORM = SHA-512
   ** @return: none
   **/
  SHA(bool mode);
  
  
  /** ~SHA()
   ** @brief:  deletes allocated memory and sets pointers to nullptr
   ** @param:  none
   ** @return: none
   **/
  ~SHA(void);
  
  
  /** SHA256()
   ** @brief:  performs the SHA-256 algorithm on input of size to produce output
   ** @param:  U8  *inputPtr     : pointer to original input buffer
   ** @param:  U64 size          : size of input buffer in bytes
   ** @param:  U64 *outputPtr    : pointer to final message digest buffer
   **                              allocated size should be 32 bytes
   ** @return: none
   **/
  void SHA256(const U8 *inputPtr, U8 *outputPtr, U64 size);
  
  
  /** SHA512()
   ** @brief:  performs the SHA-512 or SHA-512/256 algorithm on input of size to produce output
   ** @param:  U8  *inputPtr     : pointer to original input buffer
   ** @param:  U64 size          : size of input buffer in bytes
   ** @param:  U64 *outputPtr    : pointer to final message digest buffer
   **                              allocated size should be 64 bytes for SHA-512
   **                              allocated size should be 32 bytes for SHA-512/256
   ** @return: none
   **/
  void SHA512(const U8 *inputPtr, U8 *outputPtr, U64 size);
  
protected:
  // intentionally left empty
  
private:
  U64  _inputBytes;         // block-sligned buffer size in bytes
  U64  _inputWords;         // buffer size in 8-byte words
  U64  *_h64Ptr;            // pointer to 512-bit hash buffer
  U64  *_v64Ptr;            // a, b, c, d, e, f, g, h buffer
  U64  *_w64Ptr;            // message schedule buffer
  U32  *_h32Ptr;            // pointer to 512-bit hash buffer
  U32  *_v32Ptr;            // a, b, c, d, e, f, g, h buffer
  U32  *_w32Ptr;            // message schedule buffer
  U8   *_inputStringPtr;    // pointer to input buffer
  U8   _inputBlocks;        // number of 1024-bit input blocks
  bool _mode;               // mode of operation TRNC | NORM
  
  /** Ch()
   ** @brief:  (x&y)^(~x&z)
   **          for use with SHA-256
   ** @param:  U32 x             : it's x
   ** @param:  U32 y             : it's y
   ** @param:  U32 z             : it's z
   ** @return: U32               : return value
   **/
  U32 Ch(U32 x, U32 y, U32 z);
  
  /** Ch()
   ** @brief:  (x&y)^(~x&z)
   **          for use with SHA-512
   ** @param:  U64 x             : it's x
   ** @param:  U64 y             : it's y
   ** @param:  U64 z             : it's z
   ** @return: U64               : return value
   **/
  U64 Ch(U64 x, U64 y, U64 z);
  
  /** HashDigest_256()
   ** @brief:  creates the final 256-bit hash digest
   ** @param:  U8 *outputPtr     : array to store the final digest
   ** @return: none
   **/
  void HashDigest_256(U8 *outputPtr);
  
  /** HashDigest_512()
   ** @brief:  creates the final 256-bit hash digest from the 256 left bits of
   **          the SHA-512 hash digest
   ** @param:  U8 *outputPtr     : array to store the final digest
   ** @return: none
   **/
  void HashDigest_512(U8 *outputPtr);
  
  /** HashRound_256()
   ** @brief:  performs the SHA-256 algorithm specified in NIST.FIPS.180-4 for a single input block
   **          for use with SHA-256
   ** @param:  none
   ** @return: none
   **/
  void HashRound_256(void);
  
  /** HashRound_512()
   ** @brief:  performs the SHA-512 algorithm specified in NIST.FIPS.180-4 for a single input block
   **          for use with SHA-512
   ** @param:  none
   ** @return: none
   **/
  void HashRound_512(void);
  
  /** InitHash_256()
   ** @brief:  initializes private member _hPtr by copying from global U64 H
   **          for use with SHA-256
   ** @param:  none
   ** @return: none
   **/
  void InitHash_256(void);
  
  /** InitHash_512()
   ** @brief:  initializes private member _hPtr by copying from global U64 H
   **          for use with SHA-512
   ** @param:  none
   ** @return: none
   **/
  void InitHash_512(void);
  
  /** Maj()
   ** @brief:  (x&y)^(x&z)^(y&z)
   **          for use with SHA-256
   ** @param:  U32 x             : it's x
   ** @param:  U32 y             : it's y
   ** @param:  U32 z             : it's z
   ** @return: U32               : return value
   **/
  U32 Maj(U32 x, U32 y, U32 z);
  
  /** Maj()
   ** @brief:  (x&y)^(x&z)^(y&z)
   **          for use with SHA-512
   ** @param:  U64 x             : it's x
   ** @param:  U64 y             : it's y
   ** @param:  U64 z             : it's z
   ** @return: U64               : return value
   **/
  U64 Maj(U64 x, U64 y, U64 z);
  
  /** MessageSchedule_256()
   ** @brief: creates the message schedule for each of 80 rounds of SHA-512/256 according to 
   **         NIST.FIPS.180-4
   **         for use with SHA-256
   ** @param:  U32 n             : starting block for the message schedule
   ** @return: none
   **/
  void MessageSchedule_256(U32 n);
  
  /** MessageSchedule_512()
   ** @brief:  creates the message schedule for each of 80 rounds of SHA-512/256 according to 
   **          NIST.FIPS.180-4
   **          for use with SHA-512
   ** @param:  U32 n             : starting block for the message schedule
   ** @return: none
   **/
  void MessageSchedule_512(U32 n);
  
  /** Pad_256()
   ** @brief:  pads the original input string according to NIST.FIPS.180-4 specifications
   **          for use with SHA-256
   ** @param:  U8 *inputPtr      : input string
   ** @param:  U64 size          : size of input in bytes
   ** @return: none
   **/
  void Pad_256(const U8 *inputPtr, U64 size);
  
  /** Pad_512()
   ** @brief:  pads the original input string according to NIST.FIPS.180-4 specifications
   **          for use with SHA-512
   ** @param:  U8 *inputPtr      : input string
   ** @param:  U64 size          : size of input in bytes
   ** @return: none
   **/
  void Pad_512(const U8 *inputPtr, U64 size);
  
  /** Rho()
   ** @brief:  ROTR(x,1)^ROTR(x,8)^SHR(x,7) or
   **          ROTR(x,19)^ROTR(x,61)^SHR(x,6)
   **          for use with SHA-256
   ** @param:  U32 x             : it's x
   ** @param:  U8 mode           : operating mode (0 or 1)
   ** @return: U32               : result of calculation
   **/
  U32 Rho(U32 x, U8 mode);
  
  /** Rho()
   ** @brief:  ROTR(x,1)^ROTR(x,8)^SHR(x,7) or
   **          ROTR(x,19)^ROTR(x,61)^SHR(x,6)
   **          for use with SHA-512
   ** @param:  U64 x             : it's x
   ** @param:  U8 mode           : operating mode (0 or 1)
   ** @return: U64               : result of calculation
   **/
  U64 Rho(U64 x, U8 mode);
  
  /** ROTR()
   ** @brief:  performs circular shift on toShift to the right by shiftDistance bits
   **          for use with SHA-256
   ** @param:  U32 toShift       : integer to be shifted
   ** @param:  U8  shiftDistance : shift distance
   ** @return: U32               : toShift rotated right by shiftDistance bits
   **/
  U32 ROTR(U32 toShift, U8 shiftDistance);
  
  /** ROTR()
   ** @brief:  performs circular shift on toShift to the right by shiftDistance bits
   **          for use with SHA-512
   ** @param:  U64 toShift       : integer to be shifted
   ** @param:  U8  shiftDistance : shift distance
   ** @return: U64               : toShift rotated right by shiftDistance bits
   **/
  U64 ROTR(U64 toShift, U8 shiftDistance);
  
  /** SHR()
   ** @brief:  shifts toShift to the right by shiftDistance bits
   **          for use with SHA-256
   ** @param:  U32 toShift       : integer to be shifted
   ** @param:  U8  shiftDistance : shift distance
   ** @return: U32               : toShift shifted right by shiftDistance bits
   **/
  U32 SHR(U32 toShift, U8 shiftDistance);
  
  /** SHR()
   ** @brief:  shifts toShift to the right by shiftDistance bits
   **          for use with SHA-512
   ** @param:  U64 toShift       : integer to be shifted
   ** @param:  U8  shiftDistance : shift distance
   ** @return: U64               : toShift shifted right by shiftDistance bits
   **/
  U64 SHR(U64 toShift, U8 shiftDistance);
  
  /** Sigma()
   ** @brief:  ROTR(x,28)^ROTR(x,34)^ROTR(x,39) or
   **          ROTR(x,14)^ROTR(x,18)^ROTR(x,41)
   **          for use with SHA-256
   ** @param:  U32 x             : it's x
   ** @param:  U8  mode          : operating mode (0 or 1)
   ** @return: U32               : result of calculation
   **/
  U32 Sigma(U32 x, U8 mode);
  
  /** Sigma()
   ** @brief:  ROTR(x,28)^ROTR(x,34)^ROTR(x,39) or
   **          ROTR(x,14)^ROTR(x,18)^ROTR(x,41)
   **          for use with SHA-512
   ** @param:  U64 x             : it's x
   ** @param:  U8  mode          : operating mode (0 or 1)
   ** @return: U64               : result of calculation
   **/
  U64 Sigma(U64 x, U8 mode);
}; // class SHA

#endif // SHA_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/**
 ** @file: HMAC.h
 ** @brief:    header file for HMAC generation in accordance with FIPS 198-1
 **            inherits publically from class SHA
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "SHA.h"

#ifndef HMAC_H
#define HMAC_H

class HMAC : virtual public SHA {
public:
  /** HMAC()
   ** @brief:  constructor
   ** @param:  none
   ** @return: none
   **/
  HMAC(void);
  
  /** ~HMAC()
   ** @brief:  destructor
   ** @param:  none
   ** @return: none
   **/
  ~HMAC(void);
  
  /** HMAC_Hash()
   ** @brief:  Calculates HMAC digest 
   ** @param:  U8  *digestPtr    : pointer to digest buffer
   **                              allocated size should be 64 bytes
   ** @param:  BUF input         : buffer structure for plaintext buffer to be hashed
   ** @param:  BUF key           : buffer structure for key size and key material
   ** @return: none
   **/
  void HMAC_Hash(U8 *digestPtr, const BUF input, const BUF key);
  
protected:
  // intentionally left empty
  
private:
  BUF _ipad;                // buffer structure for inner pad size and inner pad block
  BUF _opad;                // buffer structure for outer pad size and outer pad block
  U8  *_digestPtr;          // pointer to output buffer
  U8  *_k0Ptr;              // pointer to the padded key, K0, buffer
  
  /** HMAC_Init()
   ** @brief:  Pads the 32-byte key to create 128-byte K0
   ** @param:  BUF key           : buffer structure for key size and key material
   ** @param:  U32 size          : size of plaintext input
   ** @return: none
   **/
  void HMAC_Init(const BUF key, const U32 size);
  
  /** XOR()
   ** @brief:  XOR two 4-word (128-bit) arrays, in and out, and store the result in array, out
   ** @param:  U8 *toPtr         : pointer to 4-word array representing XOR input
   ** @return: none
   **/
  void XOR(U8 *toPtr);
}; // class HMAC

#endif // HMAC_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/**
 ** @file: GCM.h
 ** @brief:    header file for AES-256 GCM in accordance with FIPS 800-38d
 **            inherits publically from class AES
 **            inherits publically from class DRBG
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "AES.h"
#include "DRBG.h"

#ifndef GCM_H
#define GCM_H

// universal AES-256 constants from FIPS 197 and FIPS 800-38d
#define GCM_IV_SZ         12        // gcm initialization vector size in bytes

class GCM : virtual public AES, virtual public DRBG {
public:
  /** GCM()
   ** @brief:  constructor
   ** @param:  none
   ** @return: none
   **/
  GCM(void);
  
  /** ~GCM()
   ** @brief:  destructor
   ** @param:  none
   ** @return: none
   **/
  ~GCM(void);
  
  /** GCM_Decrypt()
   ** @brief:  AES-256 GCM encryption engine
   ** @param:  U8  *ciphertextPtr: pointer to ciphertext buffer to be decrypted
   ** @param:  U8  *keyPtr       : pointer to key buffer containing with 16 bytes of key material
   ** @param:  U8  *plaintextPtr : pointer to plaintext buffer buffer
   **                              allocated size should be ((<size> - 3) **16 bytes       
   ** @param:  U32  size         : size of ciphertext in bytes
   ** @return: bool              : SUCCESS (0) or FAIL (1)
   **/
  bool GCM_Decrypt(const U8 *ciphertextPtr, const U8 *keyPtr, U8 *plaintextPtr, U32 &size);
  
  /** GCM_Encrypt()
   ** @brief:  AES-256 GCM encryption engine
   ** @param:  U8  *ciphertextPtr: pointer to ciphertext buffer
   **                              allocated size should be ((<size> / AES_BLK_SZ) + 4) **AES_BLK_SZ bytes
   ** @param:  U8  *keyPtr       : pointer to key buffer filled with 16 bytes of key material
   ** @param:  U8  *plaintextPtr : pointer to plaintext buffer to be encrypted
   ** @param:  U32  size         : size of plaintext in bytes
   ** @return: none
   **/
  void GCM_Encrypt(U8 *ciphertextPtr, const U8 *keyPtr, const U8 *plaintextPtr, U32 &size);
  
protected:
  // intentionally left empty

private:
  BUF _tagPrime;            // buffer structure for tag prime size and tag prime block (for comparison to _tagPtr)
  U32 _padSize;             // size of ciphertext padding in bytes
  U32 _size;                // size of plaintext in bytes
  U8  *_cbPtr;              // pointer to the counter block buffer
  U8  *_ciphertextPtr;      // pointer to the ciphertext output block buffer
  U8  *_hashSubKey;         // pointer to the hash subkey buffer
  U8  *_j0_Ptr;             // pointer to the initialization vector buffer
  U8  *_resultPtr;          // pointer to the result blocks buffer
  U8  *_sizePtr;            // pointer to 16-byte size block buffer
  U8  *_sPtr;               // pointer to S block containing the ghash for tag creation buffer
  U8  *_tagPtr;             // pointer to GCM tag block buffer
  
  /** Gctr()
   ** @brief: TODO: finish comment
   ** @param:  U8 *inputPtr      : bitstream of arbitrary length representing input
   ** @param:  U8 *outputPtr     : bitstream of length xLen representing output
   ** @param:  U32 size          : length of bitstream input in bytes rounded up to the nearest full block size
   ** @return: none
   **/
  void Gctr(const U8 *inputPtr, U8 *outputPtr, U32 size);

  /** GenerateSubKey()
   ** @brief:  generates AES-256 GCM hash subkey by applying the block cipher to a "zero" block
   ** @param:  none
   ** @return: none
   **/
  void GenerateSubKey(void);

  /** Ghash()
   ** @brief:  authentication mechanism of GCM
   ** @param:  U8 *inputPtr      : input bitstream of length size bytes
   ** @param:  U32 size          : length of input in bytes
   ** @return: 
   **/
  void Ghash(U8 *inputPtr, U32 size);

  /** IncrementCB()
   ** @brief:  increments the last 32 bits of _cbPtr
   ** @param:  none
   ** @return: none
   **/
  void IncrementCB(void);

  /** GCM_MakeJ0()
   ** @brief:  sets the 16-byte padded _j0 from a 12-byte initialization vector
   ** @param:  none
   ** @return: none
   **/
  void GCM_MakeJ0(void);

  /** Multiply()
   ** @brief:  performs gallois multiplication on two 16-byte arrays: x*y=z
   ** @param:  U8 *xPTr          : it's x
   ** @param:  U8 *yzPtr         : it's x
   ** @return: none
   **/
  void Multiply(U8 *xPTr, U8 *yzPtr);

  /** Pack()
   ** @brief:  packs plaintext size, IV and GCM tag to end of ciphertext buffer
   **              ciphertext                _resultPtr
   **          +------------------+      +------------------+
   **          |      IV (12)     |  <-  |      IV (12)     |
   **          |xxxxxxxxxxxxxxxxxx|      |      0x00 (4)    |
   **          +------------------+      +------------------+
   **          |                  |  <-  |                  |
   **          |  ciphertext (x)  |  <-  |  ciphertext (x)  |
   **          |                  |  <-  |                  |
   **          |xxxxxxxxxxxxxxxxxx|      | 0x00 (16-(x%16)) |
   **          +------------------+      +------------------+
   **          |  size block (16) |  <-  |  size block (16) |
   **          +------------------+      +------------------+
   **          |    GCM tag (16)  |  <-  |    GCM tag (16)  |
   **          +------------------+      +------------------+
   ** @param:  U8 *ciphertext    : pointer to ciphertext buffer
   ** @return: none
   **/
  void Pack(U8 *ciphertext);

  /** RightShift()
   ** @brief:  shift a 4-word (128-bit) array to the right (toward LSB) a distance of one (1) bit
   ** @param:  U8 *inputPtr      : pointer to 4-word array representing array to be shifted
   ** @return: none
   **/
  void RightShift(U8 *inputPtr);

  /** SetCounterBlock()
   ** @brief:  sets counter block from _j0_Ptr
   ** @param:  none
   ** @return: none
   **/
  void SetCounterBlock(void);

  /** SetSizeDec()
   ** @brief:  sets the ciphertext size, rounded up to the nearest block, and saves the value
   **          in the _size member
   ** @param:  U32 ptSize        : size of plaintext in bytes
   ** @return: none
   **/
  void SetSizeDec(U32 &ptSize);

  /** SetSizeEnc()
   ** @brief:  calculates the ciphertext size, rounded up to the nearest block, and saves the value
   **          in the _size member
   ** @param:  U32 ptSize        : size of plaintext in bytes
   ** @return: none
   **/
  void SetSizeEnc(U32 &ptSize);
  
  /** Unpack()
   ** @brief:  packs plaintext size, IV and GCM tag to end of ciphertext buffer
   **              ciphertext                  _resultPtr
   **          +------------------+        +------------------+
   **          |      IV (12)     |   ->   |      IV (12)     |
   **          |xxxxxxxxxxxxxxxxxx|   ->   |      0x00 (4)    |
   **          +------------------+        +------------------+
   **          |                  |   ->   |                  |
   **          |  ciphertext (x)  |   ->   |  ciphertext (x)  |
   **          |                  |   ->   |                  |
   **          |xxxxxxxxxxxxxxxxxx|   ->   | 0x00 (16-(x%16)) |
   **          +------------------+        +------------------+
   **          |  size block (16) |   ->   |  size block (16) |
   **          +------------------+        +------------------+
   **          |    GCM tag (16)  |   ->   |    GCM tag (16)  |
   **          +------------------+        +------------------+
   ** @param:  U8  *ciphertext   : pointer to ciphertext buffer
   ** @param:  U32 size          : ciphertext size
   ** @return: bool              : SUCCESS (0) or FAIL (1)
   **/
  bool Unpack(const U8 *ciphertext, U32 &size);

  /** XOR()
   ** @brief:  XOR two 4-word (128-bit) arrays, in and out, and store the result in array, out
   ** @param:  U8 *fromPtr       : pointer to 4-word array representing XOR input
   ** @param:  U8 *toPtr         : pointer to 4-word array representing XOR input
   ** @return: none
   **/
  void XOR(const U8 *fromPtr, U8 *toPtr);
}; // class GCM

#endif // GCM_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/**
 ** @file: KDF.h
 ** @brief:    header file for PBKDF2 in accordance with NIST SP 800-132
 **            inherits publically from class HMAC
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "HMAC.h"

#ifndef KDF_H
#define KDF_H

/** PBKDF strategies:
 *
 ** DPK   = Data Protection Key (optional)
 ** KDF   = Key Derivation Function (NIST SP 800-108)
 ** MK    = Master Key  = PBKDF(password)
 ** PBKDF = Password Based Key Derivation Function (NIST SP 800-132)
 **
 ** 1 - Use MK as DPK directly (not recommended):
 **     Enc(PBKDF(password), ptData)
 **
 ** 2 - Use KDF on MK to derive DPK:
 **     Enc(KDF(PBKDF(password)), ptData)
 **
 ** 3 - Use MK to encrypt DPK (not recommended):
 **     Enc(Enc(PBKDF(password), DPK), ptData)
 **
 ** 4 - Use KDF on MK to derive key to encrypt DPK (requires encrypted key file for DPK):
 **     Enc(Enc(KDF(PBKDF(password)), DPK), ptData)
 **/

class KDF : virtual public HMAC {
public:
  /** KDF()
   ** @brief:  constructor
   ** @param:  none
   ** @return: none
   **/
  KDF(void);
  
  /** ~KDF()
   ** @brief:  destructor
   ** @param:  none
   ** @return: none
   **/
  ~KDF(void);
  
  /** DPK()
   ** @brief:  derives data protection key from master key using key derivation formula (KDF)
   **          in double pipeline mode
   ** @param:  BUF context       : buffer structure for context size and context material
   ** @param:  BUF dpk           : buffer structure for requested key size and requested key material
   ** @param:  BUF label         : buffer structure for label size and label material
   ** @param:  BUF mk            : buffer structure for master key size and master key material
   ** @return: none
   **/
  void DPK(const BUF context, BUF dpk, const BUF label, const BUF mk);
  
  /** MK()
   ** @brief:  derives key material from salt and password using password-based key derivation
   **          formula (PBKDF)
   ** @param:  U8 hashIters      : number of hash iterations for the KDF algorithm to run
   ** @param:  BUF key           : buffer structure for key (password) size and key (password) material
   ** @param:  BUF mk            : buffer structure for requested master key size and master key material
   ** @param:  BUF salt          : buffer structure for salt size and salt material
   ** @return: none
   **/
  void MK(const U32 hashIters, const BUF key, BUF mk, const BUF salt);
  
protected:
  // intentionally left empty

private:
  BUF _a;                   // buffer structure for _a size and _a material
  BUF _context;             // buffer structure for context size and context material
  BUF _hashKey;             // buffer structure for hash key size and hash key material
  BUF _k;                   // buffer structure for _k size and _k material
  BUF _label;               // buffer structure for label size and label material
  BUF _t;                   // buffer structure for _t size and _t material
  BUF _u;                   // buffer structure for HMAC digest buffer for PBKDF

  /** DPK_Hash()
   ** @brief:  performs HMAC hash for KDF
   ** @param:  none
   ** @return: none
   **/
  void DPK_Hash(void);

  /** DPK_Init()
   ** @brief:  initializes member variables prior to key derivation
   ** @param:  BUF context       : buffer structure for context size and context material
   ** @param:  BUF label         : buffer structure for label size and label material
   ** @param:  BUF mk            : buffer structure for master key size and master key material
   ** @return: none
   **/
  void DPK_Init(const BUF context, const BUF label, const BUF mk);
  
  /** MK_Hash()
   ** @brief:  performs HMAC hash for KDF
   ** @param:  none
   ** @return: none
   **/
  void MK_Hash(void);
  
  /** MK_Init()
   ** @brief:  initializes member variables prior to key derivation
   ** @param:  BUF key           : buffer structure for key (password) size and key (password) material
   ** @param:  BUF salt          : buffer structure for salt size and salt material
   ** @return: none
   **/
  void MK_Init(const BUF key, const BUF salt);
}; // class KDF

#endif // KDF_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
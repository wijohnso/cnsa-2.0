/**
 ** @file: CBC.h
 ** @brief:    header file for AES-256 CBC in accordance with FIPS 800-38a
 **            inherits publically from class AES
 **            inherits publically from class DRBG
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "AES.h"
#include "DRBG.h"

#ifndef CBC_H
#define CBC_H

// universal AES-256 constants from FIPS 197 and FIPS 800-38d
#define CBC_IV_SZ         16        // cbc initialization vector size in bytes

class CBC : virtual public AES, virtual public DRBG {
public:
  /** CBC()
   ** @brief:  constructor
   ** @param:  none
   ** @return: none
   **/
  CBC(void);
  
  /** ~CBC()
   ** @brief:  destructor
   ** @param:  none
   ** @return: none
   **/
  ~CBC(void);
  
  /** CBC_Decrypt()
   ** @brief:  AES-256 CBC encryption engine
   ** @param:  BUF ciphertext    : buffer structure for ciphertext size and ciphertext material
   **                              ciphertext buffer should be preceeded with 16-byte iv used in encryption
   ** @param:  U8  *keyPtr       : pointer to key buffer containing with 16 bytes of key material
   ** @param:  BUF plaintext     : buffer structure for plaintext size and plaintext material
   **                              allocated size should be (<size> 1 16) bytes
   ** @return: none
   **/
  void CBC_Decrypt(const BUF ciphertext, const U8 *keyPtr, BUF &plaintext);
  
  /** CBC_Encrypt()
   ** @brief:  AES-256 CBC encryption engine
   ** @param:  BUF ciphertext    : buffer structure for ciphertext size and ciphertext material
   **                              allocated ciphertext buffer size should be (<size> + 16) bytes
   ** @param:  U8  *keyPtr       : pointer to key buffer filled with 16 bytes of key material
   ** @param:  BUF plaintext     : buffer structure for plaintext size and plaintext material
   ** @return: none
   **/
  void CBC_Encrypt(BUF &ciphertext, const U8 *keyPtr, const BUF plaintext);
  
protected:
  // intentionally left empty
  
private:
  BUF _iv;                  // buffer structure for iv size and iv material
  U32 _d;                   // bytes in the last partial block
  U32 _n;                   // number of full AES-256 blocks
  U32 _size;                // padded plaintext/ciphertext size in bytes
  U8  *_ciphertextPtr;      // pointer to ciphertext buffer
  U8  *_plaintextPtr;       // pointer to plaintext buffer with padding
  
  /** Pack()
   ** @brief:  copies ciphertext to return to user
   **               ciphertext
   **          +-------------------+
   **          |       IV (16)     |
   **          +-------------------+
   **          | C1||C2||...||Cn-2 |
   **          +-------------------+
   **          |    Cn-1* (_d)     |
   **          +-------------------+
   **          |      Cn (16)      |
   **          +-------------------+
   ** @param:  U8  *ciphertextPtr: pointer to buffer filled with <size> bytes of ciphertext
   ** @return: none
   **/
  void Pack(U8* ciphertextPtr);
  
  /** SetSizeDec()
   ** @brief:  calculates the ciphertext size, rounded up to the nearest block, and saves the value
   **          in the _size member
   ** @param:  U32 ctSize        : size of ciphertext in bytes
   ** @return: U32               : size of plaintext buffer
   **/
  U32 SetSizeDec(const U32 ctSize);
  
  /** SetSizeEnc()
   ** @brief:  calculates the ciphertext size, rounded up to the nearest block, and saves the value
   **          in the _size member
   ** @param:  U32 ptSize        : size of plaintext in bytes
   ** @return: U32               : size of ciphertext buffer
   **/
  U32 SetSizeEnc(const U32 ptSize);
  
  /** Teardown()
   ** @brief:  deletes locally allocated heap memory
   ** @param:  none
   ** @return: none
   **/
  void Teardown(void);
  
  /** Unpack()
   ** @brief:  packs plaintext size, IV and GCM tag to end of ciphertext buffer
   **               ciphertext
   **          +-------------------+
   **          |       IV (16)     |
   **          +-------------------+
   **          | C1||C2||...||Cn-2 |
   **          +-------------------+
   **          |    Cn-1* (_d)     |
   **          +-------------------+
   **          |      Cn (16)      |
   **          +-------------------+
   ** @param:  U8  *ciphertext   : pointer to ciphertext buffer
   ** @return: none
   **/
  void Unpack(const U8 *ciphertext);
  
  /** XOR()
   ** @brief:  XOR two 4-word (128-bit) arrays, in and out, and store the result in array, out
   ** @param:  U8 *fromPtr       : pointer to 4-word array representing XOR input
   ** @param:  U8 *toPtr         : pointer to 4-word array representing XOR input
   ** @return: none
   **/
  void XOR(const U8 *fromPtr, U8 *toPtr);
}; // class CBC
  
#endif // CBC_H
  
/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
/**
 ** @file: DRBG.h
 ** @brief:    header file for DRBG in accordance with NIST SP 800-90A
 **            inherits publically from class HMAC
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "HMAC.h"

#ifndef DRBG_H
#define DRBG_H

class DRBG : virtual public HMAC {
public:
  /** DRBG()
   ** @brief:  default constructor
   **          instantiates objects of class DRBG
   **          declares memory for key and random vectors
   ** @param:  none
   ** @return: none
   **/
  DRBG(void);
  
  /** ~DRBG();
   ** @brief:  default destructor
   **          uninstantiates objects of class DRBG
   ** @param:  none
   ** @return: none
   **/
  ~DRBG(void);
  
  /** Generate()
   ** @brief:  generates random bits using DRBG
   ** @param:  U8 *bufferPtr     : pointer to buffer to contain random data
   ** @param:  U32 bufferSize    : number of bytes of random data requested
   ** @return: bool              : SUCCESS (0) or FAIL (1)
   **/
  bool Generate(U8 *bufferPtr, const U32 bufferSize);
  
  /** Reseed()
   ** @brief:  reseeds DRBG by calculating new K and V values
   ** @param:  none
   ** @return: none
   **/
  void Reseed(void);
  
protected:
  // intentionally left empty
  
private:
  BUF _key;                 // buffer structure for key size and key material
  BUF _v;                   // buffer structure for state vector (contains psuedo-random data)
  U32 _reseedCtr;           // number of generations since last seed
  
  /** Update()
   ** @brief:  performs HMAC_Hash on random data to instantiate a DRBG
   ** @param:  bool addEntropy   : entropy flag
   ** @return: none
   **/
  void Update(const bool addEntropy);
}; // class DRBG

#endif // DRBG_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
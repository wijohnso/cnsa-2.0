/**
 ** @file: LMS.h
 ** @brief:    header file for Leighton-Micali Signatures (LMS)
 **            in accordance with NIST SP 800-132
 **            inherits publically from class KDF
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include <string>

#include "DRBG.h"
#include "KDF.h"

#ifndef LMS_H
#define LMS_H

class LMS : virtual public DRBG, virtual public KDF {
public:
  /** LMS()
   ** @brief:  default constructor
   **          declares heap memory and initializes pointer values
   ** @param:  none
   ** @return: none
   **/
  LMS(void);
  
  /** ~LMS()
   ** @brief:  deletes allocated memory and sets pointers to nullptr
   ** @param:  none
   ** @return: none
   **/
  ~LMS(void);
  
  /** Initialize()
   ** @brief:  initializes _seed, calculates _rootKey, and resets _currentIndex
   ** @param:  string keyFile    : string containing the filename of the public key file
   ** @param:  string stateFile  : string containing the filename of the seed file
   ** @return: bool              : SUCCESS (0) or FAIL (1)
   **/
  bool Initialize(std::string keyFile, std::string stateFile);
  
  /** Sign()
   ** @brief:  signs digest using a key at an index
   ** @param:  string keyFile    : string containing the filename of the public key file
   ** @param:  BUF buffer        : buffer structure containing length and material to be signed
   ** @param:  string stateFile  : string containing the filename of the seed file
   ** @pre:    buffer.buf should contain 2048 empty bytes to contain signature
   ** @pre:    buffer.size should contain the length of material to be signed
   ** @post:   buffer.size will contain the length of the entire signed buffer
   ** @return: bool              : SUCCESS (0) or FAIL (1)
   **/
  bool Sign(std::string keyFile);
  
  /** Verify()
   ** @brief:  verifies a signature using a key at an index
   ** @param:  BUF buffer        : buffer structure containing buffer length and buffer material
   ** @param:  BUF key           : buffer structure containing public key length and public key material
   **                              should contain 32 bytes
   ** @return: bool              : SUCCESS (0) or FAIL (1)
   **/
  bool Verify(BUF digest, BUF key);
  
protected:
  // intentionally left empty
  
private:
  BUF  **_merkleTree;       // buffer structure pointing to Merkle tree array
  BUF  *_keys;              // buffer structure containing to key array
  BUF  *_signature;         // buffer structure pointing to signature array
  BUF  *_subtreeL;          // buffer structure pointing to to left subtree array
  BUF  *_subtreeR;          // buffer structure pointing to to right subtree array
  BUF  _MK_Key;             // buffer structure containing hash key for MK()
  BUF  _MK_Salt;            // buffer structure containing hash salt for MK()
  BUF  _seed;               // buffer structure containing seed size and seed material
  U32  *_subIndices;        // array of indices for populating subtree
  U32  *_topIndices;        // array of indices for populating Merkle tree
  U32  _currentKeyset;      // current key index
  bool *_subLevels;         // boolean array to inform which subtree levels are occupied
  bool *_topLevels;         // boolean array to inform which Merkle tree levels are occupied
  
  /** InsertKeyInSubtree()
   ** @brief:  inserts a single public key into a subtree
   **          the following values are concatenated and hashed with SHA256
   **          +-----------------------------------+
   **          | keyFile name         (1x 20 byte) |
   **          +-----------------------------------+
   **          | ID_TREE              (1x 4 bytes) |
   **          +-----------------------------------+
   **          | keyNum               (1x 4 bytes) |
   **          +-----------------------------------+
   **          | level                 (8x 1 byte) |
   **          +-----------------------------------+
   **          | _keys[keysIndex]    (1x 32 bytes) |
   **          +-----------------------------------+
   **          | _subtree[keysIndex] (1x 32 bytes) |
   **          +-----------------------------------+
   ** @param:  string keyFile    : string containing the filename of the public key file
   ** @param:  U32 keysIndex     : index for _keys BUF structure
   ** @param:  U32 keyNum        : the key number for this range
   ** @param:  BUF *subtree      : pointer to subtree array for insertion
   ** @return: none
   **/
  void InsertKeyInSubtree(std::string keyFile, U32 keysIndex, U32 keyNum, BUF *subtree);
  
  /** InsertKeyInMerkleTree()
   ** @brief:  inserts the current _publicKey into the Merkle tree
   **          the following values are concatenated and hashed with SHA256
   **          +-----------------------------------+
   **          | keyFile name         (1x 20 byte) |
   **          +-----------------------------------+
   **          | ID_TREE              (1x 4 bytes) |
   **          +-----------------------------------+
   **          | _topIndices[level]   (1x 4 bytes) |
   **          +-----------------------------------+
   **          | level                 (8x 1 byte) |
   **          +-----------------------------------+
   **          | _keys[keysIndex]    (1x 32 bytes) |
   **          +-----------------------------------+
   **          | _subtree[keysIndex] (1x 32 bytes) |
   **          +-----------------------------------+
   ** @param:  string keyFile    : string containing the filename of the key file
   ** @return: none
   **/
  void InsertKeyInMerkleTree(std::string keyFile);
  
  /** MakePrivateKey()
   ** @brief:  makes a single private key using DPK()
   ** @param:  U32 keysIndex     : index for _keys BUF structure
   ** @param:  U8 keysetIndex    : index within keySet (range 0 to 33)
   ** @param:  U32 keyset        : the keyset for this range
   ** @return: none
   **/
  void MakePrivateKey(U32 keysIndex, U8 keysetIndex, U32 keyset);
  
  /** MakePrivateKeys()
   ** @brief:  makes private keys within a set range
   ** @param:  U32 end           : key end range
   ** @param:  U32 keysIndex     : index for _keys BUF structure
   ** @param:  U32 keyset        : the keyset for this range
   ** @param:  U32 start         : key start range
   ** @return: none
   **/
  void MakePrivateKeys(U32 end, U32 keysIndex, U32 keyset, U32 start);
  
  /** MakePublicKey()
   ** @brief:  makes a single public key using a private key and Winternitz()
   ** @param:  string keyFile    : string containing the filename of the key file
   ** @param:  U32 keysIndex     : index for _keys BUF structure
   ** @param:  U32 keyNum        : the key number for this range
   ** @return: none
   **/
  void MakePublicKey(std::string keyFile, U32 keysIndex, U32 keyNum);
  
  /** MakePublicKeys()
   ** @brief:  generates the public public key used for verification
   **          run once after reseeding
   ** @param:  U8  end           : key end range
   ** @param:  string keyFile    : string containing the filename of the key file
   ** @param:  U32 keysIndex     : index for _keys BUF structure
   ** @param:  U8  start         : key start range
   ** @return: none
   **/
  void MakePublicKeys(U8 end, std::string keyFile, U32 keysIndex, U8 start);
  
  /** MakeSeed()
   ** @brief:  uses KDF to generate a seed master for a signing scheme
   **          resets signing data for a clean state
   ** @param:  none
   ** @return: none
   **/
  void MakeSeed(void);
  
  /** MakeSubtree()
   ** @brief:  calculates the root key of a subtree
   ** @param:  U8  end           : key end range
   ** @param:  string keyFile    : string containing the filename of the public key file
   ** @param:  U8  start         : key start range
   ** @param:  BUF *subtree      : pointer to subtree array for insertion
   ** @return: none
   **/
  void MakeSubtree(U8 end, std::string keyFile, U8 start, BUF *subtree);
  
  /** ReadKeyset()
   ** @brief:  reads _currentKeyset to a seed file
   **          +--------------------------------+
   **          | _currentKeyset                 |
   **          +--------------------------------+
   ** @param:  string stateFile  : string containing the filename of the seed file
   ** @return: none
   **/
  void ReadKeyset(std::string stateFile);
  
  /** ReadPublicKey()
   ** @brief:  reads the public key from a key file
   **          +--------------------------------+
   **          | _merkleTree[TOP_TREE_HEIGHT-1] |
   **          +--------------------------------+
   ** @param:  string keyFile    : string containing the filename of the key file
   ** @return: none
   **/
  void ReadPublicKey(std::string keyFile);
  
  /** ReadState()
   ** @brief:  reads the internal state from a seed file
   **          +--------------------------------+
   **          | _currentKeyset                 |
   **          +--------------------------------+
   **          | _seed                          |
   **          +--------------------------------+
   **          | _merkleTree[TOP_TREE_HEIGHT-1] |
   **          +--------------------------------+
   **          | _merkleTree[TOP_TREE_HEIGHT-2] |
   **          +--------------------------------+
   **          | ...                            |
   **          +--------------------------------+
   **          | _merkleTree[0]                 |
   **          +--------------------------------+
   ** @param:  string stateFile  : string containing the filename of the seed file
   ** @return: none
   **/
  void ReadState(std::string stateFile);
  
  /** Winternitz()
   ** @brief:  runs a Winternitz chain for a key generation or signing operation
   **          the following values are concatenated and hashed with SHA256
   **          +--------------------------------+
   **          | keyFile name      (1x 20 byte) |
   **          +--------------------------------+
   **          | ID_CHAIN          (1x 4 bytes) |
   **          +--------------------------------+
   **          | keyNum            (1x 4 bytes) |
   **          +--------------------------------+
   **          | winternitz        (1x 4 bytes) |
   **          +--------------------------------+
   **          | _keys[keysIndex] (1x 32 bytes) |
   **          +--------------------------------+
   ** @param:  U32 chainLength   : number of Winternitz hashes to perform
   ** @param:  string keyFile    : string containing the filename of the key file
   ** @param:  U32 keysIndex     : index for _keys BUF structure
   ** @param:  U32 keyNum        : the key number for this range
   ** @return: none
   **/
  void Winternitz(U32 chainLength, std::string keyFile, U32 keysIndex, U32 keyNum);
  
  /** WriteKeyset()
   ** @brief:  writes _currentKeyset to a seed file
   **          +--------------------------------+
   **          | _currentKeyset                 |
   **          +--------------------------------+
   ** @param:  string stateFile  : string containing the filename of the seed file
   ** @return: none
   **/
  void WriteKeyset(std::string stateFile);
  
  /** WritePublicKey()
   ** @brief:  writes the public key to a key file
   **          +--------------------------------+
   **          | _merkleTree[TOP_TREE_HEIGHT-1] |
   **          +--------------------------------+
   ** @param:  string keyFile    : string containing the filename of the key file
   ** @return: none
   **/
  void WritePublicKey(std::string keyFile);
  
  /** WriteState()
   ** @brief:  writes the internal state to a seed file
   **          +--------------------------------+
   **          | _currentKeyset                 |
   **          +--------------------------------+
   **          | _seed                          |
   **          +--------------------------------+
   **          | _merkleTree[TOP_TREE_HEIGHT-1] |
   **          +--------------------------------+
   **          | _merkleTree[TOP_TREE_HEIGHT-2] |
   **          +--------------------------------+
   **          | ...                            |
   **          +--------------------------------+
   **          | _merkleTree[0]                 |
   **          +--------------------------------+
   ** @param:  string stateFiles  : string containing the filename of the seed file
   ** @return: none
   **/
  void WriteState(std::string stateFile);
  
}; // class LMS

#endif // LMS_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/
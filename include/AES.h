/**
 ** @file: AES.h
 ** @brief:    header file for AES-256 encryption in accordance with FIPS 197
 ** 
 ** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/

#include "Defs.h"

#ifndef AES_H
#define AES_H

// universal AES-256 constants from FIPS 197 and FIPS 800-38d
#define AES_BLK_SZ        16        // AES-256 block size in bytes

class AES {
public:
  /** AES()
   ** @brief:  constructor
   **          declares heap memory and initializes pointer values
   **          copies a key passed as input to private member
   ** @param:  none
   ** @return: none
   **/
  AES(void);
  
  /** ~AES()
   ** @brief: deletes allocated memory and sets pointers to nullptr
   ** @param:  none
   ** @return: none
   **/
  ~AES(void);
  
  /** Decrypt()
   ** @brief:  decrypts ciphertext to produce one block (16 bytes) of plaintext
   ** @param:  U8 *ciphertextPtr : pointer to one block of ciphertext data to be decrypted
   **                              size should be 16 bytes, padded if necessary
   **          U8 *plaintextPtr  : pointer to plaintext data buffer
   **                              allocated size should be 16 bytes
   ** @return: none
   **/
  void Decrypt(U8 *ciphertextPtr, U8 *plaintextPtr);
  
  /** Encrypt()
   ** @brief:  encrypts plaintext to produce one block (16 bytes) of ciphertext
   ** @param:  U8 *ciphertextPtr : pointer to ciphertext data buffer
   **                              allocated size should be 16 bytes
   ** @param:  U8 *plaintextPtr  : pointer to one block of plaintext data to be encrypted
   **                              size should be 16 bytes, padded if necessary
   ** @return: none
   **/
  void Encrypt(U8 *ciphertextPtr, U8 *plaintextPtr);
  
  /** SetKey()
   ** @brief:  Calculates AES-256 key expansion
   ** @param:  U8 *keyPtr        : pointer to 32-byte key data buffer
   ** @return: none
   **/
  void SetKey(const U8 *keyPtr);
  
protected:
  // intentionally left empty
  
private:
  BUF _stateBlock;          // buffer structure for the current AES state block
  U32 *_expandedKeyPtr;     // pointer to expanded AES-256 key buffer
  U32 *_roundConstPtr;      // pointer to AES-256 round constants buffer
  
  /** AddRoundKey()
   ** @brief:  creates a round key consisting of four (4) four-byte words
   **          (i.e. a 4x4-byte matrix) representing XOR of inverse data
   **          and inverse expanded key
   ** @param:  U32 keyOffset     : offset from the beginning of _expandedKeyPtr
   ** @return: none
   **/
  void AddRoundKey(U32 keyOffset);
  
  /** CalcRoundConst()
   ** @brief:  calculates round constants via _roundConstPtr to be
   **          used in key expansion
   ** @param:  none
   ** @return: none
   **/
  void CalcRoundConst(void);
  
  /** KeyExpansion()
   ** @brief:  performs AES-256 key expansion from original 32-byte key 
   **          to complete 240-byte AES-256 expanded key
   ** @param:  U8 *keyPtr        : pointer to 32-byte AES-256 key
   ** @return: none
   **/
  void KeyExpansion(const U8 *keyPtr);
  
  /** MakeWord()
   ** @brief:  constructs a 32-bit word from four (4) 8-bit bytes
   ** @param:  U8 byte0          : first byte
   ** @param:  U8 byte1          : second byte
   ** @param:  U8 byte2          : third byte
   ** @param:  U8 byte3          : fourth byte
   ** @return: U32               : output word
   **/
  U32 MakeWord(U8 byte0, U8 byte1, U8 byte2, U8 byte3);
  
  /** MixColsDec()
   ** @brief:  mixes columns according to FIPS 197 AES-256 specifications
   ** @param:  none
   ** @return: none
   **/
  void MixColsDec(void);
  
  /** MixColsEnc()
   ** @brief:  mixes columns according to FIPS 197 AES-256 specifications
   ** @param:  none
   ** @return: none
   **/
  void MixColsEnc(void);
  
  /** RotWord()
   ** @brief:  rotates input word: [a3, a2, a1, a0]
   **                          to: [a0, a3, a2, a1]
   ** @param:  U32 inputWord     : word to rotate
   ** @return: U32               : word with rotated bytes
   **/
  U32 RotWord(U32 inputWord);
  
  /** ShiftColsDec()
   ** @brief:  rotates matrix _stateBlockPtr:
   **          [[a0, a5, aa, af]
   **           [a4, a9, ae, a3]
   **           [a8, ad, a2, a7]
   **           [ac, a1, a6, ab]]
   **          to output words:
   **          [[a0, a1, a2, a3]
   **           [a4, a5, a6, a7]
   **           [a8, a9, aa, ab]
   **           [ac, ad, ae, af]]
   ** @param:  none
   ** @return: none
   **/
  void ShiftColsDec(void);
  
  /** ShiftColsEnc()
   ** @brief:  rotates matrix _stateBlockPtr:
   **          [[a0, a1, a2, a3]
   **           [a4, a5, a6, a7]
   **           [a8, a9, aa, ab]
   **           [ac, ad, ae, af]]
   **          to output words:
   **          [[a0, a5, aa, af]
   **           [a4, a9, ae, a3]
   **           [a8, ad, a2, a7]
   **           [ac, a1, a6, ab]]
   ** @param:  none
   ** @return: none
   **/
  void ShiftColsEnc(void);
  
  /** SubBytesDec()
   ** @brief:  iterates calls to SubWordEnc for the current state block
   ** @param:  none
   ** @return: none
   **/
  void SubBytesDec(void);
  
  /** SubBytesEnc()
   ** @brief:  iterates calls to SubWord for the current state block
   ** @param:  none
   ** @return: none
   **/
  void SubBytesEnc(void);
  
  /** SubWordDec()
   ** @brief:  uses hex values of a word to replace bytes from the lookup table S_BOX_DEC
   ** @param:  U32 inputWord     : four input bytes for lookup
   ** @return: U32               : word with S_BOX_DEC substitution
   **/
  U32 SubWordDec(U32 inputWord);
  
  /** SubWordEnc()
   ** @brief:  uses hex values of a word to replace bytes from the lookup table S_BOX_ENC
   ** @param:  U32 inputWord     : four input bytes for lookup
   ** @return: U32               : word with S_BOX_ENC substitution
   **/
  U32 SubWordEnc(U32 inputWord);
  
  /** XTime()
   ** @brief:  multiplies two (2) polynomials, p1 and p2, together and returns
   **          the result
   ** @param:  U8 poly1          : polynomial p1
   ** @param:  U8 poly2          : polynomial p2
   ** @return: U8                : result of multiplication
   **/
  U8 XTime(U8 poly1, U8 poly2);
}; // class AES

#endif // AES_H

/** @copyright (C) 2024, Wil Johnson, All rights reserved
 ** 
 ** This program is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU General Public License
 ** as published by the Free Software Foundation; either version 2
 ** of the License, or (at your option) any later version.
 ** 
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **/